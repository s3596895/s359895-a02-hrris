﻿Imports System.Text
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports s3596895_A02_HRRIS.Validation
<TestClass()> Public Class UnitTest1

    <TestInitialize()> Public Sub setup()
        Debug.Print("Setting up ...")
    End Sub

    <TestCleanup()> Public Sub cleanup()
        Debug.Print("Cleaning up ...")
    End Sub

    'test the string contain number
    <TestMethod()> Public Sub Testnumerical()
        Dim validation As New s3596895_A02_HRRIS.Validation
        Dim testobject = "213"
        Assert.AreEqual(True, validation.IsNumericVal(testobject))
    End Sub


    'test the string is NOT contain number
    <TestMethod()> Public Sub Testnumerical2()
        Dim validation As New s3596895_A02_HRRIS.Validation
        Dim testobject = "abc"
        Assert.AreEqual(False, validation.IsNumericVal(testobject))
    End Sub

    'test the string is contain number or text or some special character
    <TestMethod()> Public Sub Testalphanumeric()
        Dim validation As New s3596895_A02_HRRIS.Validation
        Dim testobject = "abc21312"
        Assert.AreEqual(True, validation.IsAlphaNumeric(testobject))
    End Sub

    'test the string is NOT contain number or text or some special character
    <TestMethod()> Public Sub Testalphanumeric2()
        Dim validation As New s3596895_A02_HRRIS.Validation
        Dim testobject = "$$$`~"
        Assert.AreEqual(False, validation.IsAlphaNumeric(testobject))
    End Sub

    'test the string have the right email format
    <TestMethod()> Public Sub testemail()
        Dim validation As New s3596895_A02_HRRIS.Validation
        Dim testobject = "sas2@ad.sada"
        Assert.AreEqual(True, validation.isvalidateEmail(testobject))
    End Sub

    'test the string DON'T have the right email format
    <TestMethod()> Public Sub testemail2()
        Dim validation As New s3596895_A02_HRRIS.Validation
        Dim testobject = "sas2@2121.12121"
        Assert.AreEqual(False, validation.isvalidateEmail(testobject))
    End Sub

    'test the string have text character only
    <TestMethod()> Public Sub testonlytext()
        Dim validation As New s3596895_A02_HRRIS.Validation
        Dim testobject = "I hate this assignment"
        Assert.AreEqual(True, validation.istext(testobject))
    End Sub

    'test the string DON'T have text character only
    <TestMethod()> Public Sub testonlytext2()
        Dim validation As New s3596895_A02_HRRIS.Validation
        Dim testobject = "2132321"
        Assert.AreEqual(False, validation.istext(testobject))
    End Sub

End Class