﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Customer
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Customer))
        Me.picErrorField4 = New System.Windows.Forms.PictureBox()
        Me.picErrorField3 = New System.Windows.Forms.PictureBox()
        Me.picErrorField2 = New System.Windows.Forms.PictureBox()
        Me.picErrorField5 = New System.Windows.Forms.PictureBox()
        Me.picErrorField6 = New System.Windows.Forms.PictureBox()
        Me.picErrorField1 = New System.Windows.Forms.PictureBox()
        Me.btnInsert = New System.Windows.Forms.Button()
        Me.lblcusGender = New System.Windows.Forms.Label()
        Me.lblcusFirstName = New System.Windows.Forms.Label()
        Me.lblcusDob = New System.Windows.Forms.Label()
        Me.lblcusEmail = New System.Windows.Forms.Label()
        Me.lblcusAddress = New System.Windows.Forms.Label()
        Me.lblcusPhone = New System.Windows.Forms.Label()
        Me.lblcusLastName = New System.Windows.Forms.Label()
        Me.txtLastName = New System.Windows.Forms.TextBox()
        Me.txtFirstName = New System.Windows.Forms.TextBox()
        Me.txtAddress = New System.Windows.Forms.TextBox()
        Me.txtEmail = New System.Windows.Forms.TextBox()
        Me.radiobtnMale = New System.Windows.Forms.RadioButton()
        Me.radiobtnFemale = New System.Windows.Forms.RadioButton()
        Me.DobValue = New System.Windows.Forms.DateTimePicker()
        Me.txtPhone = New System.Windows.Forms.TextBox()
        Me.gbxCustomer = New System.Windows.Forms.GroupBox()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.BookingToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExitToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        CType(Me.picErrorField4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picErrorField3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picErrorField2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picErrorField5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picErrorField6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picErrorField1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxCustomer.SuspendLayout()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'picErrorField4
        '
        Me.picErrorField4.Image = CType(resources.GetObject("picErrorField4.Image"), System.Drawing.Image)
        Me.picErrorField4.Location = New System.Drawing.Point(363, 140)
        Me.picErrorField4.Name = "picErrorField4"
        Me.picErrorField4.Size = New System.Drawing.Size(20, 20)
        Me.picErrorField4.TabIndex = 18
        Me.picErrorField4.TabStop = False
        Me.picErrorField4.Visible = False
        '
        'picErrorField3
        '
        Me.picErrorField3.Image = CType(resources.GetObject("picErrorField3.Image"), System.Drawing.Image)
        Me.picErrorField3.Location = New System.Drawing.Point(364, 111)
        Me.picErrorField3.Name = "picErrorField3"
        Me.picErrorField3.Size = New System.Drawing.Size(20, 20)
        Me.picErrorField3.TabIndex = 17
        Me.picErrorField3.TabStop = False
        Me.picErrorField3.Visible = False
        '
        'picErrorField2
        '
        Me.picErrorField2.Image = CType(resources.GetObject("picErrorField2.Image"), System.Drawing.Image)
        Me.picErrorField2.Location = New System.Drawing.Point(364, 80)
        Me.picErrorField2.Name = "picErrorField2"
        Me.picErrorField2.Size = New System.Drawing.Size(20, 20)
        Me.picErrorField2.TabIndex = 16
        Me.picErrorField2.TabStop = False
        Me.picErrorField2.Visible = False
        '
        'picErrorField5
        '
        Me.picErrorField5.Image = CType(resources.GetObject("picErrorField5.Image"), System.Drawing.Image)
        Me.picErrorField5.Location = New System.Drawing.Point(364, 170)
        Me.picErrorField5.Name = "picErrorField5"
        Me.picErrorField5.Size = New System.Drawing.Size(20, 20)
        Me.picErrorField5.TabIndex = 15
        Me.picErrorField5.TabStop = False
        Me.picErrorField5.Visible = False
        '
        'picErrorField6
        '
        Me.picErrorField6.Image = CType(resources.GetObject("picErrorField6.Image"), System.Drawing.Image)
        Me.picErrorField6.Location = New System.Drawing.Point(363, 200)
        Me.picErrorField6.Name = "picErrorField6"
        Me.picErrorField6.Size = New System.Drawing.Size(20, 20)
        Me.picErrorField6.TabIndex = 19
        Me.picErrorField6.TabStop = False
        Me.picErrorField6.Visible = False
        '
        'picErrorField1
        '
        Me.picErrorField1.Image = CType(resources.GetObject("picErrorField1.Image"), System.Drawing.Image)
        Me.picErrorField1.Location = New System.Drawing.Point(364, 230)
        Me.picErrorField1.Name = "picErrorField1"
        Me.picErrorField1.Size = New System.Drawing.Size(20, 20)
        Me.picErrorField1.TabIndex = 14
        Me.picErrorField1.TabStop = False
        Me.picErrorField1.Visible = False
        '
        'btnInsert
        '
        Me.btnInsert.Location = New System.Drawing.Point(409, 78)
        Me.btnInsert.Name = "btnInsert"
        Me.btnInsert.Size = New System.Drawing.Size(115, 23)
        Me.btnInsert.TabIndex = 13
        Me.btnInsert.Text = "Insert customer"
        Me.btnInsert.UseVisualStyleBackColor = True
        '
        'lblcusGender
        '
        Me.lblcusGender.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblcusGender.Location = New System.Drawing.Point(6, 241)
        Me.lblcusGender.Name = "lblcusGender"
        Me.lblcusGender.Size = New System.Drawing.Size(68, 20)
        Me.lblcusGender.TabIndex = 2
        Me.lblcusGender.Text = "Gender"
        Me.lblcusGender.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblcusFirstName
        '
        Me.lblcusFirstName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblcusFirstName.Location = New System.Drawing.Point(6, 51)
        Me.lblcusFirstName.Name = "lblcusFirstName"
        Me.lblcusFirstName.Size = New System.Drawing.Size(68, 20)
        Me.lblcusFirstName.TabIndex = 5
        Me.lblcusFirstName.Text = "First Name"
        Me.lblcusFirstName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblcusDob
        '
        Me.lblcusDob.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblcusDob.Location = New System.Drawing.Point(5, 200)
        Me.lblcusDob.Name = "lblcusDob"
        Me.lblcusDob.Size = New System.Drawing.Size(68, 20)
        Me.lblcusDob.TabIndex = 15
        Me.lblcusDob.Text = "Dob"
        Me.lblcusDob.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblcusEmail
        '
        Me.lblcusEmail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblcusEmail.Location = New System.Drawing.Point(6, 169)
        Me.lblcusEmail.Name = "lblcusEmail"
        Me.lblcusEmail.Size = New System.Drawing.Size(68, 20)
        Me.lblcusEmail.TabIndex = 13
        Me.lblcusEmail.Text = "Email"
        Me.lblcusEmail.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblcusAddress
        '
        Me.lblcusAddress.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblcusAddress.Location = New System.Drawing.Point(6, 140)
        Me.lblcusAddress.Name = "lblcusAddress"
        Me.lblcusAddress.Size = New System.Drawing.Size(68, 20)
        Me.lblcusAddress.TabIndex = 11
        Me.lblcusAddress.Text = "Address"
        Me.lblcusAddress.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblcusPhone
        '
        Me.lblcusPhone.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblcusPhone.Location = New System.Drawing.Point(6, 110)
        Me.lblcusPhone.Name = "lblcusPhone"
        Me.lblcusPhone.Size = New System.Drawing.Size(68, 20)
        Me.lblcusPhone.TabIndex = 9
        Me.lblcusPhone.Text = "Phone"
        Me.lblcusPhone.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblcusLastName
        '
        Me.lblcusLastName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblcusLastName.Location = New System.Drawing.Point(6, 80)
        Me.lblcusLastName.Name = "lblcusLastName"
        Me.lblcusLastName.Size = New System.Drawing.Size(68, 20)
        Me.lblcusLastName.TabIndex = 7
        Me.lblcusLastName.Text = "Last Name"
        Me.lblcusLastName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtLastName
        '
        Me.txtLastName.Location = New System.Drawing.Point(80, 80)
        Me.txtLastName.Name = "txtLastName"
        Me.txtLastName.Size = New System.Drawing.Size(258, 20)
        Me.txtLastName.TabIndex = 2
        '
        'txtFirstName
        '
        Me.txtFirstName.Location = New System.Drawing.Point(80, 51)
        Me.txtFirstName.Name = "txtFirstName"
        Me.txtFirstName.Size = New System.Drawing.Size(258, 20)
        Me.txtFirstName.TabIndex = 1
        '
        'txtAddress
        '
        Me.txtAddress.Location = New System.Drawing.Point(80, 140)
        Me.txtAddress.Name = "txtAddress"
        Me.txtAddress.Size = New System.Drawing.Size(258, 20)
        Me.txtAddress.TabIndex = 4
        '
        'txtEmail
        '
        Me.txtEmail.Location = New System.Drawing.Point(80, 169)
        Me.txtEmail.Name = "txtEmail"
        Me.txtEmail.Size = New System.Drawing.Size(258, 20)
        Me.txtEmail.TabIndex = 5
        '
        'radiobtnMale
        '
        Me.radiobtnMale.AutoSize = True
        Me.radiobtnMale.Location = New System.Drawing.Point(80, 243)
        Me.radiobtnMale.Name = "radiobtnMale"
        Me.radiobtnMale.Size = New System.Drawing.Size(48, 17)
        Me.radiobtnMale.TabIndex = 5
        Me.radiobtnMale.Text = "Male"
        Me.radiobtnMale.UseVisualStyleBackColor = True
        '
        'radiobtnFemale
        '
        Me.radiobtnFemale.AutoSize = True
        Me.radiobtnFemale.Location = New System.Drawing.Point(279, 243)
        Me.radiobtnFemale.Name = "radiobtnFemale"
        Me.radiobtnFemale.Size = New System.Drawing.Size(59, 17)
        Me.radiobtnFemale.TabIndex = 6
        Me.radiobtnFemale.Text = "Female"
        Me.radiobtnFemale.UseVisualStyleBackColor = True
        '
        'DobValue
        '
        Me.DobValue.Location = New System.Drawing.Point(80, 200)
        Me.DobValue.Name = "DobValue"
        Me.DobValue.Size = New System.Drawing.Size(258, 20)
        Me.DobValue.TabIndex = 6
        Me.DobValue.Value = New Date(2017, 12, 18, 0, 0, 0, 0)
        '
        'txtPhone
        '
        Me.txtPhone.Location = New System.Drawing.Point(81, 109)
        Me.txtPhone.MaxLength = 9
        Me.txtPhone.Name = "txtPhone"
        Me.txtPhone.Size = New System.Drawing.Size(257, 20)
        Me.txtPhone.TabIndex = 3
        '
        'gbxCustomer
        '
        Me.gbxCustomer.Controls.Add(Me.txtPhone)
        Me.gbxCustomer.Controls.Add(Me.DobValue)
        Me.gbxCustomer.Controls.Add(Me.radiobtnFemale)
        Me.gbxCustomer.Controls.Add(Me.radiobtnMale)
        Me.gbxCustomer.Controls.Add(Me.txtEmail)
        Me.gbxCustomer.Controls.Add(Me.txtAddress)
        Me.gbxCustomer.Controls.Add(Me.txtFirstName)
        Me.gbxCustomer.Controls.Add(Me.txtLastName)
        Me.gbxCustomer.Controls.Add(Me.lblcusLastName)
        Me.gbxCustomer.Controls.Add(Me.lblcusPhone)
        Me.gbxCustomer.Controls.Add(Me.lblcusAddress)
        Me.gbxCustomer.Controls.Add(Me.lblcusEmail)
        Me.gbxCustomer.Controls.Add(Me.lblcusDob)
        Me.gbxCustomer.Controls.Add(Me.lblcusFirstName)
        Me.gbxCustomer.Controls.Add(Me.lblcusGender)
        Me.gbxCustomer.Location = New System.Drawing.Point(12, 30)
        Me.gbxCustomer.Name = "gbxCustomer"
        Me.gbxCustomer.Size = New System.Drawing.Size(345, 299)
        Me.gbxCustomer.TabIndex = 12
        Me.gbxCustomer.TabStop = False
        Me.gbxCustomer.Text = "Customer Details"
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItem1})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(534, 24)
        Me.MenuStrip1.TabIndex = 90
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BookingToolStripMenuItem, Me.ExitToolStripMenuItem})
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        Me.ToolStripMenuItem1.Size = New System.Drawing.Size(37, 20)
        Me.ToolStripMenuItem1.Text = "File"
        '
        'BookingToolStripMenuItem
        '
        Me.BookingToolStripMenuItem.Name = "BookingToolStripMenuItem"
        Me.BookingToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.BookingToolStripMenuItem.Text = "Booking"
        '
        'ExitToolStripMenuItem
        '
        Me.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem"
        Me.ExitToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.ExitToolStripMenuItem.Text = "Exit"
        '
        'Customer
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(534, 361)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Controls.Add(Me.picErrorField4)
        Me.Controls.Add(Me.picErrorField3)
        Me.Controls.Add(Me.picErrorField2)
        Me.Controls.Add(Me.picErrorField5)
        Me.Controls.Add(Me.picErrorField6)
        Me.Controls.Add(Me.picErrorField1)
        Me.Controls.Add(Me.btnInsert)
        Me.Controls.Add(Me.gbxCustomer)
        Me.Name = "Customer"
        Me.Text = "Customer"
        CType(Me.picErrorField4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picErrorField3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picErrorField2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picErrorField5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picErrorField6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picErrorField1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxCustomer.ResumeLayout(False)
        Me.gbxCustomer.PerformLayout()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents picErrorField4 As PictureBox
    Friend WithEvents picErrorField3 As PictureBox
    Friend WithEvents picErrorField2 As PictureBox
    Friend WithEvents picErrorField5 As PictureBox
    Friend WithEvents picErrorField6 As PictureBox
    Friend WithEvents picErrorField1 As PictureBox
    Friend WithEvents btnInsert As Button
    Friend WithEvents lblcusGender As Label
    Friend WithEvents lblcusFirstName As Label
    Friend WithEvents lblcusDob As Label
    Friend WithEvents lblcusEmail As Label
    Friend WithEvents lblcusAddress As Label
    Friend WithEvents lblcusPhone As Label
    Friend WithEvents lblcusLastName As Label
    Friend WithEvents txtLastName As TextBox
    Friend WithEvents txtFirstName As TextBox
    Friend WithEvents txtAddress As TextBox
    Friend WithEvents txtEmail As TextBox
    Friend WithEvents radiobtnMale As RadioButton
    Friend WithEvents radiobtnFemale As RadioButton
    Friend WithEvents DobValue As DateTimePicker
    Friend WithEvents txtPhone As TextBox
    Friend WithEvents gbxCustomer As GroupBox
    Friend WithEvents MenuStrip1 As MenuStrip
    Friend WithEvents ToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents BookingToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ExitToolStripMenuItem As ToolStripMenuItem
End Class
