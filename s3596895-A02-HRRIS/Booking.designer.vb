﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Booking
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Booking))
        Me.Button1 = New System.Windows.Forms.Button()
        Me.lblCustomerID = New System.Windows.Forms.Label()
        Me.lblRoom = New System.Windows.Forms.Label()
        Me.lblNumberDays = New System.Windows.Forms.Label()
        Me.lblNumberGuests = New System.Windows.Forms.Label()
        Me.lblCheckinDate = New System.Windows.Forms.Label()
        Me.lblTotalPrice = New System.Windows.Forms.Label()
        Me.lblComment = New System.Windows.Forms.Label()
        Me.cbbCustomerID = New System.Windows.Forms.ComboBox()
        Me.cbbRoomID = New System.Windows.Forms.ComboBox()
        Me.CheckinDate = New System.Windows.Forms.DateTimePicker()
        Me.txtTotalPrice = New System.Windows.Forms.TextBox()
        Me.txtComment = New System.Windows.Forms.TextBox()
        Me.txtCustomerName = New System.Windows.Forms.TextBox()
        Me.txtRoomType = New System.Windows.Forms.TextBox()
        Me.txtRoomPrice = New System.Windows.Forms.TextBox()
        Me.btnAddNewBooking = New System.Windows.Forms.Button()
        Me.txtRoomNumber = New System.Windows.Forms.TextBox()
        Me.p1 = New System.Windows.Forms.PictureBox()
        Me.p2 = New System.Windows.Forms.PictureBox()
        Me.p3 = New System.Windows.Forms.PictureBox()
        Me.p4 = New System.Windows.Forms.PictureBox()
        Me.p6 = New System.Windows.Forms.PictureBox()
        Me.p7 = New System.Windows.Forms.PictureBox()
        Me.txtNumberDays = New System.Windows.Forms.TextBox()
        Me.txtNumberGuests = New System.Windows.Forms.TextBox()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.FindToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CustomerToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AddToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.FindToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.InvoiceToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AddToolStripMenuItem2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.RoomToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AddToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.FindToolStripMenuItem2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.HelpToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.HelpPageToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AboutToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AboutPageToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReportToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExitToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        CType(Me.p1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.p2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.p3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.p4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.p6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.p7, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(396, 244)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 20)
        Me.Button1.TabIndex = 74
        Me.Button1.Text = "Calc"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'lblCustomerID
        '
        Me.lblCustomerID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblCustomerID.Location = New System.Drawing.Point(46, 89)
        Me.lblCustomerID.Name = "lblCustomerID"
        Me.lblCustomerID.Size = New System.Drawing.Size(98, 20)
        Me.lblCustomerID.TabIndex = 53
        Me.lblCustomerID.Text = "Customer"
        Me.lblCustomerID.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblRoom
        '
        Me.lblRoom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblRoom.Location = New System.Drawing.Point(46, 118)
        Me.lblRoom.Name = "lblRoom"
        Me.lblRoom.Size = New System.Drawing.Size(98, 20)
        Me.lblRoom.TabIndex = 54
        Me.lblRoom.Text = "Room"
        Me.lblRoom.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblNumberDays
        '
        Me.lblNumberDays.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblNumberDays.Location = New System.Drawing.Point(46, 150)
        Me.lblNumberDays.Name = "lblNumberDays"
        Me.lblNumberDays.Size = New System.Drawing.Size(98, 20)
        Me.lblNumberDays.TabIndex = 55
        Me.lblNumberDays.Text = "Number of days"
        Me.lblNumberDays.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblNumberGuests
        '
        Me.lblNumberGuests.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblNumberGuests.Location = New System.Drawing.Point(46, 181)
        Me.lblNumberGuests.Name = "lblNumberGuests"
        Me.lblNumberGuests.Size = New System.Drawing.Size(98, 20)
        Me.lblNumberGuests.TabIndex = 56
        Me.lblNumberGuests.Text = "Number of guests"
        Me.lblNumberGuests.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblCheckinDate
        '
        Me.lblCheckinDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblCheckinDate.Location = New System.Drawing.Point(46, 211)
        Me.lblCheckinDate.Name = "lblCheckinDate"
        Me.lblCheckinDate.Size = New System.Drawing.Size(98, 20)
        Me.lblCheckinDate.TabIndex = 57
        Me.lblCheckinDate.Text = "Check in Date"
        Me.lblCheckinDate.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblTotalPrice
        '
        Me.lblTotalPrice.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblTotalPrice.Location = New System.Drawing.Point(46, 243)
        Me.lblTotalPrice.Name = "lblTotalPrice"
        Me.lblTotalPrice.Size = New System.Drawing.Size(98, 20)
        Me.lblTotalPrice.TabIndex = 58
        Me.lblTotalPrice.Text = "Total price"
        Me.lblTotalPrice.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblComment
        '
        Me.lblComment.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblComment.Location = New System.Drawing.Point(46, 275)
        Me.lblComment.Name = "lblComment"
        Me.lblComment.Size = New System.Drawing.Size(98, 20)
        Me.lblComment.TabIndex = 59
        Me.lblComment.Text = "Comment"
        Me.lblComment.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cbbCustomerID
        '
        Me.cbbCustomerID.FormattingEnabled = True
        Me.cbbCustomerID.Location = New System.Drawing.Point(159, 88)
        Me.cbbCustomerID.Name = "cbbCustomerID"
        Me.cbbCustomerID.Size = New System.Drawing.Size(66, 21)
        Me.cbbCustomerID.TabIndex = 60
        '
        'cbbRoomID
        '
        Me.cbbRoomID.FormattingEnabled = True
        Me.cbbRoomID.Location = New System.Drawing.Point(159, 117)
        Me.cbbRoomID.Name = "cbbRoomID"
        Me.cbbRoomID.Size = New System.Drawing.Size(66, 21)
        Me.cbbRoomID.TabIndex = 61
        '
        'CheckinDate
        '
        Me.CheckinDate.Location = New System.Drawing.Point(159, 211)
        Me.CheckinDate.Name = "CheckinDate"
        Me.CheckinDate.Size = New System.Drawing.Size(229, 20)
        Me.CheckinDate.TabIndex = 64
        '
        'txtTotalPrice
        '
        Me.txtTotalPrice.Enabled = False
        Me.txtTotalPrice.Location = New System.Drawing.Point(159, 244)
        Me.txtTotalPrice.Name = "txtTotalPrice"
        Me.txtTotalPrice.Size = New System.Drawing.Size(231, 20)
        Me.txtTotalPrice.TabIndex = 65
        '
        'txtComment
        '
        Me.txtComment.Location = New System.Drawing.Point(159, 275)
        Me.txtComment.Name = "txtComment"
        Me.txtComment.Size = New System.Drawing.Size(312, 20)
        Me.txtComment.TabIndex = 66
        '
        'txtCustomerName
        '
        Me.txtCustomerName.Enabled = False
        Me.txtCustomerName.Location = New System.Drawing.Point(231, 88)
        Me.txtCustomerName.Name = "txtCustomerName"
        Me.txtCustomerName.Size = New System.Drawing.Size(240, 20)
        Me.txtCustomerName.TabIndex = 67
        '
        'txtRoomType
        '
        Me.txtRoomType.Enabled = False
        Me.txtRoomType.Location = New System.Drawing.Point(297, 119)
        Me.txtRoomType.Name = "txtRoomType"
        Me.txtRoomType.Size = New System.Drawing.Size(60, 20)
        Me.txtRoomType.TabIndex = 68
        '
        'txtRoomPrice
        '
        Me.txtRoomPrice.Enabled = False
        Me.txtRoomPrice.Location = New System.Drawing.Point(365, 117)
        Me.txtRoomPrice.Name = "txtRoomPrice"
        Me.txtRoomPrice.Size = New System.Drawing.Size(106, 20)
        Me.txtRoomPrice.TabIndex = 69
        '
        'btnAddNewBooking
        '
        Me.btnAddNewBooking.Location = New System.Drawing.Point(547, 89)
        Me.btnAddNewBooking.Name = "btnAddNewBooking"
        Me.btnAddNewBooking.Size = New System.Drawing.Size(107, 23)
        Me.btnAddNewBooking.TabIndex = 70
        Me.btnAddNewBooking.Text = "Booking"
        Me.btnAddNewBooking.UseVisualStyleBackColor = True
        '
        'txtRoomNumber
        '
        Me.txtRoomNumber.Enabled = False
        Me.txtRoomNumber.Location = New System.Drawing.Point(231, 119)
        Me.txtRoomNumber.Name = "txtRoomNumber"
        Me.txtRoomNumber.Size = New System.Drawing.Size(60, 20)
        Me.txtRoomNumber.TabIndex = 73
        '
        'p1
        '
        Me.p1.Image = CType(resources.GetObject("p1.Image"), System.Drawing.Image)
        Me.p1.Location = New System.Drawing.Point(485, 88)
        Me.p1.Name = "p1"
        Me.p1.Size = New System.Drawing.Size(20, 20)
        Me.p1.TabIndex = 75
        Me.p1.TabStop = False
        Me.p1.Visible = False
        '
        'p2
        '
        Me.p2.Image = CType(resources.GetObject("p2.Image"), System.Drawing.Image)
        Me.p2.Location = New System.Drawing.Point(485, 119)
        Me.p2.Name = "p2"
        Me.p2.Size = New System.Drawing.Size(20, 20)
        Me.p2.TabIndex = 76
        Me.p2.TabStop = False
        Me.p2.Visible = False
        '
        'p3
        '
        Me.p3.Image = CType(resources.GetObject("p3.Image"), System.Drawing.Image)
        Me.p3.Location = New System.Drawing.Point(485, 151)
        Me.p3.Name = "p3"
        Me.p3.Size = New System.Drawing.Size(20, 20)
        Me.p3.TabIndex = 77
        Me.p3.TabStop = False
        Me.p3.Visible = False
        '
        'p4
        '
        Me.p4.Image = CType(resources.GetObject("p4.Image"), System.Drawing.Image)
        Me.p4.Location = New System.Drawing.Point(485, 181)
        Me.p4.Name = "p4"
        Me.p4.Size = New System.Drawing.Size(20, 20)
        Me.p4.TabIndex = 78
        Me.p4.TabStop = False
        Me.p4.Visible = False
        '
        'p6
        '
        Me.p6.Image = CType(resources.GetObject("p6.Image"), System.Drawing.Image)
        Me.p6.Location = New System.Drawing.Point(485, 244)
        Me.p6.Name = "p6"
        Me.p6.Size = New System.Drawing.Size(20, 20)
        Me.p6.TabIndex = 80
        Me.p6.TabStop = False
        Me.p6.Visible = False
        '
        'p7
        '
        Me.p7.Image = CType(resources.GetObject("p7.Image"), System.Drawing.Image)
        Me.p7.Location = New System.Drawing.Point(485, 275)
        Me.p7.Name = "p7"
        Me.p7.Size = New System.Drawing.Size(20, 20)
        Me.p7.TabIndex = 81
        Me.p7.TabStop = False
        Me.p7.Visible = False
        '
        'txtNumberDays
        '
        Me.txtNumberDays.Location = New System.Drawing.Point(159, 151)
        Me.txtNumberDays.MaxLength = 2
        Me.txtNumberDays.Name = "txtNumberDays"
        Me.txtNumberDays.Size = New System.Drawing.Size(229, 20)
        Me.txtNumberDays.TabIndex = 85
        '
        'txtNumberGuests
        '
        Me.txtNumberGuests.Location = New System.Drawing.Point(159, 181)
        Me.txtNumberGuests.MaxLength = 3
        Me.txtNumberGuests.Name = "txtNumberGuests"
        Me.txtNumberGuests.Size = New System.Drawing.Size(229, 20)
        Me.txtNumberGuests.TabIndex = 86
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItem1, Me.ToolStripMenuItem2, Me.CustomerToolStripMenuItem, Me.InvoiceToolStripMenuItem, Me.RoomToolStripMenuItem, Me.HelpToolStripMenuItem, Me.AboutToolStripMenuItem, Me.ReportToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(688, 24)
        Me.MenuStrip1.TabIndex = 88
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ExitToolStripMenuItem})
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        Me.ToolStripMenuItem1.Size = New System.Drawing.Size(37, 20)
        Me.ToolStripMenuItem1.Text = "File"
        '
        'ToolStripMenuItem2
        '
        Me.ToolStripMenuItem2.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FindToolStripMenuItem})
        Me.ToolStripMenuItem2.Name = "ToolStripMenuItem2"
        Me.ToolStripMenuItem2.Size = New System.Drawing.Size(63, 20)
        Me.ToolStripMenuItem2.Text = "Booking"
        '
        'FindToolStripMenuItem
        '
        Me.FindToolStripMenuItem.Name = "FindToolStripMenuItem"
        Me.FindToolStripMenuItem.Size = New System.Drawing.Size(97, 22)
        Me.FindToolStripMenuItem.Text = "Find"
        '
        'CustomerToolStripMenuItem
        '
        Me.CustomerToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AddToolStripMenuItem, Me.FindToolStripMenuItem1})
        Me.CustomerToolStripMenuItem.Name = "CustomerToolStripMenuItem"
        Me.CustomerToolStripMenuItem.Size = New System.Drawing.Size(71, 20)
        Me.CustomerToolStripMenuItem.Text = "Customer"
        '
        'AddToolStripMenuItem
        '
        Me.AddToolStripMenuItem.Name = "AddToolStripMenuItem"
        Me.AddToolStripMenuItem.Size = New System.Drawing.Size(97, 22)
        Me.AddToolStripMenuItem.Text = "Add"
        '
        'FindToolStripMenuItem1
        '
        Me.FindToolStripMenuItem1.Name = "FindToolStripMenuItem1"
        Me.FindToolStripMenuItem1.Size = New System.Drawing.Size(97, 22)
        Me.FindToolStripMenuItem1.Text = "Find"
        '
        'InvoiceToolStripMenuItem
        '
        Me.InvoiceToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AddToolStripMenuItem2})
        Me.InvoiceToolStripMenuItem.Name = "InvoiceToolStripMenuItem"
        Me.InvoiceToolStripMenuItem.Size = New System.Drawing.Size(57, 20)
        Me.InvoiceToolStripMenuItem.Text = "Invoice"
        '
        'AddToolStripMenuItem2
        '
        Me.AddToolStripMenuItem2.Name = "AddToolStripMenuItem2"
        Me.AddToolStripMenuItem2.Size = New System.Drawing.Size(96, 22)
        Me.AddToolStripMenuItem2.Text = "Add"
        '
        'RoomToolStripMenuItem
        '
        Me.RoomToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AddToolStripMenuItem1, Me.FindToolStripMenuItem2})
        Me.RoomToolStripMenuItem.Name = "RoomToolStripMenuItem"
        Me.RoomToolStripMenuItem.Size = New System.Drawing.Size(51, 20)
        Me.RoomToolStripMenuItem.Text = "Room"
        '
        'AddToolStripMenuItem1
        '
        Me.AddToolStripMenuItem1.Name = "AddToolStripMenuItem1"
        Me.AddToolStripMenuItem1.Size = New System.Drawing.Size(97, 22)
        Me.AddToolStripMenuItem1.Text = "Add"
        '
        'FindToolStripMenuItem2
        '
        Me.FindToolStripMenuItem2.Name = "FindToolStripMenuItem2"
        Me.FindToolStripMenuItem2.Size = New System.Drawing.Size(97, 22)
        Me.FindToolStripMenuItem2.Text = "Find"
        '
        'HelpToolStripMenuItem
        '
        Me.HelpToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.HelpPageToolStripMenuItem})
        Me.HelpToolStripMenuItem.Name = "HelpToolStripMenuItem"
        Me.HelpToolStripMenuItem.Size = New System.Drawing.Size(44, 20)
        Me.HelpToolStripMenuItem.Text = "Help"
        '
        'HelpPageToolStripMenuItem
        '
        Me.HelpPageToolStripMenuItem.Name = "HelpPageToolStripMenuItem"
        Me.HelpPageToolStripMenuItem.Size = New System.Drawing.Size(128, 22)
        Me.HelpPageToolStripMenuItem.Text = "Help Page"
        '
        'AboutToolStripMenuItem
        '
        Me.AboutToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AboutPageToolStripMenuItem})
        Me.AboutToolStripMenuItem.Name = "AboutToolStripMenuItem"
        Me.AboutToolStripMenuItem.Size = New System.Drawing.Size(52, 20)
        Me.AboutToolStripMenuItem.Text = "About"
        '
        'AboutPageToolStripMenuItem
        '
        Me.AboutPageToolStripMenuItem.Name = "AboutPageToolStripMenuItem"
        Me.AboutPageToolStripMenuItem.Size = New System.Drawing.Size(136, 22)
        Me.AboutPageToolStripMenuItem.Text = "About Page"
        '
        'ReportToolStripMenuItem
        '
        Me.ReportToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AToolStripMenuItem})
        Me.ReportToolStripMenuItem.Name = "ReportToolStripMenuItem"
        Me.ReportToolStripMenuItem.Size = New System.Drawing.Size(54, 20)
        Me.ReportToolStripMenuItem.Text = "Report"
        '
        'AToolStripMenuItem
        '
        Me.AToolStripMenuItem.Name = "AToolStripMenuItem"
        Me.AToolStripMenuItem.Size = New System.Drawing.Size(126, 22)
        Me.AToolStripMenuItem.Text = "All Report"
        '
        'ExitToolStripMenuItem
        '
        Me.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem"
        Me.ExitToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.ExitToolStripMenuItem.Text = "Exit"
        '
        'Booking
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(688, 421)
        Me.Controls.Add(Me.txtNumberGuests)
        Me.Controls.Add(Me.txtNumberDays)
        Me.Controls.Add(Me.p7)
        Me.Controls.Add(Me.p6)
        Me.Controls.Add(Me.p4)
        Me.Controls.Add(Me.p3)
        Me.Controls.Add(Me.p2)
        Me.Controls.Add(Me.p1)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.txtRoomNumber)
        Me.Controls.Add(Me.btnAddNewBooking)
        Me.Controls.Add(Me.txtRoomPrice)
        Me.Controls.Add(Me.txtRoomType)
        Me.Controls.Add(Me.txtCustomerName)
        Me.Controls.Add(Me.txtComment)
        Me.Controls.Add(Me.txtTotalPrice)
        Me.Controls.Add(Me.CheckinDate)
        Me.Controls.Add(Me.cbbRoomID)
        Me.Controls.Add(Me.cbbCustomerID)
        Me.Controls.Add(Me.lblComment)
        Me.Controls.Add(Me.lblTotalPrice)
        Me.Controls.Add(Me.lblCheckinDate)
        Me.Controls.Add(Me.lblNumberGuests)
        Me.Controls.Add(Me.lblNumberDays)
        Me.Controls.Add(Me.lblRoom)
        Me.Controls.Add(Me.lblCustomerID)
        Me.Controls.Add(Me.MenuStrip1)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "Booking"
        Me.Text = "Booking2"
        CType(Me.p1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.p2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.p3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.p4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.p6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.p7, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Button1 As Button
    Friend WithEvents lblCustomerID As Label
    Friend WithEvents lblRoom As Label
    Friend WithEvents lblNumberDays As Label
    Friend WithEvents lblNumberGuests As Label
    Friend WithEvents lblCheckinDate As Label
    Friend WithEvents lblTotalPrice As Label
    Friend WithEvents lblComment As Label
    Friend WithEvents cbbCustomerID As ComboBox
    Friend WithEvents cbbRoomID As ComboBox
    Friend WithEvents CheckinDate As DateTimePicker
    Friend WithEvents txtTotalPrice As TextBox
    Friend WithEvents txtComment As TextBox
    Friend WithEvents txtCustomerName As TextBox
    Friend WithEvents txtRoomType As TextBox
    Friend WithEvents txtRoomPrice As TextBox
    Friend WithEvents btnAddNewBooking As Button
    Friend WithEvents txtRoomNumber As TextBox
    Friend WithEvents p1 As PictureBox
    Friend WithEvents p2 As PictureBox
    Friend WithEvents p3 As PictureBox
    Friend WithEvents p4 As PictureBox
    Friend WithEvents p6 As PictureBox
    Friend WithEvents p7 As PictureBox
    Friend WithEvents txtNumberDays As TextBox
    Friend WithEvents txtNumberGuests As TextBox
    Friend WithEvents MenuStrip1 As MenuStrip
    Friend WithEvents ToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem2 As ToolStripMenuItem
    Friend WithEvents FindToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents CustomerToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents AddToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents FindToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents InvoiceToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents RoomToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents AddToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents FindToolStripMenuItem2 As ToolStripMenuItem
    Friend WithEvents HelpToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents AboutToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ReportToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents AddToolStripMenuItem2 As ToolStripMenuItem
    Friend WithEvents AToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents AboutPageToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents HelpPageToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ExitToolStripMenuItem As ToolStripMenuItem
End Class
