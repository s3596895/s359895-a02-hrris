﻿Option Explicit On
Option Strict On

Imports System.Drawing
Imports System.IO
'Author: Dao Ngoc Minh 
'studen id: s3596895
'This code is written by Dao Ngoc Minh s3596895

Public Class Report4
    Dim ocontroller As DataController = New DataController

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        Dim ovalidation As New Validation
        Dim bAllFieldsValid As Boolean
        bAllFieldsValid = True
        Dim bIsValid As Boolean
        Dim tt As New ToolTip()

        'check month
        If cbbmonth.SelectedIndex = -1 Then
            p1.Visible = True
            tt.SetToolTip(p1, "You must select a month")
            bAllFieldsValid = False

        Else p1.Visible = False
        End If
        'check year
        bIsValid = ovalidation.validateyear(txtyear.Text)
        If bIsValid Then
            p2.Visible = False
        Else
            p2.Visible = True
            tt.SetToolTip(p2, "Number of years must be a number")
            bAllFieldsValid = False

        End If


        If bAllFieldsValid = True Then
            Debug.Print("All fields are valid")
            ocontroller.createReport4(cbbmonth.SelectedItem.ToString, txtyear.Text.ToString)
        End If

    End Sub

    Private Sub Report4_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub
End Class