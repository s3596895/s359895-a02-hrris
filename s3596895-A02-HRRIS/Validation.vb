﻿Option Explicit On
Option Strict On
Imports System.Text.RegularExpressions
' Name: Validation.vb' 
'Description: Class containing validation methods
' Author: Dao Ngoc Minh s3596895
' Date: 21/11/2017
Public Class Validation

    'check number only
    Public Function IsNumericVal(ByVal Val As String) As Boolean
        ' check if a string is numerical and return true false value
        If Val.Length = 0 Then Return False
        Try
            Return IsNumeric(Val)
        Catch ex As Exception
            MsgBox("error" & ex.Message)
            Return False
        End Try
    End Function

    'Check alphanumeric
    Public Function IsAlphaNumeric(ByVal Val As String) As Boolean
        'create pattern
        Dim pattern As Regex = New Regex("[^a-zA-Z0-9 -_]")
        'check pattern
        If Val.Length > 0 And Val.Length < 255 Then
            Return Not pattern.IsMatch(Val)
        Else
            Return False
        End If
    End Function

    'check text only
    Public Function istext(ByVal Val As String) As Boolean
        'create pattern for text only
        Dim pattern As Regex = New Regex("[^a-zA-Z ]")
        'check pattern
        If Val.Length > 0 And Val.Length < 255 Then
            Return Not pattern.IsMatch(Val)
        Else
            Return False
        End If
    End Function

    'Check email
    Public Function isvalidateEmail(ByVal Val As String) As Boolean
        Return Regex.IsMatch(Val, "^([0-9a-zA-Z]([-\.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$")
    End Function

    'check year
    Public Function validateyear(ByVal Val As String) As Boolean
        ' create pattern
        Dim pattern As Regex = New Regex("[^0-9]")

        ' check if pattern is match and length is equal to 4 
        If Val.Length = 4 Then
            Return Not pattern.IsMatch(Val)
        Else
            Return False
        End If
    End Function
End Class

