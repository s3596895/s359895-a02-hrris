﻿Option Strict On
Option Explicit On
'Name: Mainmenu.vb
'Description: Form for booking (attempt number 2)
'Author: Dao Ngoc Minh s3596895
'Date: 21/11/2017
'This code is written by Dao Ngoc Minh s3596895
Imports System.IO

Public Class Invoice
    Dim IsDataBooking As List(Of Hashtable)

    Private Sub Invoice_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim oController As DataController = New DataController
        'show item in drop down list
        cbbBookingId.DropDownStyle = ComboBoxStyle.DropDownList
        IsDataBooking = oController.findAllBooking()

        For Each Booking In IsDataBooking
            cbbBookingId.Items.Add(CStr(Booking("booking_id")))
        Next
        DateInvoice.Value = DateTime.Now
    End Sub

    Private Sub cbbRoomID_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbbBookingId.SelectedIndexChanged
        Dim selectedIndex As Integer = cbbBookingId.SelectedIndex
        Dim htData = IsDataBooking.Item(selectedIndex)

        txtAmount.Text = CStr(htData("total_price"))

    End Sub

    Private Function validateFormData() As Boolean
        Dim oValidation As New Validation
        Dim bAllFieldsValid As Boolean
        bAllFieldsValid = True
        Dim tt As New ToolTip()

        ' Check customer not empty
        If cbbBookingId.Text IsNot "" Then
            p1.Visible = False
        Else
            p1.Visible = True
            tt.SetToolTip(p1, "Please choose a customer")
            bAllFieldsValid = False
        End If

        'Validate all field
        If bAllFieldsValid Then
            Debug.Print("All fields are valid")
        End If
        Return bAllFieldsValid

    End Function

    Private Sub btnGenerate_Click(sender As Object, e As EventArgs) Handles btnGenerate.Click
        Dim bIsValid = validateFormData()
        If bIsValid Then
            ' Instantiate a hashtable and populate it with form data
            Dim htData As Hashtable = New Hashtable
            'take data value from textbox

            htData("booking_id") = cbbBookingId.SelectedItem
            htData("date_invoice") = DateInvoice.Value.Date
            htData("amount") = txtAmount.Text

            Dim oDataController As DataController = New DataController
            Dim iNumRows = oDataController.insertinvoice(htData)
            If iNumRows = 1 Then MsgBox("The invoice information is recorded.")
        End If
    End Sub
End Class