﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Report6
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Report6))
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.p3 = New System.Windows.Forms.PictureBox()
        Me.p2 = New System.Windows.Forms.PictureBox()
        Me.p1 = New System.Windows.Forms.PictureBox()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.cbbBookingMonth = New System.Windows.Forms.ComboBox()
        Me.txtBookingYear = New System.Windows.Forms.TextBox()
        Me.cbbRoomNumber = New System.Windows.Forms.ComboBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.GroupBox4.SuspendLayout()
        CType(Me.p3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.p2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.p1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox4
        '
        Me.GroupBox4.BackColor = System.Drawing.Color.White
        Me.GroupBox4.Controls.Add(Me.p3)
        Me.GroupBox4.Controls.Add(Me.p2)
        Me.GroupBox4.Controls.Add(Me.p1)
        Me.GroupBox4.Controls.Add(Me.Button6)
        Me.GroupBox4.Controls.Add(Me.cbbBookingMonth)
        Me.GroupBox4.Controls.Add(Me.txtBookingYear)
        Me.GroupBox4.Controls.Add(Me.cbbRoomNumber)
        Me.GroupBox4.Controls.Add(Me.Label9)
        Me.GroupBox4.Controls.Add(Me.Label8)
        Me.GroupBox4.Controls.Add(Me.Label10)
        Me.GroupBox4.Location = New System.Drawing.Point(31, 76)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(314, 208)
        Me.GroupBox4.TabIndex = 4
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Booking for a particular room in a particular time"
        '
        'p3
        '
        Me.p3.Image = CType(resources.GetObject("p3.Image"), System.Drawing.Image)
        Me.p3.Location = New System.Drawing.Point(288, 137)
        Me.p3.Name = "p3"
        Me.p3.Size = New System.Drawing.Size(20, 20)
        Me.p3.TabIndex = 23
        Me.p3.TabStop = False
        Me.p3.Visible = False
        '
        'p2
        '
        Me.p2.Image = CType(resources.GetObject("p2.Image"), System.Drawing.Image)
        Me.p2.Location = New System.Drawing.Point(288, 86)
        Me.p2.Name = "p2"
        Me.p2.Size = New System.Drawing.Size(20, 20)
        Me.p2.TabIndex = 22
        Me.p2.TabStop = False
        Me.p2.Visible = False
        '
        'p1
        '
        Me.p1.Image = CType(resources.GetObject("p1.Image"), System.Drawing.Image)
        Me.p1.Location = New System.Drawing.Point(288, 37)
        Me.p1.Name = "p1"
        Me.p1.Size = New System.Drawing.Size(20, 20)
        Me.p1.TabIndex = 21
        Me.p1.TabStop = False
        Me.p1.Visible = False
        '
        'Button6
        '
        Me.Button6.Location = New System.Drawing.Point(116, 170)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(104, 23)
        Me.Button6.TabIndex = 17
        Me.Button6.Text = "Generate"
        Me.Button6.UseVisualStyleBackColor = True
        '
        'cbbBookingMonth
        '
        Me.cbbBookingMonth.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbbBookingMonth.FormattingEnabled = True
        Me.cbbBookingMonth.Items.AddRange(New Object() {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"})
        Me.cbbBookingMonth.Location = New System.Drawing.Point(148, 85)
        Me.cbbBookingMonth.Name = "cbbBookingMonth"
        Me.cbbBookingMonth.Size = New System.Drawing.Size(121, 21)
        Me.cbbBookingMonth.TabIndex = 19
        '
        'txtBookingYear
        '
        Me.txtBookingYear.Location = New System.Drawing.Point(148, 134)
        Me.txtBookingYear.Name = "txtBookingYear"
        Me.txtBookingYear.Size = New System.Drawing.Size(121, 20)
        Me.txtBookingYear.TabIndex = 20
        '
        'cbbRoomNumber
        '
        Me.cbbRoomNumber.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbbRoomNumber.FormattingEnabled = True
        Me.cbbRoomNumber.Location = New System.Drawing.Point(148, 36)
        Me.cbbRoomNumber.Name = "cbbRoomNumber"
        Me.cbbRoomNumber.Size = New System.Drawing.Size(121, 21)
        Me.cbbRoomNumber.TabIndex = 6
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(35, 85)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(37, 13)
        Me.Label9.TabIndex = 17
        Me.Label9.Text = "Month"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(35, 36)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(75, 13)
        Me.Label8.TabIndex = 5
        Me.Label8.Text = "Room Number"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(35, 137)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(29, 13)
        Me.Label10.TabIndex = 18
        Me.Label10.Text = "Year"
        '
        'Report6
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(384, 361)
        Me.Controls.Add(Me.GroupBox4)
        Me.Name = "Report6"
        Me.Text = "Report6"
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        CType(Me.p3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.p2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.p1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents GroupBox4 As GroupBox
    Friend WithEvents Button6 As Button
    Friend WithEvents cbbBookingMonth As ComboBox
    Friend WithEvents txtBookingYear As TextBox
    Friend WithEvents cbbRoomNumber As ComboBox
    Friend WithEvents Label9 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents p1 As PictureBox
    Friend WithEvents p3 As PictureBox
    Friend WithEvents p2 As PictureBox
End Class
