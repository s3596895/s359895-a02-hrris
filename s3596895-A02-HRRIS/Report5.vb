﻿Option Explicit On
Option Strict On

Imports System.Drawing
Imports System.IO
'Author: Dao Ngoc Minh 
'studen id: s3596895
'This code is written by Dao Ngoc Minh s3596895
Public Class Report5
    Dim ocontroller As DataController = New DataController

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles txtreport.Click
        Dim ovalidation As New Validation
        Dim bAllFieldsValid As Boolean
        bAllFieldsValid = True
        Dim bIsValid As Boolean
        Dim tt As New ToolTip()

        'check year
        bIsValid = ovalidation.validateyear(txtyear.Text)
        If bIsValid Then
            p1.Visible = False
        Else
            p1.Visible = True
            tt.SetToolTip(p1, "please select a number year")
            bAllFieldsValid = False

        End If
        'check month
        If cbbmonth.SelectedIndex = -1 Then
            p2.Visible = True
            tt.SetToolTip(p2, "please select a number month")
            bAllFieldsValid = False

        Else p2.Visible = False
        End If

        If bAllFieldsValid Then
            Debug.Print("All fields are valid")
            ocontroller.createReport5(cbbmonth.SelectedItem.ToString, txtyear.Text.ToString)
        End If
    End Sub

    Private Sub Report5_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub
End Class