﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Room
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Room))
        Me.picErrorField1 = New System.Windows.Forms.PictureBox()
        Me.radiobtnUnavailable = New System.Windows.Forms.RadioButton()
        Me.radiobtnAvailable = New System.Windows.Forms.RadioButton()
        Me.txtDescription = New System.Windows.Forms.TextBox()
        Me.picErrorField7 = New System.Windows.Forms.PictureBox()
        Me.picErrorField6 = New System.Windows.Forms.PictureBox()
        Me.picErrorField5 = New System.Windows.Forms.PictureBox()
        Me.picErrorField4 = New System.Windows.Forms.PictureBox()
        Me.picErrorField3 = New System.Windows.Forms.PictureBox()
        Me.picErrorField2 = New System.Windows.Forms.PictureBox()
        Me.btnInsert = New System.Windows.Forms.Button()
        Me.txtType = New System.Windows.Forms.TextBox()
        Me.lblroomType = New System.Windows.Forms.Label()
        Me.lblroomPrice = New System.Windows.Forms.Label()
        Me.lblroomFloor = New System.Windows.Forms.Label()
        Me.lblroomDescription = New System.Windows.Forms.Label()
        Me.lblroomRoomNumber = New System.Windows.Forms.Label()
        Me.gbxRoom = New System.Windows.Forms.GroupBox()
        Me.txtFloor = New System.Windows.Forms.TextBox()
        Me.txtNumberOfBeds = New System.Windows.Forms.TextBox()
        Me.txtPrice = New System.Windows.Forms.TextBox()
        Me.txtRoomNumber = New System.Windows.Forms.TextBox()
        Me.lblroomNumberOfBeds = New System.Windows.Forms.Label()
        Me.lblroomAvailability = New System.Windows.Forms.Label()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.BookingToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExitToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        CType(Me.picErrorField1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picErrorField7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picErrorField6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picErrorField5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picErrorField4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picErrorField3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picErrorField2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxRoom.SuspendLayout()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'picErrorField1
        '
        Me.picErrorField1.Image = CType(resources.GetObject("picErrorField1.Image"), System.Drawing.Image)
        Me.picErrorField1.Location = New System.Drawing.Point(356, 47)
        Me.picErrorField1.Name = "picErrorField1"
        Me.picErrorField1.Size = New System.Drawing.Size(20, 20)
        Me.picErrorField1.TabIndex = 14
        Me.picErrorField1.TabStop = False
        Me.picErrorField1.Visible = False
        '
        'radiobtnUnavailable
        '
        Me.radiobtnUnavailable.AutoSize = True
        Me.radiobtnUnavailable.Location = New System.Drawing.Point(241, 138)
        Me.radiobtnUnavailable.Name = "radiobtnUnavailable"
        Me.radiobtnUnavailable.Size = New System.Drawing.Size(81, 17)
        Me.radiobtnUnavailable.TabIndex = 10
        Me.radiobtnUnavailable.TabStop = True
        Me.radiobtnUnavailable.Text = "Unavailable"
        Me.radiobtnUnavailable.UseVisualStyleBackColor = True
        '
        'radiobtnAvailable
        '
        Me.radiobtnAvailable.AutoSize = True
        Me.radiobtnAvailable.Location = New System.Drawing.Point(103, 140)
        Me.radiobtnAvailable.Name = "radiobtnAvailable"
        Me.radiobtnAvailable.Size = New System.Drawing.Size(68, 17)
        Me.radiobtnAvailable.TabIndex = 9
        Me.radiobtnAvailable.TabStop = True
        Me.radiobtnAvailable.Text = "Available"
        Me.radiobtnAvailable.UseVisualStyleBackColor = True
        '
        'txtDescription
        '
        Me.txtDescription.Location = New System.Drawing.Point(103, 199)
        Me.txtDescription.Name = "txtDescription"
        Me.txtDescription.Size = New System.Drawing.Size(229, 20)
        Me.txtDescription.TabIndex = 6
        '
        'picErrorField7
        '
        Me.picErrorField7.Image = CType(resources.GetObject("picErrorField7.Image"), System.Drawing.Image)
        Me.picErrorField7.Location = New System.Drawing.Point(356, 226)
        Me.picErrorField7.Name = "picErrorField7"
        Me.picErrorField7.Size = New System.Drawing.Size(20, 20)
        Me.picErrorField7.TabIndex = 20
        Me.picErrorField7.TabStop = False
        Me.picErrorField7.Visible = False
        '
        'picErrorField6
        '
        Me.picErrorField6.Image = CType(resources.GetObject("picErrorField6.Image"), System.Drawing.Image)
        Me.picErrorField6.Location = New System.Drawing.Point(356, 195)
        Me.picErrorField6.Name = "picErrorField6"
        Me.picErrorField6.Size = New System.Drawing.Size(20, 20)
        Me.picErrorField6.TabIndex = 19
        Me.picErrorField6.TabStop = False
        Me.picErrorField6.Visible = False
        '
        'picErrorField5
        '
        Me.picErrorField5.Image = CType(resources.GetObject("picErrorField5.Image"), System.Drawing.Image)
        Me.picErrorField5.Location = New System.Drawing.Point(356, 165)
        Me.picErrorField5.Name = "picErrorField5"
        Me.picErrorField5.Size = New System.Drawing.Size(20, 20)
        Me.picErrorField5.TabIndex = 18
        Me.picErrorField5.TabStop = False
        Me.picErrorField5.Visible = False
        '
        'picErrorField4
        '
        Me.picErrorField4.Image = CType(resources.GetObject("picErrorField4.Image"), System.Drawing.Image)
        Me.picErrorField4.Location = New System.Drawing.Point(356, 135)
        Me.picErrorField4.Name = "picErrorField4"
        Me.picErrorField4.Size = New System.Drawing.Size(20, 20)
        Me.picErrorField4.TabIndex = 17
        Me.picErrorField4.TabStop = False
        Me.picErrorField4.Visible = False
        '
        'picErrorField3
        '
        Me.picErrorField3.Image = CType(resources.GetObject("picErrorField3.Image"), System.Drawing.Image)
        Me.picErrorField3.Location = New System.Drawing.Point(356, 107)
        Me.picErrorField3.Name = "picErrorField3"
        Me.picErrorField3.Size = New System.Drawing.Size(20, 20)
        Me.picErrorField3.TabIndex = 16
        Me.picErrorField3.TabStop = False
        Me.picErrorField3.Visible = False
        '
        'picErrorField2
        '
        Me.picErrorField2.Image = CType(resources.GetObject("picErrorField2.Image"), System.Drawing.Image)
        Me.picErrorField2.Location = New System.Drawing.Point(356, 78)
        Me.picErrorField2.Name = "picErrorField2"
        Me.picErrorField2.Size = New System.Drawing.Size(20, 20)
        Me.picErrorField2.TabIndex = 15
        Me.picErrorField2.TabStop = False
        Me.picErrorField2.Visible = False
        '
        'btnInsert
        '
        Me.btnInsert.Location = New System.Drawing.Point(393, 44)
        Me.btnInsert.Name = "btnInsert"
        Me.btnInsert.Size = New System.Drawing.Size(113, 23)
        Me.btnInsert.TabIndex = 13
        Me.btnInsert.Text = "Insert Room"
        Me.btnInsert.UseVisualStyleBackColor = True
        '
        'txtType
        '
        Me.txtType.Location = New System.Drawing.Point(103, 51)
        Me.txtType.Name = "txtType"
        Me.txtType.Size = New System.Drawing.Size(229, 20)
        Me.txtType.TabIndex = 2
        '
        'lblroomType
        '
        Me.lblroomType.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblroomType.Location = New System.Drawing.Point(7, 50)
        Me.lblroomType.Name = "lblroomType"
        Me.lblroomType.Size = New System.Drawing.Size(89, 20)
        Me.lblroomType.TabIndex = 2
        Me.lblroomType.Text = "Type"
        Me.lblroomType.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblroomPrice
        '
        Me.lblroomPrice.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblroomPrice.Location = New System.Drawing.Point(7, 79)
        Me.lblroomPrice.Name = "lblroomPrice"
        Me.lblroomPrice.Size = New System.Drawing.Size(89, 20)
        Me.lblroomPrice.TabIndex = 4
        Me.lblroomPrice.Text = "Price"
        Me.lblroomPrice.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblroomFloor
        '
        Me.lblroomFloor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblroomFloor.Location = New System.Drawing.Point(7, 168)
        Me.lblroomFloor.Name = "lblroomFloor"
        Me.lblroomFloor.Size = New System.Drawing.Size(89, 20)
        Me.lblroomFloor.TabIndex = 10
        Me.lblroomFloor.Text = "Floor"
        Me.lblroomFloor.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblroomDescription
        '
        Me.lblroomDescription.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblroomDescription.Location = New System.Drawing.Point(7, 199)
        Me.lblroomDescription.Name = "lblroomDescription"
        Me.lblroomDescription.Size = New System.Drawing.Size(89, 20)
        Me.lblroomDescription.TabIndex = 12
        Me.lblroomDescription.Text = "Description"
        Me.lblroomDescription.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblroomRoomNumber
        '
        Me.lblroomRoomNumber.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblroomRoomNumber.Location = New System.Drawing.Point(7, 20)
        Me.lblroomRoomNumber.Name = "lblroomRoomNumber"
        Me.lblroomRoomNumber.Size = New System.Drawing.Size(89, 20)
        Me.lblroomRoomNumber.TabIndex = 0
        Me.lblroomRoomNumber.Text = "Room Number"
        Me.lblroomRoomNumber.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'gbxRoom
        '
        Me.gbxRoom.BackColor = System.Drawing.SystemColors.ControlLight
        Me.gbxRoom.Controls.Add(Me.txtFloor)
        Me.gbxRoom.Controls.Add(Me.txtNumberOfBeds)
        Me.gbxRoom.Controls.Add(Me.txtPrice)
        Me.gbxRoom.Controls.Add(Me.txtRoomNumber)
        Me.gbxRoom.Controls.Add(Me.radiobtnUnavailable)
        Me.gbxRoom.Controls.Add(Me.radiobtnAvailable)
        Me.gbxRoom.Controls.Add(Me.txtDescription)
        Me.gbxRoom.Controls.Add(Me.txtType)
        Me.gbxRoom.Controls.Add(Me.lblroomType)
        Me.gbxRoom.Controls.Add(Me.lblroomPrice)
        Me.gbxRoom.Controls.Add(Me.lblroomNumberOfBeds)
        Me.gbxRoom.Controls.Add(Me.lblroomAvailability)
        Me.gbxRoom.Controls.Add(Me.lblroomFloor)
        Me.gbxRoom.Controls.Add(Me.lblroomDescription)
        Me.gbxRoom.Controls.Add(Me.lblroomRoomNumber)
        Me.gbxRoom.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbxRoom.Location = New System.Drawing.Point(12, 27)
        Me.gbxRoom.Name = "gbxRoom"
        Me.gbxRoom.Size = New System.Drawing.Size(338, 254)
        Me.gbxRoom.TabIndex = 12
        Me.gbxRoom.TabStop = False
        Me.gbxRoom.Text = "Room Details"
        '
        'txtFloor
        '
        Me.txtFloor.Location = New System.Drawing.Point(103, 168)
        Me.txtFloor.MaxLength = 2
        Me.txtFloor.Name = "txtFloor"
        Me.txtFloor.Size = New System.Drawing.Size(228, 20)
        Me.txtFloor.TabIndex = 5
        '
        'txtNumberOfBeds
        '
        Me.txtNumberOfBeds.Location = New System.Drawing.Point(103, 108)
        Me.txtNumberOfBeds.MaxLength = 2
        Me.txtNumberOfBeds.Name = "txtNumberOfBeds"
        Me.txtNumberOfBeds.Size = New System.Drawing.Size(228, 20)
        Me.txtNumberOfBeds.TabIndex = 4
        '
        'txtPrice
        '
        Me.txtPrice.Location = New System.Drawing.Point(103, 78)
        Me.txtPrice.MaxLength = 7
        Me.txtPrice.Name = "txtPrice"
        Me.txtPrice.Size = New System.Drawing.Size(228, 20)
        Me.txtPrice.TabIndex = 3
        '
        'txtRoomNumber
        '
        Me.txtRoomNumber.Location = New System.Drawing.Point(103, 20)
        Me.txtRoomNumber.MaxLength = 3
        Me.txtRoomNumber.Name = "txtRoomNumber"
        Me.txtRoomNumber.Size = New System.Drawing.Size(228, 20)
        Me.txtRoomNumber.TabIndex = 1
        '
        'lblroomNumberOfBeds
        '
        Me.lblroomNumberOfBeds.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblroomNumberOfBeds.Location = New System.Drawing.Point(7, 108)
        Me.lblroomNumberOfBeds.Name = "lblroomNumberOfBeds"
        Me.lblroomNumberOfBeds.Size = New System.Drawing.Size(89, 20)
        Me.lblroomNumberOfBeds.TabIndex = 6
        Me.lblroomNumberOfBeds.Text = "Number of Beds"
        Me.lblroomNumberOfBeds.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblroomAvailability
        '
        Me.lblroomAvailability.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblroomAvailability.Location = New System.Drawing.Point(7, 138)
        Me.lblroomAvailability.Name = "lblroomAvailability"
        Me.lblroomAvailability.Size = New System.Drawing.Size(89, 20)
        Me.lblroomAvailability.TabIndex = 8
        Me.lblroomAvailability.Text = "Availability"
        Me.lblroomAvailability.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItem1})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(518, 24)
        Me.MenuStrip1.TabIndex = 89
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BookingToolStripMenuItem, Me.ExitToolStripMenuItem})
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        Me.ToolStripMenuItem1.Size = New System.Drawing.Size(37, 20)
        Me.ToolStripMenuItem1.Text = "File"
        '
        'BookingToolStripMenuItem
        '
        Me.BookingToolStripMenuItem.Name = "BookingToolStripMenuItem"
        Me.BookingToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.BookingToolStripMenuItem.Text = "Booking"
        '
        'ExitToolStripMenuItem
        '
        Me.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem"
        Me.ExitToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.ExitToolStripMenuItem.Text = "Exit"
        '
        'Room
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(518, 299)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Controls.Add(Me.picErrorField1)
        Me.Controls.Add(Me.picErrorField7)
        Me.Controls.Add(Me.picErrorField6)
        Me.Controls.Add(Me.picErrorField5)
        Me.Controls.Add(Me.picErrorField4)
        Me.Controls.Add(Me.picErrorField3)
        Me.Controls.Add(Me.picErrorField2)
        Me.Controls.Add(Me.btnInsert)
        Me.Controls.Add(Me.gbxRoom)
        Me.Name = "Room"
        Me.Text = "Room"
        CType(Me.picErrorField1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picErrorField7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picErrorField6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picErrorField5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picErrorField4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picErrorField3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picErrorField2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxRoom.ResumeLayout(False)
        Me.gbxRoom.PerformLayout()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents picErrorField1 As PictureBox
    Friend WithEvents radiobtnUnavailable As RadioButton
    Friend WithEvents radiobtnAvailable As RadioButton
    Friend WithEvents txtDescription As TextBox
    Friend WithEvents picErrorField7 As PictureBox
    Friend WithEvents picErrorField6 As PictureBox
    Friend WithEvents picErrorField5 As PictureBox
    Friend WithEvents picErrorField4 As PictureBox
    Friend WithEvents picErrorField3 As PictureBox
    Friend WithEvents picErrorField2 As PictureBox
    Friend WithEvents btnInsert As Button
    Friend WithEvents txtType As TextBox
    Friend WithEvents lblroomType As Label
    Friend WithEvents lblroomPrice As Label
    Friend WithEvents lblroomFloor As Label
    Friend WithEvents lblroomDescription As Label
    Friend WithEvents lblroomRoomNumber As Label
    Friend WithEvents gbxRoom As GroupBox
    Friend WithEvents lblroomNumberOfBeds As Label
    Friend WithEvents lblroomAvailability As Label
    Friend WithEvents txtFloor As TextBox
    Friend WithEvents txtNumberOfBeds As TextBox
    Friend WithEvents txtPrice As TextBox
    Friend WithEvents txtRoomNumber As TextBox
    Friend WithEvents MenuStrip1 As MenuStrip
    Friend WithEvents ToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents BookingToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ExitToolStripMenuItem As ToolStripMenuItem
End Class
