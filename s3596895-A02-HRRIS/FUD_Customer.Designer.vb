﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FUD_Customer
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FUD_Customer))
        Me.CustomerBindingSource2 = New System.Windows.Forms.BindingSource(Me.components)
        Me.DatabaseDataSetBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DatabaseDataSet = New s3596895_A02_HRRIS.DatabaseDataSet()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.picErrorField1 = New System.Windows.Forms.PictureBox()
        Me.picErrorField6 = New System.Windows.Forms.PictureBox()
        Me.picErrorField3 = New System.Windows.Forms.PictureBox()
        Me.picErrorField2 = New System.Windows.Forms.PictureBox()
        Me.picErrorField5 = New System.Windows.Forms.PictureBox()
        Me.picErrorField4 = New System.Windows.Forms.PictureBox()
        Me.radiobtnFemale = New System.Windows.Forms.RadioButton()
        Me.radiobtnMale = New System.Windows.Forms.RadioButton()
        Me.DobValue = New System.Windows.Forms.DateTimePicker()
        Me.txttitle = New System.Windows.Forms.TextBox()
        Me.txtcustomerid = New System.Windows.Forms.TextBox()
        Me.txtPhone = New System.Windows.Forms.TextBox()
        Me.txtEmail = New System.Windows.Forms.TextBox()
        Me.txtAddress = New System.Windows.Forms.TextBox()
        Me.txtFirstName = New System.Windows.Forms.TextBox()
        Me.txtLastName = New System.Windows.Forms.TextBox()
        Me.lbltitle = New System.Windows.Forms.Label()
        Me.lblcustomerid = New System.Windows.Forms.Label()
        Me.lblcusLastName = New System.Windows.Forms.Label()
        Me.lblcusPhone = New System.Windows.Forms.Label()
        Me.lblcusAddress = New System.Windows.Forms.Label()
        Me.lblcusEmail = New System.Windows.Forms.Label()
        Me.lblcusDob = New System.Windows.Forms.Label()
        Me.lblcusFirstName = New System.Windows.Forms.Label()
        Me.lblcusGender = New System.Windows.Forms.Label()
        Me.lblFindBy = New System.Windows.Forms.Label()
        Me.gbxFind = New System.Windows.Forms.GroupBox()
        Me.btnsearchcustomer = New System.Windows.Forms.Button()
        Me.picErrorField = New System.Windows.Forms.PictureBox()
        Me.txtfindcustomervalue = New System.Windows.Forms.TextBox()
        Me.cbbFindBy = New System.Windows.Forms.ComboBox()
        Me.btntDelete = New System.Windows.Forms.Button()
        Me.btnUpdate = New System.Windows.Forms.Button()
        Me.BackgroundWorker1 = New System.ComponentModel.BackgroundWorker()
        Me.BtnPrev = New System.Windows.Forms.Button()
        Me.BtnNext = New System.Windows.Forms.Button()
        Me.CustomerTableAdapter = New s3596895_A02_HRRIS.DatabaseDataSetTableAdapters.customerTableAdapter()
        Me.dgvCustomer = New System.Windows.Forms.DataGridView()
        Me.CustomerBindingSource8 = New System.Windows.Forms.BindingSource(Me.components)
        Me.DatabaseDataSet3 = New s3596895_A02_HRRIS.DatabaseDataSet3()
        Me.CustomerBindingSource5 = New System.Windows.Forms.BindingSource(Me.components)
        Me.CustomerBindingSource4 = New System.Windows.Forms.BindingSource(Me.components)
        Me.BookingTableAdapter = New s3596895_A02_HRRIS.DatabaseDataSetTableAdapters.bookingTableAdapter()
        Me.CustomerBindingSource3 = New System.Windows.Forms.BindingSource(Me.components)
        Me.DatabaseDataSet2 = New s3596895_A02_HRRIS.DatabaseDataSet2()
        Me.DatabaseDataSet2BindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DatabaseDataSet2BindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.CustomerBindingSource7 = New System.Windows.Forms.BindingSource(Me.components)
        Me.CustomerTableAdapter2 = New s3596895_A02_HRRIS.DatabaseDataSet2TableAdapters.customerTableAdapter()
        Me.CustomerTableAdapter3 = New s3596895_A02_HRRIS.DatabaseDataSet3TableAdapters.customerTableAdapter()
        Me.btnsave = New System.Windows.Forms.Button()
        Me.CustomerBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CustomerBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.CustomerbookingBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CustomerBindingSource6 = New System.Windows.Forms.BindingSource(Me.components)
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.FileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.btnfirst = New System.Windows.Forms.Button()
        Me.btnlast = New System.Windows.Forms.Button()
        CType(Me.CustomerBindingSource2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DatabaseDataSetBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DatabaseDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.picErrorField1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picErrorField6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picErrorField3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picErrorField2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picErrorField5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picErrorField4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxFind.SuspendLayout()
        CType(Me.picErrorField, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvCustomer, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CustomerBindingSource8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DatabaseDataSet3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CustomerBindingSource5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CustomerBindingSource4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CustomerBindingSource3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DatabaseDataSet2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DatabaseDataSet2BindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DatabaseDataSet2BindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CustomerBindingSource7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CustomerBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CustomerBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CustomerbookingBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CustomerBindingSource6, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'CustomerBindingSource2
        '
        Me.CustomerBindingSource2.DataMember = "customer"
        Me.CustomerBindingSource2.DataSource = Me.DatabaseDataSetBindingSource
        '
        'DatabaseDataSetBindingSource
        '
        Me.DatabaseDataSetBindingSource.DataSource = Me.DatabaseDataSet
        Me.DatabaseDataSetBindingSource.Position = 0
        '
        'DatabaseDataSet
        '
        Me.DatabaseDataSet.DataSetName = "DatabaseDataSet"
        Me.DatabaseDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.picErrorField1)
        Me.GroupBox1.Controls.Add(Me.picErrorField6)
        Me.GroupBox1.Controls.Add(Me.picErrorField3)
        Me.GroupBox1.Controls.Add(Me.picErrorField2)
        Me.GroupBox1.Controls.Add(Me.picErrorField5)
        Me.GroupBox1.Controls.Add(Me.picErrorField4)
        Me.GroupBox1.Controls.Add(Me.radiobtnFemale)
        Me.GroupBox1.Controls.Add(Me.radiobtnMale)
        Me.GroupBox1.Controls.Add(Me.DobValue)
        Me.GroupBox1.Controls.Add(Me.txttitle)
        Me.GroupBox1.Controls.Add(Me.txtcustomerid)
        Me.GroupBox1.Controls.Add(Me.txtPhone)
        Me.GroupBox1.Controls.Add(Me.txtEmail)
        Me.GroupBox1.Controls.Add(Me.txtAddress)
        Me.GroupBox1.Controls.Add(Me.txtFirstName)
        Me.GroupBox1.Controls.Add(Me.txtLastName)
        Me.GroupBox1.Controls.Add(Me.lbltitle)
        Me.GroupBox1.Controls.Add(Me.lblcustomerid)
        Me.GroupBox1.Controls.Add(Me.lblcusLastName)
        Me.GroupBox1.Controls.Add(Me.lblcusPhone)
        Me.GroupBox1.Controls.Add(Me.lblcusAddress)
        Me.GroupBox1.Controls.Add(Me.lblcusEmail)
        Me.GroupBox1.Controls.Add(Me.lblcusDob)
        Me.GroupBox1.Controls.Add(Me.lblcusFirstName)
        Me.GroupBox1.Controls.Add(Me.lblcusGender)
        Me.GroupBox1.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.GroupBox1.Location = New System.Drawing.Point(2, 34)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(403, 317)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Customer Information"
        '
        'picErrorField1
        '
        Me.picErrorField1.Image = CType(resources.GetObject("picErrorField1.Image"), System.Drawing.Image)
        Me.picErrorField1.Location = New System.Drawing.Point(374, 102)
        Me.picErrorField1.Name = "picErrorField1"
        Me.picErrorField1.Size = New System.Drawing.Size(20, 20)
        Me.picErrorField1.TabIndex = 168
        Me.picErrorField1.TabStop = False
        Me.picErrorField1.Visible = False
        '
        'picErrorField6
        '
        Me.picErrorField6.Image = CType(resources.GetObject("picErrorField6.Image"), System.Drawing.Image)
        Me.picErrorField6.Location = New System.Drawing.Point(373, 283)
        Me.picErrorField6.Name = "picErrorField6"
        Me.picErrorField6.Size = New System.Drawing.Size(20, 20)
        Me.picErrorField6.TabIndex = 173
        Me.picErrorField6.TabStop = False
        Me.picErrorField6.Visible = False
        '
        'picErrorField3
        '
        Me.picErrorField3.Image = CType(resources.GetObject("picErrorField3.Image"), System.Drawing.Image)
        Me.picErrorField3.Location = New System.Drawing.Point(373, 158)
        Me.picErrorField3.Name = "picErrorField3"
        Me.picErrorField3.Size = New System.Drawing.Size(20, 20)
        Me.picErrorField3.TabIndex = 171
        Me.picErrorField3.TabStop = False
        Me.picErrorField3.Visible = False
        '
        'picErrorField2
        '
        Me.picErrorField2.Image = CType(resources.GetObject("picErrorField2.Image"), System.Drawing.Image)
        Me.picErrorField2.Location = New System.Drawing.Point(373, 131)
        Me.picErrorField2.Name = "picErrorField2"
        Me.picErrorField2.Size = New System.Drawing.Size(20, 20)
        Me.picErrorField2.TabIndex = 170
        Me.picErrorField2.TabStop = False
        Me.picErrorField2.Visible = False
        '
        'picErrorField5
        '
        Me.picErrorField5.Image = CType(resources.GetObject("picErrorField5.Image"), System.Drawing.Image)
        Me.picErrorField5.Location = New System.Drawing.Point(374, 221)
        Me.picErrorField5.Name = "picErrorField5"
        Me.picErrorField5.Size = New System.Drawing.Size(20, 20)
        Me.picErrorField5.TabIndex = 169
        Me.picErrorField5.TabStop = False
        Me.picErrorField5.Visible = False
        '
        'picErrorField4
        '
        Me.picErrorField4.Image = CType(resources.GetObject("picErrorField4.Image"), System.Drawing.Image)
        Me.picErrorField4.Location = New System.Drawing.Point(373, 192)
        Me.picErrorField4.Name = "picErrorField4"
        Me.picErrorField4.Size = New System.Drawing.Size(20, 20)
        Me.picErrorField4.TabIndex = 172
        Me.picErrorField4.TabStop = False
        Me.picErrorField4.Visible = False
        '
        'radiobtnFemale
        '
        Me.radiobtnFemale.AutoSize = True
        Me.radiobtnFemale.Enabled = False
        Me.radiobtnFemale.Location = New System.Drawing.Point(298, 286)
        Me.radiobtnFemale.Name = "radiobtnFemale"
        Me.radiobtnFemale.Size = New System.Drawing.Size(59, 17)
        Me.radiobtnFemale.TabIndex = 167
        Me.radiobtnFemale.Text = "Female"
        Me.radiobtnFemale.UseVisualStyleBackColor = True
        '
        'radiobtnMale
        '
        Me.radiobtnMale.AutoSize = True
        Me.radiobtnMale.Enabled = False
        Me.radiobtnMale.Location = New System.Drawing.Point(99, 286)
        Me.radiobtnMale.Name = "radiobtnMale"
        Me.radiobtnMale.Size = New System.Drawing.Size(48, 17)
        Me.radiobtnMale.TabIndex = 166
        Me.radiobtnMale.Text = "Male"
        Me.radiobtnMale.UseVisualStyleBackColor = True
        '
        'DobValue
        '
        Me.DobValue.Enabled = False
        Me.DobValue.Location = New System.Drawing.Point(99, 251)
        Me.DobValue.Name = "DobValue"
        Me.DobValue.Size = New System.Drawing.Size(258, 20)
        Me.DobValue.TabIndex = 165
        Me.DobValue.Value = New Date(2017, 12, 18, 0, 0, 0, 0)
        '
        'txttitle
        '
        Me.txttitle.Enabled = False
        Me.txttitle.Location = New System.Drawing.Point(98, 58)
        Me.txttitle.Name = "txttitle"
        Me.txttitle.Size = New System.Drawing.Size(258, 20)
        Me.txttitle.TabIndex = 164
        '
        'txtcustomerid
        '
        Me.txtcustomerid.Enabled = False
        Me.txtcustomerid.Location = New System.Drawing.Point(98, 29)
        Me.txtcustomerid.Name = "txtcustomerid"
        Me.txtcustomerid.Size = New System.Drawing.Size(258, 20)
        Me.txtcustomerid.TabIndex = 163
        '
        'txtPhone
        '
        Me.txtPhone.Enabled = False
        Me.txtPhone.Location = New System.Drawing.Point(99, 158)
        Me.txtPhone.MaxLength = 9
        Me.txtPhone.Name = "txtPhone"
        Me.txtPhone.Size = New System.Drawing.Size(257, 20)
        Me.txtPhone.TabIndex = 160
        '
        'txtEmail
        '
        Me.txtEmail.Enabled = False
        Me.txtEmail.Location = New System.Drawing.Point(98, 221)
        Me.txtEmail.Name = "txtEmail"
        Me.txtEmail.Size = New System.Drawing.Size(258, 20)
        Me.txtEmail.TabIndex = 162
        '
        'txtAddress
        '
        Me.txtAddress.Enabled = False
        Me.txtAddress.Location = New System.Drawing.Point(99, 192)
        Me.txtAddress.Name = "txtAddress"
        Me.txtAddress.Size = New System.Drawing.Size(258, 20)
        Me.txtAddress.TabIndex = 161
        '
        'txtFirstName
        '
        Me.txtFirstName.Enabled = False
        Me.txtFirstName.Location = New System.Drawing.Point(98, 102)
        Me.txtFirstName.Name = "txtFirstName"
        Me.txtFirstName.Size = New System.Drawing.Size(258, 20)
        Me.txtFirstName.TabIndex = 158
        '
        'txtLastName
        '
        Me.txtLastName.Enabled = False
        Me.txtLastName.Location = New System.Drawing.Point(98, 132)
        Me.txtLastName.Name = "txtLastName"
        Me.txtLastName.Size = New System.Drawing.Size(258, 20)
        Me.txtLastName.TabIndex = 159
        '
        'lbltitle
        '
        Me.lbltitle.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lbltitle.Location = New System.Drawing.Point(7, 57)
        Me.lbltitle.Name = "lbltitle"
        Me.lbltitle.Size = New System.Drawing.Size(68, 20)
        Me.lbltitle.TabIndex = 27
        Me.lbltitle.Text = "Title"
        Me.lbltitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblcustomerid
        '
        Me.lblcustomerid.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblcustomerid.Location = New System.Drawing.Point(7, 28)
        Me.lblcustomerid.Name = "lblcustomerid"
        Me.lblcustomerid.Size = New System.Drawing.Size(68, 20)
        Me.lblcustomerid.TabIndex = 26
        Me.lblcustomerid.Text = "Customer ID"
        Me.lblcustomerid.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblcusLastName
        '
        Me.lblcusLastName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblcusLastName.Location = New System.Drawing.Point(7, 131)
        Me.lblcusLastName.Name = "lblcusLastName"
        Me.lblcusLastName.Size = New System.Drawing.Size(68, 20)
        Me.lblcusLastName.TabIndex = 21
        Me.lblcusLastName.Text = "Last Name"
        Me.lblcusLastName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblcusPhone
        '
        Me.lblcusPhone.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblcusPhone.Location = New System.Drawing.Point(7, 161)
        Me.lblcusPhone.Name = "lblcusPhone"
        Me.lblcusPhone.Size = New System.Drawing.Size(68, 20)
        Me.lblcusPhone.TabIndex = 22
        Me.lblcusPhone.Text = "Phone"
        Me.lblcusPhone.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblcusAddress
        '
        Me.lblcusAddress.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblcusAddress.Location = New System.Drawing.Point(7, 192)
        Me.lblcusAddress.Name = "lblcusAddress"
        Me.lblcusAddress.Size = New System.Drawing.Size(68, 20)
        Me.lblcusAddress.TabIndex = 23
        Me.lblcusAddress.Text = "Address"
        Me.lblcusAddress.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblcusEmail
        '
        Me.lblcusEmail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblcusEmail.Location = New System.Drawing.Point(7, 221)
        Me.lblcusEmail.Name = "lblcusEmail"
        Me.lblcusEmail.Size = New System.Drawing.Size(68, 20)
        Me.lblcusEmail.TabIndex = 24
        Me.lblcusEmail.Text = "Email"
        Me.lblcusEmail.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblcusDob
        '
        Me.lblcusDob.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblcusDob.Location = New System.Drawing.Point(7, 253)
        Me.lblcusDob.Name = "lblcusDob"
        Me.lblcusDob.Size = New System.Drawing.Size(68, 20)
        Me.lblcusDob.TabIndex = 25
        Me.lblcusDob.Text = "Dob"
        Me.lblcusDob.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblcusFirstName
        '
        Me.lblcusFirstName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblcusFirstName.Location = New System.Drawing.Point(7, 102)
        Me.lblcusFirstName.Name = "lblcusFirstName"
        Me.lblcusFirstName.Size = New System.Drawing.Size(68, 20)
        Me.lblcusFirstName.TabIndex = 20
        Me.lblcusFirstName.Text = "First Name"
        Me.lblcusFirstName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblcusGender
        '
        Me.lblcusGender.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblcusGender.Location = New System.Drawing.Point(7, 283)
        Me.lblcusGender.Name = "lblcusGender"
        Me.lblcusGender.Size = New System.Drawing.Size(68, 20)
        Me.lblcusGender.TabIndex = 19
        Me.lblcusGender.Text = "Gender"
        Me.lblcusGender.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblFindBy
        '
        Me.lblFindBy.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblFindBy.Location = New System.Drawing.Point(6, 17)
        Me.lblFindBy.Name = "lblFindBy"
        Me.lblFindBy.Size = New System.Drawing.Size(68, 20)
        Me.lblFindBy.TabIndex = 27
        Me.lblFindBy.Text = "Find By"
        Me.lblFindBy.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'gbxFind
        '
        Me.gbxFind.Controls.Add(Me.btnsearchcustomer)
        Me.gbxFind.Controls.Add(Me.picErrorField)
        Me.gbxFind.Controls.Add(Me.txtfindcustomervalue)
        Me.gbxFind.Controls.Add(Me.cbbFindBy)
        Me.gbxFind.Controls.Add(Me.lblFindBy)
        Me.gbxFind.Location = New System.Drawing.Point(427, 34)
        Me.gbxFind.Name = "gbxFind"
        Me.gbxFind.Size = New System.Drawing.Size(512, 85)
        Me.gbxFind.TabIndex = 28
        Me.gbxFind.TabStop = False
        Me.gbxFind.Text = "Find"
        '
        'btnsearchcustomer
        '
        Me.btnsearchcustomer.Location = New System.Drawing.Point(210, 54)
        Me.btnsearchcustomer.Name = "btnsearchcustomer"
        Me.btnsearchcustomer.Size = New System.Drawing.Size(96, 23)
        Me.btnsearchcustomer.TabIndex = 147
        Me.btnsearchcustomer.Text = "Search"
        Me.btnsearchcustomer.UseVisualStyleBackColor = True
        '
        'picErrorField
        '
        Me.picErrorField.Image = CType(resources.GetObject("picErrorField.Image"), System.Drawing.Image)
        Me.picErrorField.Location = New System.Drawing.Point(488, 16)
        Me.picErrorField.Name = "picErrorField"
        Me.picErrorField.Size = New System.Drawing.Size(20, 20)
        Me.picErrorField.TabIndex = 146
        Me.picErrorField.TabStop = False
        Me.picErrorField.Visible = False
        '
        'txtfindcustomervalue
        '
        Me.txtfindcustomervalue.Enabled = False
        Me.txtfindcustomervalue.Location = New System.Drawing.Point(264, 16)
        Me.txtfindcustomervalue.Name = "txtfindcustomervalue"
        Me.txtfindcustomervalue.Size = New System.Drawing.Size(218, 20)
        Me.txtfindcustomervalue.TabIndex = 144
        '
        'cbbFindBy
        '
        Me.cbbFindBy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbbFindBy.Items.AddRange(New Object() {"customer first name", "customer ID", "customer last name"})
        Me.cbbFindBy.Location = New System.Drawing.Point(80, 16)
        Me.cbbFindBy.Name = "cbbFindBy"
        Me.cbbFindBy.Size = New System.Drawing.Size(176, 21)
        Me.cbbFindBy.TabIndex = 143
        '
        'btntDelete
        '
        Me.btntDelete.Location = New System.Drawing.Point(433, 162)
        Me.btntDelete.Name = "btntDelete"
        Me.btntDelete.Size = New System.Drawing.Size(96, 23)
        Me.btntDelete.TabIndex = 159
        Me.btntDelete.Text = "Delete"
        Me.btntDelete.UseVisualStyleBackColor = True
        '
        'btnUpdate
        '
        Me.btnUpdate.Location = New System.Drawing.Point(839, 165)
        Me.btnUpdate.Name = "btnUpdate"
        Me.btnUpdate.Size = New System.Drawing.Size(96, 23)
        Me.btnUpdate.TabIndex = 158
        Me.btnUpdate.Text = "Update"
        Me.btnUpdate.UseVisualStyleBackColor = True
        '
        'BtnPrev
        '
        Me.BtnPrev.Location = New System.Drawing.Point(571, 320)
        Me.BtnPrev.Name = "BtnPrev"
        Me.BtnPrev.Size = New System.Drawing.Size(96, 23)
        Me.BtnPrev.TabIndex = 162
        Me.BtnPrev.Text = "Previous Record"
        Me.BtnPrev.UseVisualStyleBackColor = True
        '
        'BtnNext
        '
        Me.BtnNext.Location = New System.Drawing.Point(703, 320)
        Me.BtnNext.Name = "BtnNext"
        Me.BtnNext.Size = New System.Drawing.Size(96, 23)
        Me.BtnNext.TabIndex = 163
        Me.BtnNext.Text = "Next Record"
        Me.BtnNext.UseVisualStyleBackColor = True
        '
        'CustomerTableAdapter
        '
        Me.CustomerTableAdapter.ClearBeforeFill = True
        '
        'dgvCustomer
        '
        Me.dgvCustomer.AllowUserToAddRows = False
        Me.dgvCustomer.AllowUserToDeleteRows = False
        Me.dgvCustomer.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvCustomer.Location = New System.Drawing.Point(2, 355)
        Me.dgvCustomer.Name = "dgvCustomer"
        Me.dgvCustomer.ReadOnly = True
        Me.dgvCustomer.Size = New System.Drawing.Size(937, 254)
        Me.dgvCustomer.TabIndex = 0
        '
        'CustomerBindingSource8
        '
        Me.CustomerBindingSource8.DataMember = "customer"
        Me.CustomerBindingSource8.DataSource = Me.DatabaseDataSet3
        '
        'DatabaseDataSet3
        '
        Me.DatabaseDataSet3.DataSetName = "DatabaseDataSet3"
        Me.DatabaseDataSet3.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'CustomerBindingSource5
        '
        Me.CustomerBindingSource5.DataMember = "customer"
        Me.CustomerBindingSource5.DataSource = Me.DatabaseDataSetBindingSource
        '
        'CustomerBindingSource4
        '
        Me.CustomerBindingSource4.DataMember = "customer"
        Me.CustomerBindingSource4.DataSource = Me.DatabaseDataSetBindingSource
        '
        'BookingTableAdapter
        '
        Me.BookingTableAdapter.ClearBeforeFill = True
        '
        'CustomerBindingSource3
        '
        Me.CustomerBindingSource3.DataMember = "customer"
        Me.CustomerBindingSource3.DataSource = Me.DatabaseDataSet
        '
        'DatabaseDataSet2
        '
        Me.DatabaseDataSet2.DataSetName = "DatabaseDataSet2"
        Me.DatabaseDataSet2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'DatabaseDataSet2BindingSource
        '
        Me.DatabaseDataSet2BindingSource.DataSource = Me.DatabaseDataSet2
        Me.DatabaseDataSet2BindingSource.Position = 0
        '
        'DatabaseDataSet2BindingSource1
        '
        Me.DatabaseDataSet2BindingSource1.DataSource = Me.DatabaseDataSet2
        Me.DatabaseDataSet2BindingSource1.Position = 0
        '
        'CustomerBindingSource7
        '
        Me.CustomerBindingSource7.DataMember = "customer"
        Me.CustomerBindingSource7.DataSource = Me.DatabaseDataSet2
        '
        'CustomerTableAdapter2
        '
        Me.CustomerTableAdapter2.ClearBeforeFill = True
        '
        'CustomerTableAdapter3
        '
        Me.CustomerTableAdapter3.ClearBeforeFill = True
        '
        'btnsave
        '
        Me.btnsave.Location = New System.Drawing.Point(637, 166)
        Me.btnsave.Name = "btnsave"
        Me.btnsave.Size = New System.Drawing.Size(96, 23)
        Me.btnsave.TabIndex = 166
        Me.btnsave.Text = "Save update"
        Me.btnsave.UseVisualStyleBackColor = True
        '
        'CustomerBindingSource
        '
        Me.CustomerBindingSource.DataMember = "customer"
        '
        'CustomerBindingSource1
        '
        Me.CustomerBindingSource1.DataMember = "customer"
        '
        'CustomerbookingBindingSource
        '
        Me.CustomerbookingBindingSource.DataMember = "customerbooking"
        Me.CustomerbookingBindingSource.DataSource = Me.CustomerBindingSource2
        '
        'CustomerBindingSource6
        '
        Me.CustomerBindingSource6.DataMember = "customer"
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FileToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(942, 24)
        Me.MenuStrip1.TabIndex = 167
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'FileToolStripMenuItem
        '
        Me.FileToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MenuToolStripMenuItem})
        Me.FileToolStripMenuItem.Name = "FileToolStripMenuItem"
        Me.FileToolStripMenuItem.Size = New System.Drawing.Size(37, 20)
        Me.FileToolStripMenuItem.Text = "File"
        '
        'MenuToolStripMenuItem
        '
        Me.MenuToolStripMenuItem.Name = "MenuToolStripMenuItem"
        Me.MenuToolStripMenuItem.Size = New System.Drawing.Size(105, 22)
        Me.MenuToolStripMenuItem.Text = "Menu"
        '
        'btnfirst
        '
        Me.btnfirst.Location = New System.Drawing.Point(433, 320)
        Me.btnfirst.Name = "btnfirst"
        Me.btnfirst.Size = New System.Drawing.Size(96, 23)
        Me.btnfirst.TabIndex = 168
        Me.btnfirst.Text = "First record"
        Me.btnfirst.UseVisualStyleBackColor = True
        '
        'btnlast
        '
        Me.btnlast.Location = New System.Drawing.Point(834, 320)
        Me.btnlast.Name = "btnlast"
        Me.btnlast.Size = New System.Drawing.Size(96, 23)
        Me.btnlast.TabIndex = 169
        Me.btnlast.Text = "Last Record"
        Me.btnlast.UseVisualStyleBackColor = True
        '
        'FUD_Customer
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(942, 611)
        Me.Controls.Add(Me.btnlast)
        Me.Controls.Add(Me.btnfirst)
        Me.Controls.Add(Me.btnsave)
        Me.Controls.Add(Me.BtnNext)
        Me.Controls.Add(Me.BtnPrev)
        Me.Controls.Add(Me.btntDelete)
        Me.Controls.Add(Me.btnUpdate)
        Me.Controls.Add(Me.gbxFind)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.dgvCustomer)
        Me.Controls.Add(Me.MenuStrip1)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "FUD_Customer"
        Me.Text = "FUD Customer"
        CType(Me.CustomerBindingSource2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DatabaseDataSetBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DatabaseDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.picErrorField1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picErrorField6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picErrorField3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picErrorField2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picErrorField5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picErrorField4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxFind.ResumeLayout(False)
        Me.gbxFind.PerformLayout()
        CType(Me.picErrorField, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvCustomer, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CustomerBindingSource8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DatabaseDataSet3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CustomerBindingSource5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CustomerBindingSource4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CustomerBindingSource3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DatabaseDataSet2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DatabaseDataSet2BindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DatabaseDataSet2BindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CustomerBindingSource7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CustomerBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CustomerBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CustomerbookingBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CustomerBindingSource6, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents lbltitle As Label
    Friend WithEvents lblcustomerid As Label
    Friend WithEvents lblcusLastName As Label
    Friend WithEvents lblcusPhone As Label
    Friend WithEvents lblcusAddress As Label
    Friend WithEvents lblcusEmail As Label
    Friend WithEvents lblcusDob As Label
    Friend WithEvents lblcusFirstName As Label
    Friend WithEvents lblcusGender As Label
    Friend WithEvents txttitle As TextBox
    Friend WithEvents txtcustomerid As TextBox
    Friend WithEvents txtPhone As TextBox
    Friend WithEvents txtEmail As TextBox
    Friend WithEvents txtAddress As TextBox
    Friend WithEvents txtFirstName As TextBox
    Friend WithEvents txtLastName As TextBox
    Friend WithEvents DobValue As DateTimePicker
    Friend WithEvents radiobtnFemale As RadioButton
    Friend WithEvents radiobtnMale As RadioButton
    Friend WithEvents picErrorField1 As PictureBox
    Friend WithEvents picErrorField6 As PictureBox
    Friend WithEvents picErrorField3 As PictureBox
    Friend WithEvents picErrorField2 As PictureBox
    Friend WithEvents picErrorField5 As PictureBox
    Friend WithEvents picErrorField4 As PictureBox
    Friend WithEvents lblFindBy As Label
    Friend WithEvents gbxFind As GroupBox
    Friend WithEvents cbbFindBy As ComboBox
    Friend WithEvents txtfindcustomervalue As TextBox
    Friend WithEvents picErrorField As PictureBox
    Friend WithEvents btnsearchcustomer As Button
    Friend WithEvents btntDelete As Button
    Friend WithEvents btnUpdate As Button
    Friend WithEvents BackgroundWorker1 As System.ComponentModel.BackgroundWorker
    Friend WithEvents BtnPrev As Button
    Friend WithEvents BtnNext As Button

    Friend WithEvents CustomerBindingSource As BindingSource
    Friend WithEvents CustomerBindingSource1 As BindingSource
    Friend WithEvents DatabaseDataSetBindingSource As BindingSource
    Friend WithEvents DatabaseDataSet As DatabaseDataSet
    Friend WithEvents CustomerBindingSource2 As BindingSource
    Friend WithEvents CustomerTableAdapter As DatabaseDataSetTableAdapters.customerTableAdapter
    Friend WithEvents dgvCustomer As DataGridView
    Friend WithEvents CustomerbookingBindingSource As BindingSource
    Friend WithEvents BookingTableAdapter As DatabaseDataSetTableAdapters.bookingTableAdapter
    Friend WithEvents CustomerBindingSource4 As BindingSource
    Friend WithEvents CustomerBindingSource3 As BindingSource
    Friend WithEvents CustomerBindingSource5 As BindingSource

    Friend WithEvents CustomerBindingSource6 As BindingSource

    Friend WithEvents DatabaseDataSet2BindingSource As BindingSource
    Friend WithEvents DatabaseDataSet2 As DatabaseDataSet2
    Friend WithEvents DatabaseDataSet2BindingSource1 As BindingSource
    Friend WithEvents CustomerBindingSource7 As BindingSource
    Friend WithEvents CustomerTableAdapter2 As DatabaseDataSet2TableAdapters.customerTableAdapter
    Friend WithEvents DatabaseDataSet3 As DatabaseDataSet3
    Friend WithEvents CustomerBindingSource8 As BindingSource
    Friend WithEvents CustomerTableAdapter3 As DatabaseDataSet3TableAdapters.customerTableAdapter
    Friend WithEvents btnsave As Button
    Friend WithEvents MenuStrip1 As MenuStrip
    Friend WithEvents FileToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents MenuToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents btnfirst As Button
    Friend WithEvents btnlast As Button
End Class
