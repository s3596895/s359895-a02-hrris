﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Report5
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Report5))
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.p2 = New System.Windows.Forms.PictureBox()
        Me.p1 = New System.Windows.Forms.PictureBox()
        Me.cbbmonth = New System.Windows.Forms.ComboBox()
        Me.txtyear = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtreport = New System.Windows.Forms.Button()
        Me.GroupBox5.SuspendLayout()
        CType(Me.p2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.p1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox5
        '
        Me.GroupBox5.BackColor = System.Drawing.Color.White
        Me.GroupBox5.Controls.Add(Me.p2)
        Me.GroupBox5.Controls.Add(Me.p1)
        Me.GroupBox5.Controls.Add(Me.cbbmonth)
        Me.GroupBox5.Controls.Add(Me.txtyear)
        Me.GroupBox5.Controls.Add(Me.Label7)
        Me.GroupBox5.Controls.Add(Me.Label5)
        Me.GroupBox5.Controls.Add(Me.txtreport)
        Me.GroupBox5.Location = New System.Drawing.Point(24, 41)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(322, 280)
        Me.GroupBox5.TabIndex = 5
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Customer due for check-in"
        '
        'p2
        '
        Me.p2.Image = CType(resources.GetObject("p2.Image"), System.Drawing.Image)
        Me.p2.Location = New System.Drawing.Point(296, 131)
        Me.p2.Name = "p2"
        Me.p2.Size = New System.Drawing.Size(20, 20)
        Me.p2.TabIndex = 18
        Me.p2.TabStop = False
        Me.p2.Visible = False
        '
        'p1
        '
        Me.p1.Image = CType(resources.GetObject("p1.Image"), System.Drawing.Image)
        Me.p1.Location = New System.Drawing.Point(296, 80)
        Me.p1.Name = "p1"
        Me.p1.Size = New System.Drawing.Size(20, 20)
        Me.p1.TabIndex = 17
        Me.p1.TabStop = False
        Me.p1.Visible = False
        '
        'cbbmonth
        '
        Me.cbbmonth.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbbmonth.FormattingEnabled = True
        Me.cbbmonth.Items.AddRange(New Object() {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"})
        Me.cbbmonth.Location = New System.Drawing.Point(143, 130)
        Me.cbbmonth.Name = "cbbmonth"
        Me.cbbmonth.Size = New System.Drawing.Size(121, 21)
        Me.cbbmonth.TabIndex = 15
        '
        'txtyear
        '
        Me.txtyear.Location = New System.Drawing.Point(143, 80)
        Me.txtyear.Name = "txtyear"
        Me.txtyear.Size = New System.Drawing.Size(121, 20)
        Me.txtyear.TabIndex = 16
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(30, 130)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(37, 13)
        Me.Label7.TabIndex = 13
        Me.Label7.Text = "Month"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(30, 83)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(29, 13)
        Me.Label5.TabIndex = 14
        Me.Label5.Text = "Year"
        '
        'txtreport
        '
        Me.txtreport.Location = New System.Drawing.Point(109, 228)
        Me.txtreport.Name = "txtreport"
        Me.txtreport.Size = New System.Drawing.Size(104, 23)
        Me.txtreport.TabIndex = 12
        Me.txtreport.Text = "Generate"
        Me.txtreport.UseVisualStyleBackColor = True
        '
        'Report5
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(384, 361)
        Me.Controls.Add(Me.GroupBox5)
        Me.Name = "Report5"
        Me.Text = "Report5"
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        CType(Me.p2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.p1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents GroupBox5 As GroupBox
    Friend WithEvents cbbmonth As ComboBox
    Friend WithEvents txtyear As TextBox
    Friend WithEvents Label7 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents txtreport As Button
    Friend WithEvents p1 As PictureBox
    Friend WithEvents p2 As PictureBox
End Class
