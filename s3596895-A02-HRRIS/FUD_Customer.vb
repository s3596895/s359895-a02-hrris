﻿Option Explicit On
Option Strict On
'Name: IFUD_Customer
'Description: Form for Customer
'Author: Dao Ngoc Minh 
'studen id: s3596895
'This code is written by Dao Ngoc Minh s3596895
Imports System.Drawing
Imports System.IO
Imports System
Imports System.Data
Imports System.Data.OleDb
Imports System.Data.Odbc
Imports System.Data.DataTable
'Author: Dao Ngoc Minh 
'studen id: s3596895
'This code is written by Dao Ngoc Minh s3596895
Public Class FUD_Customer

    Dim CustomerID As Integer
    Dim oConnection As New OleDbConnection(DataController.CONNECTION_STRING)

    'when load form, show data in datagridview and fill in form based on selected datagrid view
    Private Sub FindBooking_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim oController As DataController = New DataController
        loadDatabase()
        fillintheform(0)
    End Sub

    'load start up data on datagrid
    Private Sub loadDatabase()
        Dim oController As DataController = New DataController
        Dim query As String = "select * from customer"
        Dim adpt As New OleDbDataAdapter(query, oConnection)
        Dim ds As New DataSet()
        adpt.Fill(ds, "load")
        dgvCustomer.DataSource = ds.Tables(0)
    End Sub

    'table columm order:
    'customer id 0
    'title 1
    'first name 2
    'last name 3
    'phone 4
    'address 5 
    'email 6
    'dob 7 
    'gender 8
    Private Sub fillintheform(ByVal i As Integer)
        CustomerID = Convert.ToInt16(dgvCustomer.Item(0, i).Value)
        txtcustomerid.Text = dgvCustomer.Item(0, i).Value.ToString
        txtFirstName.Text = dgvCustomer.Item(2, i).Value.ToString
        txtLastName.Text = dgvCustomer.Item(3, i).Value.ToString
        If dgvCustomer.Item(8, i).Value.ToString = "Male" Then
            radiobtnMale.Checked = True
        Else
            radiobtnFemale.Checked = True
        End If
        txtPhone.Text = dgvCustomer.Item(4, i).Value.ToString
        txtAddress.Text = dgvCustomer.Item(5, i).Value.ToString
        txtEmail.Text = dgvCustomer.Item(6, i).Value.ToString
        DobValue.Text = dgvCustomer.Item(7, i).Value.ToString
        txttitle.Text = dgvCustomer.Item(1, i).Value.ToString
    End Sub

    'when click on a row, fill form
    Private Sub dgvSearchResult_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvCustomer.CellClick
        Dim i As Integer
        i = e.RowIndex
        If i >= 0 Then
            Dim selectedrow As DataGridViewRow
            selectedrow = dgvCustomer.Rows(i)
            fillintheform(i)
        End If
    End Sub

    'when selected an item in combobox, the search text box allow user input
    Private Sub cbbFind_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbbFindBy.SelectedIndexChanged
        Dim selectedIndex As Integer = cbbFindBy.SelectedIndex
        txtfindcustomervalue.Enabled = True
    End Sub

    'validate search text box input
    Private Function validatesearchinput() As Boolean
        Dim oValidation As New Validation
        Dim tt As New ToolTip()
        Dim bIsValid As Boolean = True

        If cbbFindBy.SelectedIndex = 0 Then
            bIsValid = oValidation.istext(txtfindcustomervalue.Text)
            If bIsValid Then
                picErrorField.Visible = False
            Else
                picErrorField.Visible = True
                tt.SetToolTip(picErrorField, "Please choose a search option and enter a right format")
            End If
        End If

        If cbbFindBy.SelectedIndex = 1 Then
            bIsValid = oValidation.IsNumericVal(txtfindcustomervalue.Text)
            If bIsValid Then
                picErrorField.Visible = False
            Else
                picErrorField.Visible = True
                tt.SetToolTip(picErrorField, "Please choose a search option and enter a right format")
            End If
        End If

        If cbbFindBy.SelectedIndex = 2 Then
            bIsValid = oValidation.istext(txtfindcustomervalue.Text)
            If bIsValid Then
                picErrorField.Visible = False
            Else
                picErrorField.Visible = True
                tt.SetToolTip(picErrorField, "Please choose a search option and enter a right format")
            End If
        End If

        Return bIsValid
    End Function

    'search first name in database
    Private Sub findfirstname(ByVal search As String)
        Dim oController As DataController = New DataController
        Dim query As String = "Select * FROM customer where firstname like '%" & search & "%';"
        Dim adpt As New OleDbDataAdapter(query, oConnection)
        Dim ds As New DataSet()
        adpt.Fill(ds, "load")
        dgvCustomer.DataSource = ds.Tables(0)
    End Sub

    'search last name in database
    Private Sub findlastname(ByVal search As String)
        Dim oController As DataController = New DataController
        Dim query As String = "Select * FROM customer where lastname like '%" & search & "%';"
        Dim adpt As New OleDbDataAdapter(query, oConnection)
        Dim ds As New DataSet()
        adpt.Fill(ds, "load")
        dgvCustomer.DataSource = ds.Tables(0)
    End Sub

    'search customer id in database
    Private Sub findcustomerid(ByVal search As String)
        Dim oController As DataController = New DataController
        Dim query As String = "Select * FROM customer where customer_id = " & search & " ;"
        Dim adpt As New OleDbDataAdapter(query, oConnection)
        Dim ds As New DataSet()
        adpt.Fill(ds, "load")
        dgvCustomer.DataSource = ds.Tables(0)
    End Sub

    'find customer
    Private Sub btnfindcustomer_Click(sender As Object, e As EventArgs) Handles btnsearchcustomer.Click
        Dim oController As DataController = New DataController
        Dim search As String
        Dim bisvalid = validatesearchinput()

        'find record based on first name
        If cbbFindBy.SelectedIndex = 0 And bisvalid Then
            search = txtfindcustomervalue.Text
            findfirstname(search)
            If dgvCustomer.Rows.Count > 0 Then
                fillintheform(0)
            End If
        End If

        'find record based on customer id
        If cbbFindBy.SelectedIndex = 1 And bisvalid Then
            search = txtfindcustomervalue.Text
            findcustomerid(search)
            If dgvCustomer.Rows.Count > 0 Then
                fillintheform(0)
            End If
        End If

        'find record based on last name
        If cbbFindBy.SelectedIndex = 2 And bisvalid Then
            search = txtfindcustomervalue.Text
            findlastname(search)
            If dgvCustomer.Rows.Count > 0 Then
                fillintheform(0)
            End If
        End If

    End Sub

    'allow user to edit
    Private Sub btnUpdate_Click(sender As Object, e As EventArgs) Handles btnUpdate.Click
        txtFirstName.Enabled = True
        txtLastName.Enabled = True
        txtPhone.Enabled = True
        txtAddress.Enabled = True
        txtEmail.Enabled = True
        DobValue.Enabled = True
        radiobtnFemale.Enabled = True
        radiobtnMale.Enabled = True
    End Sub

    'update function
    Private Sub btnsave_Click(sender As Object, e As EventArgs) Handles btnsave.Click
        Dim oValidation As New Validation
        Dim bIsValid As Boolean
        Dim bAllFieldsValid As Boolean
        bAllFieldsValid = True
        Dim tt As New ToolTip()

        If txtFirstName.Enabled = False Then Exit Sub

        'Validate gender
        If radiobtnMale.Checked = False And radiobtnFemale.Checked = False Then
            picErrorField6.Visible = True
            tt.SetToolTip(picErrorField1, "Please choose your gender")
            bAllFieldsValid = False
        Else picErrorField6.Visible = False
        End If

        'Validate first name - only text
        bIsValid = oValidation.istext(txtFirstName.Text)
        If bIsValid Then
            picErrorField1.Visible = False
        Else
            picErrorField1.Visible = True
            tt.SetToolTip(picErrorField1, "First name is empty or contain numbers")
            bAllFieldsValid = False
        End If

        'Validate last name - only text
        bIsValid = oValidation.istext(txtLastName.Text)
        If bIsValid Then
            picErrorField2.Visible = False
        Else
            picErrorField2.Visible = True
            tt.SetToolTip(picErrorField2, "Last name is empty or contain numbers")
            bAllFieldsValid = False
        End If

        'Validate phone number - only number        
        bIsValid = IsNumeric(txtPhone.Text)
        If bIsValid Then
            picErrorField3.Visible = False
        Else
            picErrorField3.Visible = True
            tt.SetToolTip(picErrorField3, "Characters and blank space are not allow")
            bAllFieldsValid = False
        End If

        'Validate address - no empty
        bIsValid = oValidation.IsAlphaNumeric(txtAddress.Text)
        If bIsValid Then
            picErrorField4.Visible = False
        Else
            picErrorField4.Visible = True
            tt.SetToolTip(picErrorField4, "Please enter your address")
            bAllFieldsValid = False
        End If

        'Validate email - email format only
        bIsValid = oValidation.isvalidateEmail(txtEmail.Text)
        If bIsValid Then
            picErrorField5.Visible = False
        Else
            picErrorField5.Visible = True
            tt.SetToolTip(picErrorField5, "Please enter correct email format")
            bAllFieldsValid = False
        End If

        'check all field
        If bAllFieldsValid Then
            Debug.Print("All fields are valid")
        End If

        ' Instantiate a hashtable and populate it with form data

        If bAllFieldsValid = True Then
            Dim htData As Hashtable = New Hashtable

            If radiobtnMale.Checked Then
                htData("title") = "Mr"
            ElseIf radiobtnFemale.Checked Then
                htData("title") = "Ms"
            End If
            htData("firstname") = txtFirstName.Text
            htData("lastname") = txtLastName.Text
            htData("phone") = txtPhone.Text
            htData("address") = txtAddress.Text
            htData("email") = txtEmail.Text
            htData("dob") = DobValue.Value
            If radiobtnMale.Checked Then
                htData("gender") = "Male"
            Else
                htData("gender") = "Female"
            End If
            htData("customer_id") = CustomerID

            Dim oDataController As DataController = New DataController
            Dim iNumRows = oDataController.updatecustomer(htData)
            If iNumRows = 1 Then
                MsgBox("The booking information is updated.")
                loadDatabase()
            Else
                MsgBox("The booking information is NOT updated.")
            End If
        End If

    End Sub

    'delete a record
    Private Sub btntDelete_Click_1(sender As Object, e As EventArgs) Handles btntDelete.Click
        Dim oDataController As DataController = New DataController
        Dim iNumRows = oDataController.deletecustomer(CStr(CustomerID))

        If iNumRows = 1 Then
            MsgBox("The booking information is Deleted.")
            loadDatabase()
        Else
            MsgBox("The booking information is NOT Deleted. ")
        End If
        txtFirstName.Enabled = False
        txtLastName.Enabled = False
        txtPhone.Enabled = False
        txtAddress.Enabled = False
        txtEmail.Enabled = False
        DobValue.Enabled = False
        radiobtnFemale.Enabled = False
        radiobtnMale.Enabled = False
        fillintheform(0)
    End Sub

    'move to next record
    Private Sub btnNext_Click_1(sender As Object, e As EventArgs) Handles BtnNext.Click
        If dgvCustomer.CurrentRow.Index < dgvCustomer.RowCount - 1 Then
            dgvCustomer.CurrentCell = dgvCustomer.Item(0, dgvCustomer.CurrentRow.Index + 1)
            fillintheform(dgvCustomer.CurrentRow.Index)
        End If
    End Sub

    'move to previous record
    Private Sub btnPrev_Click_1(sender As Object, e As EventArgs) Handles BtnPrev.Click
        If dgvCustomer.CurrentRow.Index > 0 Then
            dgvCustomer.CurrentCell = dgvCustomer.Item(0, dgvCustomer.CurrentRow.Index - 1)
            fillintheform(dgvCustomer.CurrentRow.Index)
        End If
    End Sub

    'move to first record
    Private Sub btnFirst_Click(sender As Object, e As EventArgs) Handles btnfirst.Click
        dgvCustomer.CurrentCell = dgvCustomer.Item(0, 0)
        fillintheform(dgvCustomer.CurrentRow.Index)
    End Sub

    'move to last record
    Private Sub btnLast_Click(sender As Object, e As EventArgs) Handles btnlast.Click
        dgvCustomer.CurrentCell = dgvCustomer.Item(0, dgvCustomer.RowCount - 1)
        fillintheform(dgvCustomer.CurrentRow.Index)
    End Sub

    'go back booking form
    Private Sub MenuToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles MenuToolStripMenuItem.Click
        Booking.Show()
        Me.Close()
    End Sub

End Class
