﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Report4
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Report4))
        Me.txtbookingyear = New System.Windows.Forms.GroupBox()
        Me.p2 = New System.Windows.Forms.PictureBox()
        Me.p1 = New System.Windows.Forms.PictureBox()
        Me.cbbmonth = New System.Windows.Forms.ComboBox()
        Me.txtyear = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtbookingyear.SuspendLayout()
        CType(Me.p2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.p1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txtbookingyear
        '
        Me.txtbookingyear.BackColor = System.Drawing.Color.White
        Me.txtbookingyear.Controls.Add(Me.p2)
        Me.txtbookingyear.Controls.Add(Me.p1)
        Me.txtbookingyear.Controls.Add(Me.cbbmonth)
        Me.txtbookingyear.Controls.Add(Me.txtyear)
        Me.txtbookingyear.Controls.Add(Me.Label6)
        Me.txtbookingyear.Controls.Add(Me.Button4)
        Me.txtbookingyear.Controls.Add(Me.Label4)
        Me.txtbookingyear.Location = New System.Drawing.Point(30, 44)
        Me.txtbookingyear.Name = "txtbookingyear"
        Me.txtbookingyear.Size = New System.Drawing.Size(310, 256)
        Me.txtbookingyear.TabIndex = 3
        Me.txtbookingyear.TabStop = False
        Me.txtbookingyear.Text = "All booking made in the given month and year"
        '
        'p2
        '
        Me.p2.Image = CType(resources.GetObject("p2.Image"), System.Drawing.Image)
        Me.p2.Location = New System.Drawing.Point(274, 86)
        Me.p2.Name = "p2"
        Me.p2.Size = New System.Drawing.Size(20, 20)
        Me.p2.TabIndex = 17
        Me.p2.TabStop = False
        Me.p2.Visible = False
        '
        'p1
        '
        Me.p1.Image = CType(resources.GetObject("p1.Image"), System.Drawing.Image)
        Me.p1.Location = New System.Drawing.Point(274, 38)
        Me.p1.Name = "p1"
        Me.p1.Size = New System.Drawing.Size(20, 20)
        Me.p1.TabIndex = 16
        Me.p1.TabStop = False
        Me.p1.Visible = False
        '
        'cbbmonth
        '
        Me.cbbmonth.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbbmonth.FormattingEnabled = True
        Me.cbbmonth.Items.AddRange(New Object() {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"})
        Me.cbbmonth.Location = New System.Drawing.Point(133, 37)
        Me.cbbmonth.Name = "cbbmonth"
        Me.cbbmonth.Size = New System.Drawing.Size(121, 21)
        Me.cbbmonth.TabIndex = 10
        '
        'txtyear
        '
        Me.txtyear.Location = New System.Drawing.Point(133, 86)
        Me.txtyear.MaxLength = 4
        Me.txtyear.Name = "txtyear"
        Me.txtyear.Size = New System.Drawing.Size(121, 20)
        Me.txtyear.TabIndex = 11
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(15, 88)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(29, 13)
        Me.Label6.TabIndex = 10
        Me.Label6.Text = "Year"
        '
        'Button4
        '
        Me.Button4.Location = New System.Drawing.Point(94, 170)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(104, 23)
        Me.Button4.TabIndex = 3
        Me.Button4.Text = "Generate"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(15, 36)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(37, 13)
        Me.Label4.TabIndex = 10
        Me.Label4.Text = "Month"
        '
        'Report4
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(384, 361)
        Me.Controls.Add(Me.txtbookingyear)
        Me.Name = "Report4"
        Me.Text = "Report4"
        Me.txtbookingyear.ResumeLayout(False)
        Me.txtbookingyear.PerformLayout()
        CType(Me.p2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.p1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents txtbookingyear As GroupBox
    Friend WithEvents cbbmonth As ComboBox
    Friend WithEvents txtyear As TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents Button4 As Button
    Friend WithEvents Label4 As Label
    Friend WithEvents p1 As PictureBox
    Friend WithEvents p2 As PictureBox
End Class
