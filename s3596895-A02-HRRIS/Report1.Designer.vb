﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Report1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Report1))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.p1 = New System.Windows.Forms.PictureBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.cbbCustomerId = New System.Windows.Forms.ComboBox()
        Me.lblCustomerId = New System.Windows.Forms.Label()
        Me.GroupBox1.SuspendLayout()
        CType(Me.p1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.GroupBox1.Controls.Add(Me.p1)
        Me.GroupBox1.Controls.Add(Me.Button1)
        Me.GroupBox1.Controls.Add(Me.cbbCustomerId)
        Me.GroupBox1.Controls.Add(Me.lblCustomerId)
        Me.GroupBox1.ForeColor = System.Drawing.Color.Black
        Me.GroupBox1.Location = New System.Drawing.Point(12, 43)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(310, 192)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Last booking for customer"
        '
        'p1
        '
        Me.p1.Image = CType(resources.GetObject("p1.Image"), System.Drawing.Image)
        Me.p1.Location = New System.Drawing.Point(271, 35)
        Me.p1.Name = "p1"
        Me.p1.Size = New System.Drawing.Size(20, 20)
        Me.p1.TabIndex = 76
        Me.p1.TabStop = False
        Me.p1.Visible = False
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(94, 154)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(104, 23)
        Me.Button1.TabIndex = 2
        Me.Button1.Text = "Generate"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'cbbCustomerId
        '
        Me.cbbCustomerId.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbbCustomerId.FormattingEnabled = True
        Me.cbbCustomerId.Location = New System.Drawing.Point(133, 34)
        Me.cbbCustomerId.Name = "cbbCustomerId"
        Me.cbbCustomerId.Size = New System.Drawing.Size(121, 21)
        Me.cbbCustomerId.TabIndex = 1
        '
        'lblCustomerId
        '
        Me.lblCustomerId.AutoSize = True
        Me.lblCustomerId.Location = New System.Drawing.Point(15, 33)
        Me.lblCustomerId.Name = "lblCustomerId"
        Me.lblCustomerId.Size = New System.Drawing.Size(65, 13)
        Me.lblCustomerId.TabIndex = 0
        Me.lblCustomerId.Text = "Customer ID"
        '
        'Report1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(334, 311)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "Report1"
        Me.Text = "Report1"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.p1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents Button1 As Button
    Friend WithEvents cbbCustomerId As ComboBox
    Friend WithEvents lblCustomerId As Label
    Friend WithEvents p1 As PictureBox
End Class
