﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FUD_Room
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FUD_Room))
        Me.picErrorField1 = New System.Windows.Forms.PictureBox()
        Me.picErrorField7 = New System.Windows.Forms.PictureBox()
        Me.picErrorField6 = New System.Windows.Forms.PictureBox()
        Me.picErrorField5 = New System.Windows.Forms.PictureBox()
        Me.picErrorField4 = New System.Windows.Forms.PictureBox()
        Me.picErrorField3 = New System.Windows.Forms.PictureBox()
        Me.picErrorField2 = New System.Windows.Forms.PictureBox()
        Me.gbxRoom = New System.Windows.Forms.GroupBox()
        Me.txtroomid = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtFloor = New System.Windows.Forms.TextBox()
        Me.txtNumberOfBeds = New System.Windows.Forms.TextBox()
        Me.txtPrice = New System.Windows.Forms.TextBox()
        Me.txtRoomNumber = New System.Windows.Forms.TextBox()
        Me.radiobtnUnavailable = New System.Windows.Forms.RadioButton()
        Me.radiobtnAvailable = New System.Windows.Forms.RadioButton()
        Me.txtDescription = New System.Windows.Forms.TextBox()
        Me.txtType = New System.Windows.Forms.TextBox()
        Me.lblroomType = New System.Windows.Forms.Label()
        Me.lblroomPrice = New System.Windows.Forms.Label()
        Me.lblroomNumberOfBeds = New System.Windows.Forms.Label()
        Me.lblroomAvailability = New System.Windows.Forms.Label()
        Me.lblroomFloor = New System.Windows.Forms.Label()
        Me.lblroomDescription = New System.Windows.Forms.Label()
        Me.lblroomRoomNumber = New System.Windows.Forms.Label()
        Me.dgvRoom = New System.Windows.Forms.DataGridView()
        Me.btnsave = New System.Windows.Forms.Button()
        Me.BtnNext = New System.Windows.Forms.Button()
        Me.BtnPrev = New System.Windows.Forms.Button()
        Me.btntDelete = New System.Windows.Forms.Button()
        Me.btnUpdate = New System.Windows.Forms.Button()
        Me.gbxFind = New System.Windows.Forms.GroupBox()
        Me.btnsearchroom = New System.Windows.Forms.Button()
        Me.picErrorField = New System.Windows.Forms.PictureBox()
        Me.txtfindRoomvalue = New System.Windows.Forms.TextBox()
        Me.cbbFindBy = New System.Windows.Forms.ComboBox()
        Me.lblFindBy = New System.Windows.Forms.Label()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.FileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.btnfirst = New System.Windows.Forms.Button()
        Me.btnlast = New System.Windows.Forms.Button()
        CType(Me.picErrorField1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picErrorField7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picErrorField6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picErrorField5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picErrorField4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picErrorField3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picErrorField2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxRoom.SuspendLayout()
        CType(Me.dgvRoom, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxFind.SuspendLayout()
        CType(Me.picErrorField, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'picErrorField1
        '
        Me.picErrorField1.Image = CType(resources.GetObject("picErrorField1.Image"), System.Drawing.Image)
        Me.picErrorField1.Location = New System.Drawing.Point(356, 58)
        Me.picErrorField1.Name = "picErrorField1"
        Me.picErrorField1.Size = New System.Drawing.Size(20, 20)
        Me.picErrorField1.TabIndex = 183
        Me.picErrorField1.TabStop = False
        Me.picErrorField1.Visible = False
        '
        'picErrorField7
        '
        Me.picErrorField7.Image = CType(resources.GetObject("picErrorField7.Image"), System.Drawing.Image)
        Me.picErrorField7.Location = New System.Drawing.Point(356, 237)
        Me.picErrorField7.Name = "picErrorField7"
        Me.picErrorField7.Size = New System.Drawing.Size(20, 20)
        Me.picErrorField7.TabIndex = 189
        Me.picErrorField7.TabStop = False
        Me.picErrorField7.Visible = False
        '
        'picErrorField6
        '
        Me.picErrorField6.Image = CType(resources.GetObject("picErrorField6.Image"), System.Drawing.Image)
        Me.picErrorField6.Location = New System.Drawing.Point(356, 206)
        Me.picErrorField6.Name = "picErrorField6"
        Me.picErrorField6.Size = New System.Drawing.Size(20, 20)
        Me.picErrorField6.TabIndex = 188
        Me.picErrorField6.TabStop = False
        Me.picErrorField6.Visible = False
        '
        'picErrorField5
        '
        Me.picErrorField5.Image = CType(resources.GetObject("picErrorField5.Image"), System.Drawing.Image)
        Me.picErrorField5.Location = New System.Drawing.Point(356, 176)
        Me.picErrorField5.Name = "picErrorField5"
        Me.picErrorField5.Size = New System.Drawing.Size(20, 20)
        Me.picErrorField5.TabIndex = 187
        Me.picErrorField5.TabStop = False
        Me.picErrorField5.Visible = False
        '
        'picErrorField4
        '
        Me.picErrorField4.Image = CType(resources.GetObject("picErrorField4.Image"), System.Drawing.Image)
        Me.picErrorField4.Location = New System.Drawing.Point(356, 146)
        Me.picErrorField4.Name = "picErrorField4"
        Me.picErrorField4.Size = New System.Drawing.Size(20, 20)
        Me.picErrorField4.TabIndex = 186
        Me.picErrorField4.TabStop = False
        Me.picErrorField4.Visible = False
        '
        'picErrorField3
        '
        Me.picErrorField3.Image = CType(resources.GetObject("picErrorField3.Image"), System.Drawing.Image)
        Me.picErrorField3.Location = New System.Drawing.Point(356, 118)
        Me.picErrorField3.Name = "picErrorField3"
        Me.picErrorField3.Size = New System.Drawing.Size(20, 20)
        Me.picErrorField3.TabIndex = 185
        Me.picErrorField3.TabStop = False
        Me.picErrorField3.Visible = False
        '
        'picErrorField2
        '
        Me.picErrorField2.Image = CType(resources.GetObject("picErrorField2.Image"), System.Drawing.Image)
        Me.picErrorField2.Location = New System.Drawing.Point(356, 89)
        Me.picErrorField2.Name = "picErrorField2"
        Me.picErrorField2.Size = New System.Drawing.Size(20, 20)
        Me.picErrorField2.TabIndex = 184
        Me.picErrorField2.TabStop = False
        Me.picErrorField2.Visible = False
        '
        'gbxRoom
        '
        Me.gbxRoom.BackColor = System.Drawing.SystemColors.ControlLight
        Me.gbxRoom.Controls.Add(Me.txtroomid)
        Me.gbxRoom.Controls.Add(Me.Label1)
        Me.gbxRoom.Controls.Add(Me.txtFloor)
        Me.gbxRoom.Controls.Add(Me.txtNumberOfBeds)
        Me.gbxRoom.Controls.Add(Me.txtPrice)
        Me.gbxRoom.Controls.Add(Me.txtRoomNumber)
        Me.gbxRoom.Controls.Add(Me.radiobtnUnavailable)
        Me.gbxRoom.Controls.Add(Me.radiobtnAvailable)
        Me.gbxRoom.Controls.Add(Me.txtDescription)
        Me.gbxRoom.Controls.Add(Me.txtType)
        Me.gbxRoom.Controls.Add(Me.lblroomType)
        Me.gbxRoom.Controls.Add(Me.lblroomPrice)
        Me.gbxRoom.Controls.Add(Me.lblroomNumberOfBeds)
        Me.gbxRoom.Controls.Add(Me.lblroomAvailability)
        Me.gbxRoom.Controls.Add(Me.lblroomFloor)
        Me.gbxRoom.Controls.Add(Me.lblroomDescription)
        Me.gbxRoom.Controls.Add(Me.lblroomRoomNumber)
        Me.gbxRoom.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbxRoom.Location = New System.Drawing.Point(12, 38)
        Me.gbxRoom.Name = "gbxRoom"
        Me.gbxRoom.Size = New System.Drawing.Size(338, 254)
        Me.gbxRoom.TabIndex = 182
        Me.gbxRoom.TabStop = False
        Me.gbxRoom.Text = "Room information"
        '
        'txtroomid
        '
        Me.txtroomid.Enabled = False
        Me.txtroomid.Location = New System.Drawing.Point(103, 228)
        Me.txtroomid.Name = "txtroomid"
        Me.txtroomid.Size = New System.Drawing.Size(229, 20)
        Me.txtroomid.TabIndex = 14
        '
        'Label1
        '
        Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label1.Location = New System.Drawing.Point(7, 231)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(89, 20)
        Me.Label1.TabIndex = 13
        Me.Label1.Text = "Room ID"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtFloor
        '
        Me.txtFloor.Enabled = False
        Me.txtFloor.Location = New System.Drawing.Point(103, 168)
        Me.txtFloor.MaxLength = 2
        Me.txtFloor.Name = "txtFloor"
        Me.txtFloor.Size = New System.Drawing.Size(228, 20)
        Me.txtFloor.TabIndex = 5
        '
        'txtNumberOfBeds
        '
        Me.txtNumberOfBeds.Enabled = False
        Me.txtNumberOfBeds.Location = New System.Drawing.Point(103, 108)
        Me.txtNumberOfBeds.MaxLength = 2
        Me.txtNumberOfBeds.Name = "txtNumberOfBeds"
        Me.txtNumberOfBeds.Size = New System.Drawing.Size(228, 20)
        Me.txtNumberOfBeds.TabIndex = 4
        '
        'txtPrice
        '
        Me.txtPrice.Enabled = False
        Me.txtPrice.Location = New System.Drawing.Point(103, 78)
        Me.txtPrice.MaxLength = 7
        Me.txtPrice.Name = "txtPrice"
        Me.txtPrice.Size = New System.Drawing.Size(228, 20)
        Me.txtPrice.TabIndex = 3
        '
        'txtRoomNumber
        '
        Me.txtRoomNumber.Enabled = False
        Me.txtRoomNumber.Location = New System.Drawing.Point(103, 20)
        Me.txtRoomNumber.MaxLength = 3
        Me.txtRoomNumber.Name = "txtRoomNumber"
        Me.txtRoomNumber.Size = New System.Drawing.Size(228, 20)
        Me.txtRoomNumber.TabIndex = 1
        '
        'radiobtnUnavailable
        '
        Me.radiobtnUnavailable.AutoSize = True
        Me.radiobtnUnavailable.Enabled = False
        Me.radiobtnUnavailable.Location = New System.Drawing.Point(241, 138)
        Me.radiobtnUnavailable.Name = "radiobtnUnavailable"
        Me.radiobtnUnavailable.Size = New System.Drawing.Size(81, 17)
        Me.radiobtnUnavailable.TabIndex = 10
        Me.radiobtnUnavailable.TabStop = True
        Me.radiobtnUnavailable.Text = "Unavailable"
        Me.radiobtnUnavailable.UseVisualStyleBackColor = True
        '
        'radiobtnAvailable
        '
        Me.radiobtnAvailable.AutoSize = True
        Me.radiobtnAvailable.Enabled = False
        Me.radiobtnAvailable.Location = New System.Drawing.Point(103, 140)
        Me.radiobtnAvailable.Name = "radiobtnAvailable"
        Me.radiobtnAvailable.Size = New System.Drawing.Size(68, 17)
        Me.radiobtnAvailable.TabIndex = 9
        Me.radiobtnAvailable.TabStop = True
        Me.radiobtnAvailable.Text = "Available"
        Me.radiobtnAvailable.UseVisualStyleBackColor = True
        '
        'txtDescription
        '
        Me.txtDescription.Enabled = False
        Me.txtDescription.Location = New System.Drawing.Point(103, 199)
        Me.txtDescription.Name = "txtDescription"
        Me.txtDescription.Size = New System.Drawing.Size(229, 20)
        Me.txtDescription.TabIndex = 6
        '
        'txtType
        '
        Me.txtType.Enabled = False
        Me.txtType.Location = New System.Drawing.Point(103, 51)
        Me.txtType.Name = "txtType"
        Me.txtType.Size = New System.Drawing.Size(229, 20)
        Me.txtType.TabIndex = 2
        '
        'lblroomType
        '
        Me.lblroomType.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblroomType.Location = New System.Drawing.Point(7, 50)
        Me.lblroomType.Name = "lblroomType"
        Me.lblroomType.Size = New System.Drawing.Size(89, 20)
        Me.lblroomType.TabIndex = 2
        Me.lblroomType.Text = "Type"
        Me.lblroomType.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblroomPrice
        '
        Me.lblroomPrice.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblroomPrice.Location = New System.Drawing.Point(7, 79)
        Me.lblroomPrice.Name = "lblroomPrice"
        Me.lblroomPrice.Size = New System.Drawing.Size(89, 20)
        Me.lblroomPrice.TabIndex = 4
        Me.lblroomPrice.Text = "Price"
        Me.lblroomPrice.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblroomNumberOfBeds
        '
        Me.lblroomNumberOfBeds.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblroomNumberOfBeds.Location = New System.Drawing.Point(7, 108)
        Me.lblroomNumberOfBeds.Name = "lblroomNumberOfBeds"
        Me.lblroomNumberOfBeds.Size = New System.Drawing.Size(89, 20)
        Me.lblroomNumberOfBeds.TabIndex = 6
        Me.lblroomNumberOfBeds.Text = "Number of Beds"
        Me.lblroomNumberOfBeds.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblroomAvailability
        '
        Me.lblroomAvailability.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblroomAvailability.Location = New System.Drawing.Point(7, 138)
        Me.lblroomAvailability.Name = "lblroomAvailability"
        Me.lblroomAvailability.Size = New System.Drawing.Size(89, 20)
        Me.lblroomAvailability.TabIndex = 8
        Me.lblroomAvailability.Text = "Availability"
        Me.lblroomAvailability.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblroomFloor
        '
        Me.lblroomFloor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblroomFloor.Location = New System.Drawing.Point(7, 168)
        Me.lblroomFloor.Name = "lblroomFloor"
        Me.lblroomFloor.Size = New System.Drawing.Size(89, 20)
        Me.lblroomFloor.TabIndex = 10
        Me.lblroomFloor.Text = "Floor"
        Me.lblroomFloor.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblroomDescription
        '
        Me.lblroomDescription.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblroomDescription.Location = New System.Drawing.Point(7, 199)
        Me.lblroomDescription.Name = "lblroomDescription"
        Me.lblroomDescription.Size = New System.Drawing.Size(89, 20)
        Me.lblroomDescription.TabIndex = 12
        Me.lblroomDescription.Text = "Description"
        Me.lblroomDescription.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblroomRoomNumber
        '
        Me.lblroomRoomNumber.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblroomRoomNumber.Location = New System.Drawing.Point(7, 20)
        Me.lblroomRoomNumber.Name = "lblroomRoomNumber"
        Me.lblroomRoomNumber.Size = New System.Drawing.Size(89, 20)
        Me.lblroomRoomNumber.TabIndex = 0
        Me.lblroomRoomNumber.Text = "Room Number"
        Me.lblroomRoomNumber.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'dgvRoom
        '
        Me.dgvRoom.AllowUserToAddRows = False
        Me.dgvRoom.AllowUserToDeleteRows = False
        Me.dgvRoom.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvRoom.Location = New System.Drawing.Point(2, 354)
        Me.dgvRoom.Name = "dgvRoom"
        Me.dgvRoom.ReadOnly = True
        Me.dgvRoom.Size = New System.Drawing.Size(937, 254)
        Me.dgvRoom.TabIndex = 190
        '
        'btnsave
        '
        Me.btnsave.Location = New System.Drawing.Point(620, 170)
        Me.btnsave.Name = "btnsave"
        Me.btnsave.Size = New System.Drawing.Size(96, 23)
        Me.btnsave.TabIndex = 198
        Me.btnsave.Text = "Save Update"
        Me.btnsave.UseVisualStyleBackColor = True
        '
        'BtnNext
        '
        Me.BtnNext.Location = New System.Drawing.Point(683, 325)
        Me.BtnNext.Name = "BtnNext"
        Me.BtnNext.Size = New System.Drawing.Size(96, 23)
        Me.BtnNext.TabIndex = 196
        Me.BtnNext.Text = "Next Record"
        Me.BtnNext.UseVisualStyleBackColor = True
        '
        'BtnPrev
        '
        Me.BtnPrev.Location = New System.Drawing.Point(559, 324)
        Me.BtnPrev.Name = "BtnPrev"
        Me.BtnPrev.Size = New System.Drawing.Size(96, 23)
        Me.BtnPrev.TabIndex = 195
        Me.BtnPrev.Text = "Previous Record"
        Me.BtnPrev.UseVisualStyleBackColor = True
        '
        'btntDelete
        '
        Me.btntDelete.Location = New System.Drawing.Point(430, 173)
        Me.btntDelete.Name = "btntDelete"
        Me.btntDelete.Size = New System.Drawing.Size(96, 23)
        Me.btntDelete.TabIndex = 193
        Me.btntDelete.Text = "Delete"
        Me.btntDelete.UseVisualStyleBackColor = True
        '
        'btnUpdate
        '
        Me.btnUpdate.Location = New System.Drawing.Point(804, 170)
        Me.btnUpdate.Name = "btnUpdate"
        Me.btnUpdate.Size = New System.Drawing.Size(96, 23)
        Me.btnUpdate.TabIndex = 192
        Me.btnUpdate.Text = "Update"
        Me.btnUpdate.UseVisualStyleBackColor = True
        '
        'gbxFind
        '
        Me.gbxFind.Controls.Add(Me.btnsearchroom)
        Me.gbxFind.Controls.Add(Me.picErrorField)
        Me.gbxFind.Controls.Add(Me.txtfindRoomvalue)
        Me.gbxFind.Controls.Add(Me.cbbFindBy)
        Me.gbxFind.Controls.Add(Me.lblFindBy)
        Me.gbxFind.Location = New System.Drawing.Point(430, 38)
        Me.gbxFind.Name = "gbxFind"
        Me.gbxFind.Size = New System.Drawing.Size(470, 85)
        Me.gbxFind.TabIndex = 191
        Me.gbxFind.TabStop = False
        Me.gbxFind.Text = "Find"
        '
        'btnsearchroom
        '
        Me.btnsearchroom.Location = New System.Drawing.Point(190, 50)
        Me.btnsearchroom.Name = "btnsearchroom"
        Me.btnsearchroom.Size = New System.Drawing.Size(96, 23)
        Me.btnsearchroom.TabIndex = 147
        Me.btnsearchroom.Text = "Search"
        Me.btnsearchroom.UseVisualStyleBackColor = True
        '
        'picErrorField
        '
        Me.picErrorField.Image = CType(resources.GetObject("picErrorField.Image"), System.Drawing.Image)
        Me.picErrorField.Location = New System.Drawing.Point(444, 16)
        Me.picErrorField.Name = "picErrorField"
        Me.picErrorField.Size = New System.Drawing.Size(20, 20)
        Me.picErrorField.TabIndex = 146
        Me.picErrorField.TabStop = False
        Me.picErrorField.Visible = False
        '
        'txtfindRoomvalue
        '
        Me.txtfindRoomvalue.Enabled = False
        Me.txtfindRoomvalue.Location = New System.Drawing.Point(220, 16)
        Me.txtfindRoomvalue.Name = "txtfindRoomvalue"
        Me.txtfindRoomvalue.Size = New System.Drawing.Size(218, 20)
        Me.txtfindRoomvalue.TabIndex = 144
        '
        'cbbFindBy
        '
        Me.cbbFindBy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbbFindBy.Items.AddRange(New Object() {"Room Type", "Room Number"})
        Me.cbbFindBy.Location = New System.Drawing.Point(89, 16)
        Me.cbbFindBy.Name = "cbbFindBy"
        Me.cbbFindBy.Size = New System.Drawing.Size(125, 21)
        Me.cbbFindBy.TabIndex = 143
        '
        'lblFindBy
        '
        Me.lblFindBy.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblFindBy.Location = New System.Drawing.Point(6, 17)
        Me.lblFindBy.Name = "lblFindBy"
        Me.lblFindBy.Size = New System.Drawing.Size(68, 20)
        Me.lblFindBy.TabIndex = 27
        Me.lblFindBy.Text = "Find By"
        Me.lblFindBy.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FileToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(942, 24)
        Me.MenuStrip1.TabIndex = 199
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'FileToolStripMenuItem
        '
        Me.FileToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MenuToolStripMenuItem})
        Me.FileToolStripMenuItem.Name = "FileToolStripMenuItem"
        Me.FileToolStripMenuItem.Size = New System.Drawing.Size(37, 20)
        Me.FileToolStripMenuItem.Text = "File"
        '
        'MenuToolStripMenuItem
        '
        Me.MenuToolStripMenuItem.Name = "MenuToolStripMenuItem"
        Me.MenuToolStripMenuItem.Size = New System.Drawing.Size(105, 22)
        Me.MenuToolStripMenuItem.Text = "Menu"
        '
        'btnfirst
        '
        Me.btnfirst.Location = New System.Drawing.Point(430, 325)
        Me.btnfirst.Name = "btnfirst"
        Me.btnfirst.Size = New System.Drawing.Size(96, 23)
        Me.btnfirst.TabIndex = 200
        Me.btnfirst.Text = "First record"
        Me.btnfirst.UseVisualStyleBackColor = True
        '
        'btnlast
        '
        Me.btnlast.Location = New System.Drawing.Point(804, 324)
        Me.btnlast.Name = "btnlast"
        Me.btnlast.Size = New System.Drawing.Size(96, 23)
        Me.btnlast.TabIndex = 201
        Me.btnlast.Text = "Last record"
        Me.btnlast.UseVisualStyleBackColor = True
        '
        'FUD_Room
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(942, 611)
        Me.Controls.Add(Me.btnlast)
        Me.Controls.Add(Me.btnfirst)
        Me.Controls.Add(Me.btnsave)
        Me.Controls.Add(Me.BtnNext)
        Me.Controls.Add(Me.BtnPrev)
        Me.Controls.Add(Me.btntDelete)
        Me.Controls.Add(Me.btnUpdate)
        Me.Controls.Add(Me.gbxFind)
        Me.Controls.Add(Me.dgvRoom)
        Me.Controls.Add(Me.picErrorField1)
        Me.Controls.Add(Me.picErrorField7)
        Me.Controls.Add(Me.picErrorField6)
        Me.Controls.Add(Me.picErrorField5)
        Me.Controls.Add(Me.picErrorField4)
        Me.Controls.Add(Me.picErrorField3)
        Me.Controls.Add(Me.picErrorField2)
        Me.Controls.Add(Me.gbxRoom)
        Me.Controls.Add(Me.MenuStrip1)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "FUD_Room"
        Me.Text = "FUD_Room"
        CType(Me.picErrorField1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picErrorField7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picErrorField6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picErrorField5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picErrorField4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picErrorField3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picErrorField2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxRoom.ResumeLayout(False)
        Me.gbxRoom.PerformLayout()
        CType(Me.dgvRoom, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxFind.ResumeLayout(False)
        Me.gbxFind.PerformLayout()
        CType(Me.picErrorField, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents picErrorField1 As PictureBox
    Friend WithEvents picErrorField7 As PictureBox
    Friend WithEvents picErrorField6 As PictureBox
    Friend WithEvents picErrorField5 As PictureBox
    Friend WithEvents picErrorField4 As PictureBox
    Friend WithEvents picErrorField3 As PictureBox
    Friend WithEvents picErrorField2 As PictureBox
    Friend WithEvents gbxRoom As GroupBox
    Friend WithEvents txtFloor As TextBox
    Friend WithEvents txtNumberOfBeds As TextBox
    Friend WithEvents txtPrice As TextBox
    Friend WithEvents txtRoomNumber As TextBox
    Friend WithEvents radiobtnUnavailable As RadioButton
    Friend WithEvents radiobtnAvailable As RadioButton
    Friend WithEvents txtDescription As TextBox
    Friend WithEvents txtType As TextBox
    Friend WithEvents lblroomType As Label
    Friend WithEvents lblroomPrice As Label
    Friend WithEvents lblroomNumberOfBeds As Label
    Friend WithEvents lblroomAvailability As Label
    Friend WithEvents lblroomFloor As Label
    Friend WithEvents lblroomDescription As Label
    Friend WithEvents lblroomRoomNumber As Label
    Friend WithEvents dgvRoom As DataGridView
    Friend WithEvents btnsave As Button
    Friend WithEvents BtnNext As Button
    Friend WithEvents BtnPrev As Button
    Friend WithEvents btntDelete As Button
    Friend WithEvents btnUpdate As Button
    Friend WithEvents gbxFind As GroupBox
    Friend WithEvents btnsearchroom As Button
    Friend WithEvents picErrorField As PictureBox
    Friend WithEvents txtfindRoomvalue As TextBox
    Friend WithEvents cbbFindBy As ComboBox
    Friend WithEvents lblFindBy As Label
    Friend WithEvents txtroomid As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents MenuStrip1 As MenuStrip
    Friend WithEvents FileToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents MenuToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents btnfirst As Button
    Friend WithEvents btnlast As Button
End Class
