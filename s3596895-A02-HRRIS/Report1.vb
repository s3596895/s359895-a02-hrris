﻿Option Explicit On
Option Strict On

Imports System.Drawing
Imports System.IO
'Author: Dao Ngoc Minh 
'studen id: s3596895
'This code is written by Dao Ngoc Minh s3596895
Public Class Report1
    Dim ocontroller As DataController = New DataController
    'load combobox
    Private Sub Report1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        cbbCustomerId.DropDownStyle = ComboBoxStyle.DropDownList
        Dim lsdata = ocontroller.findAllCustomer()

        For Each customerID In lsdata
            cbbCustomerId.Items.Add(CStr(customerID("customer_id")))
        Next
    End Sub

    'generate report
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim tt As New ToolTip()
        If Not (cbbCustomerId.SelectedItem Is Nothing) Then
            p1.Visible = False
        Else p1.Visible = True
            tt.SetToolTip(p1, "You must select a customer ID")
            Exit Sub
        End If
        ocontroller.createReport1(cbbCustomerId.SelectedItem.ToString)
    End Sub
End Class