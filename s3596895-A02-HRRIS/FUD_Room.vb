﻿Option Explicit On
Option Strict On

Imports System.IO
Imports System
Imports System.Data
Imports System.Data.OleDb

Imports System.Data.Odbc
Imports System.Data.DataTable
'Author: Dao Ngoc Minh 
'studen id: s3596895
'This code is written by Dao Ngoc Minh s3596895
Public Class FUD_Room
    Dim oConnection As New OleDbConnection(DataController.CONNECTION_STRING)
    Dim RoomID As Integer
    Dim index As Integer

    'loading room form with all data in the reocord
    Private Sub FindRoom_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim oController As DataController = New DataController
        Dim query As String = "select * from customer"
        loadDatabase()
        If dgvRoom.RowCount > 1 Then fillintheform(0)
    End Sub

    'find all room record
    Private Sub loadDatabase()
        Dim oController As DataController = New DataController
        Dim query As String = "select * from room"
        Dim adpt As New OleDbDataAdapter(query, oConnection)
        Dim ds As New DataSet()
        adpt.Fill(ds)
        dgvRoom.DataSource = ds.Tables(0)
    End Sub

    'table columm order:
    'room id 0
    'room_number 1
    'type 2
    'price 3
    'num_bed 4
    'availability 5 
    'floor 6
    'description 7 
    Private Sub fillintheform(ByVal i As Integer)
        RoomID = Convert.ToInt16(dgvRoom.Item(0, i).Value)
        txtroomid.Text = dgvRoom.Item(0, i).Value.ToString
        txtRoomNumber.Text = dgvRoom.Item(1, i).Value.ToString
        txtType.Text = dgvRoom.Item(2, i).Value.ToString
        txtPrice.Text = dgvRoom.Item(3, i).Value.ToString
        txtNumberOfBeds.Text = dgvRoom.Item(4, i).Value.ToString
        If dgvRoom.Item(5, i).Value.ToString = "True" Then
            radiobtnAvailable.Checked = True
        Else
            radiobtnUnavailable.Checked = True
        End If
        txtFloor.Text = dgvRoom.Item(6, i).Value.ToString
        txtDescription.Text = dgvRoom.Item(7, i).Value.ToString


    End Sub

    'when click on a row, fill form
    Private Sub dgvSearchResult_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvRoom.CellClick
        Dim i As Integer
        i = e.RowIndex
        If i >= 0 Then
            Dim selectedrow As DataGridViewRow
            selectedrow = dgvRoom.Rows(i)
            fillintheform(i)
        End If
    End Sub

    'when selected an item in combobox, the search text box allow user input
    Private Sub cbbFind_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbbFindBy.SelectedIndexChanged
        Dim selectedIndex As Integer = cbbFindBy.SelectedIndex
        txtfindRoomvalue.Enabled = True
    End Sub

    'validate search input
    Private Function validatesearchinput() As Boolean
        Dim oValidation As New Validation
        Dim tt As New ToolTip()
        Dim bIsValid As Boolean = True

        If cbbFindBy.SelectedIndex = 0 Then
            bIsValid = oValidation.istext(txtfindRoomvalue.Text)
            If bIsValid Then
                picErrorField.Visible = False
            Else
                picErrorField.Visible = True
                tt.SetToolTip(picErrorField, "Please enter a right format")
            End If
        End If

        If cbbFindBy.SelectedIndex = 1 Then
            bIsValid = oValidation.IsNumericVal(txtfindRoomvalue.Text)
            If bIsValid Then
                picErrorField.Visible = False
            Else
                picErrorField.Visible = True
                tt.SetToolTip(picErrorField, "Please enter a right format")
            End If
        End If

        Return bIsValid

    End Function

    'find room type sub
    Private Sub findRoomType(ByVal search As String)
        Dim oController As DataController = New DataController
        Dim query As String = "Select * FROM room where type like '%" & search & "%';"
        Dim adpt As New OleDbDataAdapter(query, oConnection)
        Dim ds As New DataSet()
        adpt.Fill(ds, "load")
        dgvRoom.DataSource = ds.Tables(0)
        Dim view As New DataView(ds.Tables(0))
    End Sub

    'find room number
    Private Sub findRoomNumber(ByVal search As String)
        Dim oController As DataController = New DataController
        Dim query As String = "Select * FROM room where room_number = " & search & ";"
        Dim adpt As New OleDbDataAdapter(query, oConnection)
        Dim ds As New DataSet()
        adpt.Fill(ds, "load")
        dgvRoom.DataSource = ds.Tables(0)
    End Sub

    'find record and load  record to form
    Private Sub btnfindroom_Click(sender As Object, e As EventArgs) Handles btnsearchroom.Click
        Dim oController As DataController = New DataController
        Dim search As String
        Dim bisvalid = validatesearchinput()
        If cbbFindBy.SelectedIndex = 0 And bisvalid Then
            search = txtfindRoomvalue.Text
            findRoomType(search)
            If dgvRoom.Rows.Count > 0 Then
                fillintheform(0)
            End If
        End If

        If cbbFindBy.SelectedIndex = 1 And bisvalid Then
            search = txtfindRoomvalue.Text
            findRoomNumber(search)
            If dgvRoom.Rows.Count > 0 Then
                fillintheform(0)
            End If
        End If

    End Sub

    'edit/update
    Private Sub btnEdit_Click(sender As Object, e As EventArgs) Handles btnUpdate.Click
        txtRoomNumber.Enabled = True
        txtType.Enabled = True
        txtPrice.Enabled = True
        txtNumberOfBeds.Enabled = True
        radiobtnAvailable.Enabled = True
        radiobtnUnavailable.Enabled = True
        txtFloor.Enabled = True
        txtDescription.Enabled = True

    End Sub

    'save the updated record
    Private Sub btnsave_Click(sender As Object, e As EventArgs) Handles btnsave.Click
        Dim tt As New ToolTip()
        Dim oValidation As New Validation
        Dim bIsValid As Boolean
        Dim bAllFieldsValid As Boolean = True

        If txtRoomNumber.Enabled = False Then Exit Sub

        'Check Room Number
        bIsValid = IsNumeric(txtRoomNumber.Text)
        If bIsValid Then
            picErrorField1.Visible = False
        Else
            picErrorField1.Visible = True
            tt.SetToolTip(picErrorField1, "Please enter room number")
            bAllFieldsValid = False
        End If

        'Check room type
        bIsValid = oValidation.istext(txtType.Text)
        If bIsValid Then
            picErrorField2.Visible = False
        Else
            picErrorField2.Visible = True
            tt.SetToolTip(picErrorField2, "Room type is text only")
            bAllFieldsValid = False
        End If

        'Check price
        bIsValid = IsNumeric(txtPrice.Text)
        If bIsValid Then
            picErrorField3.Visible = False
        Else
            picErrorField3.Visible = True
            tt.SetToolTip(picErrorField3, "Please enter numerical price")
            bAllFieldsValid = False
        End If

        'check number of bed
        bIsValid = IsNumeric(txtNumberOfBeds.Text)
        If bIsValid Then
            picErrorField4.Visible = False
        Else
            picErrorField4.Visible = True
            tt.SetToolTip(picErrorField4, "Please enter number of bed")
            bAllFieldsValid = False
        End If

        'check Availability
        If radiobtnAvailable.Checked = False And radiobtnUnavailable.Checked = False Then
            picErrorField5.Visible = True
            tt.SetToolTip(picErrorField5, "Please choose availability")
            bAllFieldsValid = False
        Else picErrorField5.Visible = False
        End If

        'check floor
        bIsValid = IsNumeric(txtFloor.Text)
        If bIsValid Then
            picErrorField6.Visible = False
        Else
            picErrorField6.Visible = True
            tt.SetToolTip(picErrorField6, "Please enter floor number")
            bAllFieldsValid = False
        End If

        'check description
        bIsValid = oValidation.IsAlphaNumeric(txtDescription.Text)
        If bIsValid Then
            picErrorField7.Visible = False
        Else
            picErrorField7.Visible = True
            tt.SetToolTip(picErrorField7, "Please enter description")
            bAllFieldsValid = False
        End If

        'check all field
        If bAllFieldsValid Then
            Debug.Print("All fields are valid")
        End If

        ' Instantiate a hashtable and populate it with form data

        If bAllFieldsValid = True Then
            Dim htData As Hashtable = New Hashtable

            'take data from textbox
            htData("room_number") = txtRoomNumber.Text
            htData("type") = txtType.Text
            htData("price") = txtPrice.Text
            htData("num_bed") = txtNumberOfBeds.Text
            htData("floor") = txtFloor.Text
            htData("description") = txtDescription.Text
            If radiobtnAvailable.Checked = True Then
                htData("availability") = True
            ElseIf radiobtnUnavailable.Checked = True Then
                htData("availability") = False
            End If
            htData("room_id") = RoomID

            Dim oDataController As DataController = New DataController
            Dim iNumRows = oDataController.updateroom(htData)
            If iNumRows = 1 Then
                MsgBox("The room information is updated.")
                loadDatabase()
            Else
                MsgBox("The room information is NOT updated.")
            End If
        End If

    End Sub

    'delete record
    Private Sub btntDelete_Click(sender As Object, e As EventArgs) Handles btntDelete.Click
        Dim oDataController As DataController = New DataController
        Dim iNumRows = oDataController.deleteroom(CStr(RoomID))

        If iNumRows = 1 Then
            MsgBox("The room information is Deleted.")
            loadDatabase()
        Else
            MsgBox("The room information is NOT Deleted. ")
        End If
    End Sub


    'move to next record
    Private Sub btnNext_Click(sender As Object, e As EventArgs) Handles BtnNext.Click
        If dgvRoom.CurrentRow.Index < dgvRoom.RowCount - 1 Then
            dgvRoom.CurrentCell = dgvRoom.Item(0, dgvRoom.CurrentRow.Index + 1)
            fillintheform(dgvRoom.CurrentRow.Index)
        End If
    End Sub

    'move to previous record
    Private Sub btnPrev_Click(sender As Object, e As EventArgs) Handles BtnPrev.Click
        If dgvRoom.CurrentRow.Index > 0 Then
            dgvRoom.CurrentCell = dgvRoom.Item(0, dgvRoom.CurrentRow.Index - 1)
            fillintheform(dgvRoom.CurrentRow.Index)
        End If
    End Sub

    'move to first record
    Private Sub btnfirst_Click(sender As Object, e As EventArgs) Handles btnfirst.Click
        dgvRoom.CurrentCell = dgvRoom.Item(0, 0)
        fillintheform(dgvRoom.CurrentRow.Index)
    End Sub

    'move to last record
    Private Sub btnlast_Click(sender As Object, e As EventArgs) Handles btnlast.Click
        dgvRoom.CurrentCell = dgvRoom.Item(0, dgvRoom.RowCount - 1)
        fillintheform(dgvRoom.CurrentRow.Index)
    End Sub

    'go to booking form
    Private Sub MenuToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles MenuToolStripMenuItem.Click
        Booking.Show()
        Me.Close()
    End Sub

End Class