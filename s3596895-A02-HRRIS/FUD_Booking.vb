﻿Option Explicit On
Option Strict On
Imports System.Drawing
Imports System.IO
Imports System
Imports System.Data
Imports System.Data.OleDb
Imports System.Data.Odbc
Imports System.Data.DataTable
'Author: Dao Ngoc Minh 
'studen id: s3596895
'This code is written by Dao Ngoc Minh s3596895
Public Class FuD_Booking

    Dim bookingID As Integer
    Dim oConnection As New OleDbConnection(DataController.CONNECTION_STRING)

    'open all the record and load the first record on form
    Private Sub FindBooking_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim oController As DataController = New DataController
        loadDatabase()
        If dgvBooking.Rows.Count > 1 Then fillintheform(0)
    End Sub

    'find all the record
    Private Sub loadDatabase()
        Dim oController As DataController = New DataController
        Dim query As String = "select booking.booking_id, booking.bdate, booking.num_days, booking.num_guests, booking.checkin_date, booking.comments, booking.customer_id, booking.room_id ,booking.total_price, customer.firstname, customer.lastname, room.type, room.room_number, room.price From room INNER Join (customer INNER Join booking On Customer.customer_id = booking.customer_id) ON room.room_id = booking.room_id;"
        Dim adpt As New OleDbDataAdapter(query, oConnection)
        Dim ds As New DataSet()
        adpt.Fill(ds)
        dgvBooking.DataSource = ds.Tables(0)
    End Sub

    'table columm order:
    'booking id 0
    'bdate 1
    'num_days 2
    'num_guests 3
    'checkin_date 4
    'comments 5 
    'customer_id 6
    'room_id 7 
    'total_price 8
    'firstname 9
    'lastname 10
    'type 11
    'room_number 11
    'price 13
    Private Sub fillintheform(ByVal i As Integer)
        bookingID = Convert.ToInt16(dgvBooking.Item(0, i).Value)
        txtNumberDays.Text = dgvBooking.Item(2, i).Value.ToString
        txtNumberGuests.Text = dgvBooking.Item(3, i).Value.ToString
        CheckinDate.Text = dgvBooking.Item(4, i).Value.ToString
        txtComment.Text = dgvBooking.Item(5, i).Value.ToString
        cbbCustomerID.Text = dgvBooking.Item(6, i).Value.ToString
        cbbRoomID.Text = dgvBooking.Item(7, i).Value.ToString
        txtTotalPrice.Text = dgvBooking.Item(8, i).Value.ToString
        txtfirstname.Text = dgvBooking.Item(9, i).Value.ToString
        txtlastname.Text = dgvBooking.Item(10, i).Value.ToString
        txttype.Text = dgvBooking.Item(11, i).Value.ToString
        txtRoomNumber.Text = dgvBooking.Item(12, i).Value.ToString
        txtRoomPrice.Text = dgvBooking.Item(13, i).Value.ToString
    End Sub

    'when click on a row, fill form
    Private Sub dgvSearchResult_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvBooking.CellClick
        Dim i As Integer
        i = e.RowIndex
        If i >= 0 Then
            Dim selectedrow As DataGridViewRow
            selectedrow = dgvBooking.Rows(i)
            fillintheform(i)
        End If
    End Sub

    'when selected an item in combobox, the search text box allow user input
    Private Sub cbbFind_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbbFindBy.SelectedIndexChanged
        Dim selectedIndex As Integer = cbbFindBy.SelectedIndex
        txtfindbookingvalue.Enabled = True
    End Sub

    'validate input for search
    Private Function validateinput() As Boolean
        Dim oValidation As New Validation
        Dim tt As New ToolTip()
        Dim bIsValid As Boolean = True

        If cbbFindBy.SelectedIndex = 0 Then
            bIsValid = oValidation.istext(txtfindbookingvalue.Text)
            If bIsValid Then
                picErrorField.Visible = False
            Else
                picErrorField.Visible = True
                tt.SetToolTip(picErrorField, "Please enter a right format")
            End If
        End If

        If cbbFindBy.SelectedIndex = 1 Then
            bIsValid = oValidation.IsNumericVal(txtfindbookingvalue.Text)
            If bIsValid Then
                picErrorField.Visible = False
            Else
                picErrorField.Visible = True
                tt.SetToolTip(picErrorField, "Please enter a right format")
            End If
        End If

        Return bIsValid

    End Function

    'find customer name sub
    Private Sub findcustomername(ByVal search As String)
        Dim oController As DataController = New DataController
        Dim query As String = "select booking.booking_id, booking.bdate, booking.num_days, booking.num_guests, booking.checkin_date, booking.comments, booking.customer_id, booking.room_id ,booking.total_price, customer.firstname, customer.lastname, room.type, room.room_number, room.price From room INNER Join (customer INNER Join booking On Customer.customer_id = booking.customer_id) ON room.room_id = booking.room_id where customer.firstname like '%" & search & "%' or customer.lastname like '%" & search & "%';"
        Dim adpt As New OleDbDataAdapter(query, oConnection)
        Dim ds As New DataSet()
        adpt.Fill(ds, "load")
        dgvBooking.DataSource = ds.Tables(0)
    End Sub

    'find room number
    Private Sub findroomnumber(ByVal search As String)
        Dim oController As DataController = New DataController
        Dim query As String = "select booking.booking_id, booking.bdate, booking.num_days, booking.num_guests, booking.checkin_date, booking.comments, booking.customer_id, booking.room_id ,booking.total_price, customer.firstname, customer.lastname, room.type, room.room_number, room.price From room INNER Join (customer INNER Join booking On Customer.customer_id = booking.customer_id) ON room.room_id = booking.room_id where room.room_number = " & search & ";"
        Dim adpt As New OleDbDataAdapter(query, oConnection)
        Dim ds As New DataSet()
        adpt.Fill(ds, "load")
        dgvBooking.DataSource = ds.Tables(0)
        Dim view As New DataView(ds.Tables(0))
    End Sub

    'find booking record
    Private Sub btnfindbooking_Click(sender As Object, e As EventArgs) Handles btnsearchbooking.Click
        Dim oController As DataController = New DataController
        Dim search As String
        Dim bisvalid = validateinput()
        If cbbFindBy.SelectedIndex = 0 And bisvalid Then
            search = txtfindbookingvalue.Text
            findcustomername(search)
            If dgvBooking.Rows.Count > 0 Then
                fillintheform(0)
            End If
        End If

        If cbbFindBy.SelectedIndex = 1 And bisvalid Then
            search = txtfindbookingvalue.Text
            findroomnumber(search)
            If dgvBooking.Rows.Count > 0 Then
                fillintheform(0)
            End If
        End If
    End Sub

    'calculate total price
    Private Sub btncal_Click(sender As Object, e As EventArgs) Handles btncal.Click
        If txttype.Text IsNot "" And txtNumberDays.Text IsNot "" Then txtTotalPrice.Text = CStr(Convert.ToInt64(txttype.Text) * Convert.ToInt64(txtNumberDays.Text))
    End Sub

    'update buttun, allow user to input
    Private Sub btnEdit_Click(sender As Object, e As EventArgs) Handles btnbookingupdate.Click
        txtNumberDays.Enabled = True
        txtNumberGuests.Enabled = True
        CheckinDate.Enabled = True
        txtComment.Enabled = True
    End Sub

    'save update button, save the input from user to database
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim oValidation As New Validation
        Dim bIsValid As Boolean
        Dim bAllFieldsValid As Boolean
        bAllFieldsValid = True
        Dim tt As New ToolTip()

        If txtNumberDays.Enabled = False Then Exit Sub

        ' Check number of days is number
        bIsValid = IsNumeric(txtNumberDays.Text)
        If bIsValid Then
            PictureBox3.Visible = False
        Else
            PictureBox3.Visible = True
            tt.SetToolTip(PictureBox3, "Number of days must be a number")
            bAllFieldsValid = False
        End If

        ' Check number of guests is number
        bIsValid = IsNumeric(txtNumberGuests.Text)
        If bIsValid Then
            PictureBox4.Visible = False
        Else
            PictureBox4.Visible = True
            tt.SetToolTip(PictureBox4, "Number of guests must be a number")
            bAllFieldsValid = False
        End If

        ' Check total price not empty
        If txtTotalPrice.Text IsNot "" Then
            PictureBox5.Visible = False
        Else
            PictureBox5.Visible = True
            tt.SetToolTip(PictureBox5, "Please calculate the total price")
            bAllFieldsValid = False
        End If

        ' Check comment not empty
        If txtComment.Text IsNot "" Then
            PictureBox6.Visible = False
        Else
            PictureBox6.Visible = True
            tt.SetToolTip(PictureBox6, "Please choose a room")
            bAllFieldsValid = False
        End If

        'Validate all field
        If bAllFieldsValid Then
            Debug.Print("All fields are valid")
        End If

        ' Instantiate a hashtable and populate it with form data

        If bIsValid = True Then
            Dim htData As Hashtable = New Hashtable

            htData("checkin_date") = CheckinDate.Value
            htData("num_days") = txtNumberDays.Text
            htData("num_guests") = txtNumberGuests.Text
            htData("comments") = txtComment.Text
            htData("booking_id") = bookingID

            Dim oDataController As DataController = New DataController
            Dim iNumRows = oDataController.updatebooking(htData)
            dgvBooking.DataSource = Nothing
            dgvBooking.Refresh()
            loadDatabase()
            If iNumRows = 1 Then
                MsgBox("The booking information is updated.")
                loadDatabase()
            Else
                MsgBox("The booking information is NOT updated.")
            End If
        End If
    End Sub

    'delete record
    Private Sub btntDelete_Click_1(sender As Object, e As EventArgs) Handles Button2.Click
        Dim oDataController As DataController = New DataController
        Dim iNumRows = oDataController.deleteBooking(CStr(bookingID))
        If iNumRows = 1 Then
            MsgBox("The booking information is Deleted.")
            loadDatabase()
        Else
            MsgBox("The booking information is NOT Deleted.")
        End If
    End Sub

    'move to next record
    Private Sub btnPrev_Click(sender As Object, e As EventArgs) Handles BtnPrev.Click
        If dgvBooking.CurrentRow.Index > 0 Then
            dgvBooking.CurrentCell = dgvBooking.Item(0, dgvBooking.CurrentRow.Index - 1)
            fillintheform(dgvBooking.CurrentRow.Index)
        End If
    End Sub

    'move to previous record
    Private Sub btnNext_Click(sender As Object, e As EventArgs) Handles BtnNext.Click
        If dgvBooking.CurrentRow.Index < dgvBooking.RowCount - 1 Then
            dgvBooking.CurrentCell = dgvBooking.Item(0, dgvBooking.CurrentRow.Index + 1)
            fillintheform(dgvBooking.CurrentRow.Index)
        End If
    End Sub

    'go to booking form
    Private Sub MenuToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles MenuToolStripMenuItem.Click
        Booking.Show()
        Me.Close()
    End Sub

    'move to first record
    Private Sub btnfirst_Click(sender As Object, e As EventArgs) Handles btnfirst.Click
        dgvBooking.CurrentCell = dgvBooking.Item(0, 0)
        fillintheform(dgvBooking.CurrentRow.Index)
    End Sub

    'move to last record
    Private Sub btnLast_Click(sender As Object, e As EventArgs) Handles btnLast.Click
        dgvBooking.CurrentCell = dgvBooking.Item(0, dgvBooking.RowCount - 1)
        fillintheform(dgvBooking.CurrentRow.Index)
    End Sub
End Class