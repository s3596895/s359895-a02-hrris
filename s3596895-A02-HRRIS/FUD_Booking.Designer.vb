﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FuD_Booking
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FuD_Booking))
        Me.btnsave = New System.Windows.Forms.Button()
        Me.BtnNext = New System.Windows.Forms.Button()
        Me.BtnPrev = New System.Windows.Forms.Button()
        Me.btntDelete = New System.Windows.Forms.Button()
        Me.btnUpdate = New System.Windows.Forms.Button()
        Me.dgvBooking = New System.Windows.Forms.DataGridView()
        Me.gbxFind = New System.Windows.Forms.GroupBox()
        Me.btnsearchbooking = New System.Windows.Forms.Button()
        Me.picErrorField = New System.Windows.Forms.PictureBox()
        Me.txtfindbookingvalue = New System.Windows.Forms.TextBox()
        Me.cbbFindBy = New System.Windows.Forms.ComboBox()
        Me.lblFindBy = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.txtlastname = New System.Windows.Forms.TextBox()
        Me.PictureBox6 = New System.Windows.Forms.PictureBox()
        Me.lblCustomerID = New System.Windows.Forms.Label()
        Me.lblRoom = New System.Windows.Forms.Label()
        Me.txtfirstname = New System.Windows.Forms.TextBox()
        Me.txtTotalPrice = New System.Windows.Forms.TextBox()
        Me.lblComment = New System.Windows.Forms.Label()
        Me.btncal = New System.Windows.Forms.Button()
        Me.txttype = New System.Windows.Forms.TextBox()
        Me.txtRoomNumber = New System.Windows.Forms.TextBox()
        Me.lblTotalPrice = New System.Windows.Forms.Label()
        Me.txtRoomPrice = New System.Windows.Forms.TextBox()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.CheckinDate = New System.Windows.Forms.DateTimePicker()
        Me.PictureBox4 = New System.Windows.Forms.PictureBox()
        Me.PictureBox5 = New System.Windows.Forms.PictureBox()
        Me.cbbRoomID = New System.Windows.Forms.ComboBox()
        Me.cbbCustomerID = New System.Windows.Forms.ComboBox()
        Me.lblNumberDays = New System.Windows.Forms.Label()
        Me.txtNumberDays = New System.Windows.Forms.TextBox()
        Me.lblNumberGuests = New System.Windows.Forms.Label()
        Me.txtComment = New System.Windows.Forms.TextBox()
        Me.txtNumberGuests = New System.Windows.Forms.TextBox()
        Me.lblCheckinDate = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.btnbookingupdate = New System.Windows.Forms.Button()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.FileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.btnfirst = New System.Windows.Forms.Button()
        Me.btnLast = New System.Windows.Forms.Button()
        CType(Me.dgvBooking, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxFind.SuspendLayout()
        CType(Me.picErrorField, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnsave
        '
        Me.btnsave.Location = New System.Drawing.Point(657, 69)
        Me.btnsave.Name = "btnsave"
        Me.btnsave.Size = New System.Drawing.Size(75, 23)
        Me.btnsave.TabIndex = 175
        Me.btnsave.Text = "Save"
        Me.btnsave.UseVisualStyleBackColor = True
        '
        'BtnNext
        '
        Me.BtnNext.Location = New System.Drawing.Point(729, 296)
        Me.BtnNext.Name = "BtnNext"
        Me.BtnNext.Size = New System.Drawing.Size(96, 23)
        Me.BtnNext.TabIndex = 172
        Me.BtnNext.Text = "Next Record"
        Me.BtnNext.UseVisualStyleBackColor = True
        '
        'BtnPrev
        '
        Me.BtnPrev.Location = New System.Drawing.Point(612, 296)
        Me.BtnPrev.Name = "BtnPrev"
        Me.BtnPrev.Size = New System.Drawing.Size(96, 23)
        Me.BtnPrev.TabIndex = 171
        Me.BtnPrev.Text = "Previous Record"
        Me.BtnPrev.UseVisualStyleBackColor = True
        '
        'btntDelete
        '
        Me.btntDelete.Location = New System.Drawing.Point(501, 66)
        Me.btntDelete.Name = "btntDelete"
        Me.btntDelete.Size = New System.Drawing.Size(75, 23)
        Me.btntDelete.TabIndex = 169
        Me.btntDelete.Text = "Delete"
        Me.btntDelete.UseVisualStyleBackColor = True
        '
        'btnUpdate
        '
        Me.btnUpdate.Location = New System.Drawing.Point(859, 65)
        Me.btnUpdate.Name = "btnUpdate"
        Me.btnUpdate.Size = New System.Drawing.Size(75, 23)
        Me.btnUpdate.TabIndex = 168
        Me.btnUpdate.Text = "Update"
        Me.btnUpdate.UseVisualStyleBackColor = True
        '
        'dgvBooking
        '
        Me.dgvBooking.AllowUserToAddRows = False
        Me.dgvBooking.AllowUserToDeleteRows = False
        Me.dgvBooking.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvBooking.Location = New System.Drawing.Point(3, 335)
        Me.dgvBooking.Name = "dgvBooking"
        Me.dgvBooking.ReadOnly = True
        Me.dgvBooking.Size = New System.Drawing.Size(937, 275)
        Me.dgvBooking.TabIndex = 167
        '
        'gbxFind
        '
        Me.gbxFind.Controls.Add(Me.btnsearchbooking)
        Me.gbxFind.Controls.Add(Me.picErrorField)
        Me.gbxFind.Controls.Add(Me.txtfindbookingvalue)
        Me.gbxFind.Controls.Add(Me.cbbFindBy)
        Me.gbxFind.Controls.Add(Me.lblFindBy)
        Me.gbxFind.Location = New System.Drawing.Point(484, 49)
        Me.gbxFind.Name = "gbxFind"
        Me.gbxFind.Size = New System.Drawing.Size(470, 85)
        Me.gbxFind.TabIndex = 177
        Me.gbxFind.TabStop = False
        Me.gbxFind.Text = "Find"
        '
        'btnsearchbooking
        '
        Me.btnsearchbooking.Location = New System.Drawing.Point(173, 43)
        Me.btnsearchbooking.Name = "btnsearchbooking"
        Me.btnsearchbooking.Size = New System.Drawing.Size(96, 23)
        Me.btnsearchbooking.TabIndex = 147
        Me.btnsearchbooking.Text = "Search"
        Me.btnsearchbooking.UseVisualStyleBackColor = True
        '
        'picErrorField
        '
        Me.picErrorField.Image = CType(resources.GetObject("picErrorField.Image"), System.Drawing.Image)
        Me.picErrorField.Location = New System.Drawing.Point(444, 16)
        Me.picErrorField.Name = "picErrorField"
        Me.picErrorField.Size = New System.Drawing.Size(20, 20)
        Me.picErrorField.TabIndex = 146
        Me.picErrorField.TabStop = False
        Me.picErrorField.Visible = False
        '
        'txtfindbookingvalue
        '
        Me.txtfindbookingvalue.Enabled = False
        Me.txtfindbookingvalue.Location = New System.Drawing.Point(220, 16)
        Me.txtfindbookingvalue.Name = "txtfindbookingvalue"
        Me.txtfindbookingvalue.Size = New System.Drawing.Size(218, 20)
        Me.txtfindbookingvalue.TabIndex = 144
        '
        'cbbFindBy
        '
        Me.cbbFindBy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbbFindBy.Items.AddRange(New Object() {"Customer Name", "Room Number"})
        Me.cbbFindBy.Location = New System.Drawing.Point(89, 16)
        Me.cbbFindBy.Name = "cbbFindBy"
        Me.cbbFindBy.Size = New System.Drawing.Size(125, 21)
        Me.cbbFindBy.TabIndex = 143
        '
        'lblFindBy
        '
        Me.lblFindBy.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblFindBy.Location = New System.Drawing.Point(6, 17)
        Me.lblFindBy.Name = "lblFindBy"
        Me.lblFindBy.Size = New System.Drawing.Size(68, 20)
        Me.lblFindBy.TabIndex = 27
        Me.lblFindBy.Text = "Find By"
        Me.lblFindBy.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtlastname)
        Me.GroupBox1.Controls.Add(Me.PictureBox6)
        Me.GroupBox1.Controls.Add(Me.lblCustomerID)
        Me.GroupBox1.Controls.Add(Me.lblRoom)
        Me.GroupBox1.Controls.Add(Me.txtfirstname)
        Me.GroupBox1.Controls.Add(Me.txtTotalPrice)
        Me.GroupBox1.Controls.Add(Me.lblComment)
        Me.GroupBox1.Controls.Add(Me.btncal)
        Me.GroupBox1.Controls.Add(Me.txttype)
        Me.GroupBox1.Controls.Add(Me.txtRoomNumber)
        Me.GroupBox1.Controls.Add(Me.lblTotalPrice)
        Me.GroupBox1.Controls.Add(Me.txtRoomPrice)
        Me.GroupBox1.Controls.Add(Me.PictureBox3)
        Me.GroupBox1.Controls.Add(Me.CheckinDate)
        Me.GroupBox1.Controls.Add(Me.PictureBox4)
        Me.GroupBox1.Controls.Add(Me.PictureBox5)
        Me.GroupBox1.Controls.Add(Me.cbbRoomID)
        Me.GroupBox1.Controls.Add(Me.cbbCustomerID)
        Me.GroupBox1.Controls.Add(Me.lblNumberDays)
        Me.GroupBox1.Controls.Add(Me.txtNumberDays)
        Me.GroupBox1.Controls.Add(Me.lblNumberGuests)
        Me.GroupBox1.Controls.Add(Me.txtComment)
        Me.GroupBox1.Controls.Add(Me.txtNumberGuests)
        Me.GroupBox1.Controls.Add(Me.lblCheckinDate)
        Me.GroupBox1.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.GroupBox1.Location = New System.Drawing.Point(3, 44)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(461, 285)
        Me.GroupBox1.TabIndex = 176
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Booking information"
        '
        'txtlastname
        '
        Me.txtlastname.Enabled = False
        Me.txtlastname.Location = New System.Drawing.Point(296, 38)
        Me.txtlastname.Name = "txtlastname"
        Me.txtlastname.Size = New System.Drawing.Size(144, 20)
        Me.txtlastname.TabIndex = 172
        '
        'PictureBox6
        '
        Me.PictureBox6.Image = CType(resources.GetObject("PictureBox6.Image"), System.Drawing.Image)
        Me.PictureBox6.Location = New System.Drawing.Point(439, 225)
        Me.PictureBox6.Name = "PictureBox6"
        Me.PictureBox6.Size = New System.Drawing.Size(20, 20)
        Me.PictureBox6.TabIndex = 171
        Me.PictureBox6.TabStop = False
        Me.PictureBox6.Visible = False
        '
        'lblCustomerID
        '
        Me.lblCustomerID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblCustomerID.Location = New System.Drawing.Point(0, 38)
        Me.lblCustomerID.Name = "lblCustomerID"
        Me.lblCustomerID.Size = New System.Drawing.Size(98, 20)
        Me.lblCustomerID.TabIndex = 151
        Me.lblCustomerID.Text = "Customer"
        Me.lblCustomerID.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblRoom
        '
        Me.lblRoom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblRoom.Location = New System.Drawing.Point(0, 69)
        Me.lblRoom.Name = "lblRoom"
        Me.lblRoom.Size = New System.Drawing.Size(98, 20)
        Me.lblRoom.TabIndex = 152
        Me.lblRoom.Text = "Room"
        Me.lblRoom.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtfirstname
        '
        Me.txtfirstname.Enabled = False
        Me.txtfirstname.Location = New System.Drawing.Point(184, 38)
        Me.txtfirstname.Name = "txtfirstname"
        Me.txtfirstname.Size = New System.Drawing.Size(106, 20)
        Me.txtfirstname.TabIndex = 160
        '
        'txtTotalPrice
        '
        Me.txtTotalPrice.Enabled = False
        Me.txtTotalPrice.Location = New System.Drawing.Point(104, 195)
        Me.txtTotalPrice.Name = "txtTotalPrice"
        Me.txtTotalPrice.Size = New System.Drawing.Size(231, 20)
        Me.txtTotalPrice.TabIndex = 164
        '
        'lblComment
        '
        Me.lblComment.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblComment.Location = New System.Drawing.Point(0, 225)
        Me.lblComment.Name = "lblComment"
        Me.lblComment.Size = New System.Drawing.Size(98, 20)
        Me.lblComment.TabIndex = 157
        Me.lblComment.Text = "Comment"
        Me.lblComment.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'btncal
        '
        Me.btncal.Location = New System.Drawing.Point(341, 193)
        Me.btncal.Name = "btncal"
        Me.btncal.Size = New System.Drawing.Size(75, 20)
        Me.btncal.TabIndex = 170
        Me.btncal.Text = "Calc"
        Me.btncal.UseVisualStyleBackColor = True
        '
        'txttype
        '
        Me.txttype.Enabled = False
        Me.txttype.Location = New System.Drawing.Point(184, 70)
        Me.txttype.Name = "txttype"
        Me.txttype.Size = New System.Drawing.Size(106, 20)
        Me.txttype.TabIndex = 162
        '
        'txtRoomNumber
        '
        Me.txtRoomNumber.Enabled = False
        Me.txtRoomNumber.Location = New System.Drawing.Point(296, 70)
        Me.txtRoomNumber.Name = "txtRoomNumber"
        Me.txtRoomNumber.Size = New System.Drawing.Size(60, 20)
        Me.txtRoomNumber.TabIndex = 163
        '
        'lblTotalPrice
        '
        Me.lblTotalPrice.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblTotalPrice.Location = New System.Drawing.Point(0, 194)
        Me.lblTotalPrice.Name = "lblTotalPrice"
        Me.lblTotalPrice.Size = New System.Drawing.Size(98, 20)
        Me.lblTotalPrice.TabIndex = 156
        Me.lblTotalPrice.Text = "Total price"
        Me.lblTotalPrice.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtRoomPrice
        '
        Me.txtRoomPrice.Enabled = False
        Me.txtRoomPrice.Location = New System.Drawing.Point(364, 70)
        Me.txtRoomPrice.Name = "txtRoomPrice"
        Me.txtRoomPrice.Size = New System.Drawing.Size(76, 20)
        Me.txtRoomPrice.TabIndex = 161
        '
        'PictureBox3
        '
        Me.PictureBox3.Image = CType(resources.GetObject("PictureBox3.Image"), System.Drawing.Image)
        Me.PictureBox3.Location = New System.Drawing.Point(439, 100)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(20, 20)
        Me.PictureBox3.TabIndex = 167
        Me.PictureBox3.TabStop = False
        Me.PictureBox3.Visible = False
        '
        'CheckinDate
        '
        Me.CheckinDate.Enabled = False
        Me.CheckinDate.Location = New System.Drawing.Point(104, 161)
        Me.CheckinDate.Name = "CheckinDate"
        Me.CheckinDate.Size = New System.Drawing.Size(200, 20)
        Me.CheckinDate.TabIndex = 150
        '
        'PictureBox4
        '
        Me.PictureBox4.Image = CType(resources.GetObject("PictureBox4.Image"), System.Drawing.Image)
        Me.PictureBox4.Location = New System.Drawing.Point(439, 135)
        Me.PictureBox4.Name = "PictureBox4"
        Me.PictureBox4.Size = New System.Drawing.Size(20, 20)
        Me.PictureBox4.TabIndex = 168
        Me.PictureBox4.TabStop = False
        Me.PictureBox4.Visible = False
        '
        'PictureBox5
        '
        Me.PictureBox5.Image = CType(resources.GetObject("PictureBox5.Image"), System.Drawing.Image)
        Me.PictureBox5.Location = New System.Drawing.Point(439, 193)
        Me.PictureBox5.Name = "PictureBox5"
        Me.PictureBox5.Size = New System.Drawing.Size(20, 20)
        Me.PictureBox5.TabIndex = 169
        Me.PictureBox5.TabStop = False
        Me.PictureBox5.Visible = False
        '
        'cbbRoomID
        '
        Me.cbbRoomID.Enabled = False
        Me.cbbRoomID.FormattingEnabled = True
        Me.cbbRoomID.Location = New System.Drawing.Point(104, 68)
        Me.cbbRoomID.Name = "cbbRoomID"
        Me.cbbRoomID.Size = New System.Drawing.Size(66, 21)
        Me.cbbRoomID.TabIndex = 166
        '
        'cbbCustomerID
        '
        Me.cbbCustomerID.Enabled = False
        Me.cbbCustomerID.FormattingEnabled = True
        Me.cbbCustomerID.Location = New System.Drawing.Point(104, 39)
        Me.cbbCustomerID.Name = "cbbCustomerID"
        Me.cbbCustomerID.Size = New System.Drawing.Size(66, 21)
        Me.cbbCustomerID.TabIndex = 165
        '
        'lblNumberDays
        '
        Me.lblNumberDays.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblNumberDays.Location = New System.Drawing.Point(0, 100)
        Me.lblNumberDays.Name = "lblNumberDays"
        Me.lblNumberDays.Size = New System.Drawing.Size(98, 20)
        Me.lblNumberDays.TabIndex = 153
        Me.lblNumberDays.Text = "Number of days"
        Me.lblNumberDays.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtNumberDays
        '
        Me.txtNumberDays.Enabled = False
        Me.txtNumberDays.Location = New System.Drawing.Point(104, 100)
        Me.txtNumberDays.Name = "txtNumberDays"
        Me.txtNumberDays.Size = New System.Drawing.Size(100, 20)
        Me.txtNumberDays.TabIndex = 149
        '
        'lblNumberGuests
        '
        Me.lblNumberGuests.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblNumberGuests.Location = New System.Drawing.Point(0, 131)
        Me.lblNumberGuests.Name = "lblNumberGuests"
        Me.lblNumberGuests.Size = New System.Drawing.Size(98, 20)
        Me.lblNumberGuests.TabIndex = 154
        Me.lblNumberGuests.Text = "Number of guests"
        Me.lblNumberGuests.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtComment
        '
        Me.txtComment.Enabled = False
        Me.txtComment.Location = New System.Drawing.Point(104, 226)
        Me.txtComment.Name = "txtComment"
        Me.txtComment.Size = New System.Drawing.Size(312, 20)
        Me.txtComment.TabIndex = 159
        '
        'txtNumberGuests
        '
        Me.txtNumberGuests.Enabled = False
        Me.txtNumberGuests.Location = New System.Drawing.Point(104, 132)
        Me.txtNumberGuests.Name = "txtNumberGuests"
        Me.txtNumberGuests.Size = New System.Drawing.Size(100, 20)
        Me.txtNumberGuests.TabIndex = 158
        '
        'lblCheckinDate
        '
        Me.lblCheckinDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblCheckinDate.Location = New System.Drawing.Point(0, 163)
        Me.lblCheckinDate.Name = "lblCheckinDate"
        Me.lblCheckinDate.Size = New System.Drawing.Size(98, 20)
        Me.lblCheckinDate.TabIndex = 155
        Me.lblCheckinDate.Text = "Check in Date"
        Me.lblCheckinDate.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(657, 156)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(96, 23)
        Me.Button1.TabIndex = 180
        Me.Button1.Text = "Save Update"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(490, 156)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(96, 23)
        Me.Button2.TabIndex = 179
        Me.Button2.Text = "Delete"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'btnbookingupdate
        '
        Me.btnbookingupdate.Location = New System.Drawing.Point(852, 156)
        Me.btnbookingupdate.Name = "btnbookingupdate"
        Me.btnbookingupdate.Size = New System.Drawing.Size(96, 23)
        Me.btnbookingupdate.TabIndex = 178
        Me.btnbookingupdate.Text = "Update"
        Me.btnbookingupdate.UseVisualStyleBackColor = True
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FileToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(954, 24)
        Me.MenuStrip1.TabIndex = 181
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'FileToolStripMenuItem
        '
        Me.FileToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MenuToolStripMenuItem})
        Me.FileToolStripMenuItem.Name = "FileToolStripMenuItem"
        Me.FileToolStripMenuItem.Size = New System.Drawing.Size(37, 20)
        Me.FileToolStripMenuItem.Text = "File"
        '
        'MenuToolStripMenuItem
        '
        Me.MenuToolStripMenuItem.Name = "MenuToolStripMenuItem"
        Me.MenuToolStripMenuItem.Size = New System.Drawing.Size(105, 22)
        Me.MenuToolStripMenuItem.Text = "Menu"
        '
        'btnfirst
        '
        Me.btnfirst.Location = New System.Drawing.Point(490, 296)
        Me.btnfirst.Name = "btnfirst"
        Me.btnfirst.Size = New System.Drawing.Size(96, 23)
        Me.btnfirst.TabIndex = 182
        Me.btnfirst.Text = "First Record"
        Me.btnfirst.UseVisualStyleBackColor = True
        '
        'btnLast
        '
        Me.btnLast.Location = New System.Drawing.Point(852, 296)
        Me.btnLast.Name = "btnLast"
        Me.btnLast.Size = New System.Drawing.Size(96, 23)
        Me.btnLast.TabIndex = 183
        Me.btnLast.Text = "Last Record"
        Me.btnLast.UseVisualStyleBackColor = True
        '
        'FuD_Booking
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(954, 611)
        Me.Controls.Add(Me.btnLast)
        Me.Controls.Add(Me.btnfirst)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.btnbookingupdate)
        Me.Controls.Add(Me.gbxFind)
        Me.Controls.Add(Me.btnsave)
        Me.Controls.Add(Me.BtnNext)
        Me.Controls.Add(Me.BtnPrev)
        Me.Controls.Add(Me.btntDelete)
        Me.Controls.Add(Me.btnUpdate)
        Me.Controls.Add(Me.dgvBooking)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.MenuStrip1)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "FuD_Booking"
        Me.Text = "FUD_Booking"
        CType(Me.dgvBooking, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxFind.ResumeLayout(False)
        Me.gbxFind.PerformLayout()
        CType(Me.picErrorField, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btnsave As Button
    Friend WithEvents BtnNext As Button
    Friend WithEvents BtnPrev As Button
    Friend WithEvents btntDelete As Button
    Friend WithEvents btnUpdate As Button
    Friend WithEvents dgvBooking As DataGridView
    Friend WithEvents gbxFind As GroupBox
    Friend WithEvents btnsearchbooking As Button
    Friend WithEvents picErrorField As PictureBox
    Friend WithEvents txtfindbookingvalue As TextBox
    Friend WithEvents cbbFindBy As ComboBox
    Friend WithEvents lblFindBy As Label
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents lblCustomerID As Label
    Friend WithEvents lblRoom As Label
    Friend WithEvents txtfirstname As TextBox
    Friend WithEvents lblComment As Label
    Friend WithEvents txttype As TextBox
    Friend WithEvents txtRoomNumber As TextBox
    Friend WithEvents lblTotalPrice As Label
    Friend WithEvents txtRoomPrice As TextBox
    Friend WithEvents PictureBox3 As PictureBox
    Friend WithEvents PictureBox4 As PictureBox
    Friend WithEvents cbbRoomID As ComboBox
    Friend WithEvents cbbCustomerID As ComboBox
    Friend WithEvents lblNumberDays As Label
    Friend WithEvents lblNumberGuests As Label
    Friend WithEvents txtNumberGuests As TextBox
    Friend WithEvents lblCheckinDate As Label
    Friend WithEvents Button1 As Button
    Friend WithEvents Button2 As Button
    Friend WithEvents btnbookingupdate As Button
    Friend WithEvents PictureBox6 As PictureBox
    Friend WithEvents txtTotalPrice As TextBox
    Friend WithEvents btncal As Button
    Friend WithEvents PictureBox5 As PictureBox
    Friend WithEvents CheckinDate As DateTimePicker
    Friend WithEvents txtComment As TextBox
    Friend WithEvents txtNumberDays As TextBox
    Friend WithEvents MenuStrip1 As MenuStrip
    Friend WithEvents FileToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents MenuToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents txtlastname As TextBox
    Friend WithEvents btnfirst As Button
    Friend WithEvents btnLast As Button
End Class
