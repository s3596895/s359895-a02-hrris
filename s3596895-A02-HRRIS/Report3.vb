﻿Option Explicit On
Option Strict On

Imports System.Drawing
Imports System.IO
'Author: Dao Ngoc Minh 
'studen id: s3596895
'This code is written by Dao Ngoc Minh s3596895
Public Class Report3
    Dim ocontroller As DataController = New DataController
    Private Sub Report3_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Dim lsdata = ocontroller.findAllCustomer()
        'item for customer ID
        For Each customerID In lsdata
            cbbCusId.Items.Add(CStr(customerID("customer_id")))
        Next
        'Check duplicate combobox customer id item
        For i As Integer = 0 To cbbCusId.Items.Count - 2 ' a loop from the first element to the next to last element
            For j As Integer = cbbCusId.Items.Count - 1 To i + 1 Step -1 ' decrementing loop from the last element to the first, Because the second loop is counting down rather than up, you don't need to go all the way to the last item in the first loop
                If cbbCusId.Items(i).ToString = cbbCusId.Items(j).ToString Then
                    cbbCusId.Items.RemoveAt(j)
                End If
            Next
        Next

    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Dim ovalidation As New Validation
        Dim bAllFieldsValid As Boolean

        bAllFieldsValid = True
        Dim tt As New ToolTip()
        'check customer not empty
        If cbbCusId.SelectedIndex = -1 Then
            p1.Visible = True
            tt.SetToolTip(p1, "You must select a customer id")
            bAllFieldsValid = False
        Else p1.Visible = False
        End If
        'check year
        If ovalidation.validateyear(txtBookingYear.Text) Then
            p2.Visible = False
        Else
            p2.Visible = True
            tt.SetToolTip(p2, "Number of years must be a number")
            bAllFieldsValid = False

        End If
        'check month
        If cbbBookingMonth.SelectedIndex = -1 Then
            p3.Visible = True
            tt.SetToolTip(p3, "You must select a month")
            bAllFieldsValid = False

        Else p3.Visible = False
        End If

        If bAllFieldsValid Then
            Debug.Print("All fields are valid")
            ocontroller.createReport3(cbbCusId.SelectedItem.ToString, cbbBookingMonth.SelectedItem.ToString, txtBookingYear.Text.ToString)
        End If

    End Sub

End Class