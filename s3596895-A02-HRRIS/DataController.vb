﻿Option Explicit On
Option Strict On
Imports System.Data.OleDb
Imports System.IO
'Name: Dao Ngoc Minh
'Description: Class acting As intermediary between the form And database
'Contains most of the CRUD business logic and report
'Author:Dao Ngoc Minh s3596895
'Date: 22/11/2017
Public Class DataController

    Public Const CONNECTION_STRING As String =
        "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=Database.accdb"
    'insert customer data
    Public Function insertCustomer(ByVal htData As Hashtable) As Integer
        'established connection to data source
        Dim oConnection As OleDbConnection = New OleDbConnection(CONNECTION_STRING)
        Dim iNumRows As Integer

        Try
            MsgBox("Connection string: " & oConnection.ConnectionString)

            oConnection.Open()
            Dim oCommand As OleDbCommand = New OleDbCommand
            oCommand.Connection = oConnection

            'insert value from customer form to parameter of sql query
            oCommand.CommandText =
              "INSERT INTO Customer (title, firstname, lastname, phone, address, email, dob, gender) VALUES (?, ?, ?, ?, ?, ?, ?, ?);"
            oCommand.Parameters.Add("title", OleDbType.VarChar, 10)
            oCommand.Parameters.Add("firstname", OleDbType.VarChar, 255)
            oCommand.Parameters.Add("lastname", OleDbType.VarChar, 255)
            oCommand.Parameters.Add("phone", OleDbType.BigInt, 255)
            oCommand.Parameters.Add("address", OleDbType.VarChar, 255)
            oCommand.Parameters.Add("email", OleDbType.VarChar, 255)
            oCommand.Parameters.Add("dob", OleDbType.Date, 255)
            oCommand.Parameters.Add("gender", OleDbType.VarChar, 10)

            'take data from form
            oCommand.Parameters("title").Value = CStr(htData("title"))
            oCommand.Parameters("firstname").Value = CStr(htData("firstname"))
            oCommand.Parameters("lastname").Value = CStr(htData("lastname"))
            oCommand.Parameters("phone").Value = CInt(htData("phone"))
            oCommand.Parameters("address").Value = CStr(htData("address"))
            oCommand.Parameters("email").Value = CStr(htData("email"))
            oCommand.Parameters("dob").Value = CDate(htData("dob"))
            oCommand.Parameters("gender").Value = CStr(htData("gender"))

            'execute sql query
            oCommand.Prepare()
            iNumRows = oCommand.ExecuteNonQuery()

            'show error
        Catch ex As Exception
            MsgBox("ERROR: " & ex.Message)
            MsgBox("An error occured. The record wasn't inserted.")
        Finally
            oConnection.Close()
        End Try

        Return iNumRows

    End Function

    'insert room data
    Public Function addroom(ByVal htData As Hashtable) As Integer

        Dim oConnection As OleDbConnection = New OleDbConnection(CONNECTION_STRING)
        Dim iNumRows As Integer

        Try
            MsgBox("Connection string: " & oConnection.ConnectionString)
            oConnection.Open()
            Dim oCommand As OleDbCommand = New OleDbCommand
            oCommand.Connection = oConnection

            'insert value from room form to parameter of sql query
            oCommand.CommandText =
                "INSERT INTO room (room_number, type, price, num_bed, availability, floor, description) VALUES (?, ?, ?, ?, ?, ?, ?);"
            oCommand.Parameters.Add("room_number", OleDbType.Integer, 11)
            oCommand.Parameters.Add("type", OleDbType.VarChar, 255)
            oCommand.Parameters.Add("price", OleDbType.BigInt, 11)
            oCommand.Parameters.Add("num_bed", OleDbType.Integer, 11)
            oCommand.Parameters.Add("availability", OleDbType.Boolean, 255)
            oCommand.Parameters.Add("floor", OleDbType.Integer, 11)
            oCommand.Parameters.Add("description", OleDbType.VarChar, 255)

            'take data from form
            oCommand.Parameters("room_number").Value = CInt(htData("room_number"))
            oCommand.Parameters("type").Value = CStr(htData("type"))
            oCommand.Parameters("price").Value = CInt(htData("price"))
            oCommand.Parameters("num_bed").Value = CInt(htData("num_bed"))
            oCommand.Parameters("availability").Value = CBool(htData("availability"))
            oCommand.Parameters("floor").Value = CInt(htData("floor"))
            oCommand.Parameters("description").Value = CStr(htData("description"))

            'execute sql query
            oCommand.Prepare()
            iNumRows = oCommand.ExecuteNonQuery()

            'show error
        Catch ex As Exception
            MsgBox("ERROR: " & ex.Message)
            MsgBox("An error occured. The record wasn't inserted.")
        Finally
            oConnection.Close()
        End Try

        Return iNumRows
    End Function

    'insert into booking data
    Public Function insertbooking(ByVal htData As Hashtable) As Integer

        Dim oConnection As OleDbConnection = New OleDbConnection(CONNECTION_STRING)
        Dim iNumRows As Integer

        Try
            oConnection.Open()
            Dim oCommand As OleDbCommand = New OleDbCommand
            oCommand.Connection = oConnection

            'insert value from booking form to parameter of sql query
            oCommand.CommandText = "INSERT INTO booking (bdate, num_days, num_guests, checkin_date, comments, customer_id, room_id, total_price) VALUES (?, ?, ?, ?, ?, ?, ?, ?);"

            oCommand.Parameters.Add("bdate", OleDbType.Date)
            oCommand.Parameters.Add("num_days", OleDbType.Integer, 12)
            oCommand.Parameters.Add("num_guests", OleDbType.Integer, 12)
            oCommand.Parameters.Add("checkin_date", OleDbType.Date)
            oCommand.Parameters.Add("comments", OleDbType.VarChar, 255)
            oCommand.Parameters.Add("customer_id", OleDbType.Integer, 12)
            oCommand.Parameters.Add("room_id", OleDbType.Integer, 12)
            oCommand.Parameters.Add("total_price", OleDbType.BigInt, 20)
            'take data from form
            oCommand.Parameters("bdate").Value = CStr(htData("bdate"))
            oCommand.Parameters("num_days").Value = CInt(htData("num_days"))
            oCommand.Parameters("num_guests").Value = CInt(htData("num_guests"))
            oCommand.Parameters("checkin_date").Value = CStr(htData("checkin_date"))
            oCommand.Parameters("comments").Value = CStr(htData("comments"))
            oCommand.Parameters("customer_id").Value = CInt(htData("customer_id"))
            oCommand.Parameters("room_id").Value = CInt(htData("room_id"))
            oCommand.Parameters("total_price").Value = CLng(htData("total_price"))
            'execute sql query
            oCommand.Prepare()
            iNumRows = oCommand.ExecuteNonQuery()

            'show error
        Catch ex As Exception
            Debug.Print("ERROR: " & ex.Message)
            MsgBox("An error occured. The record wasn't inserted.")
        Finally
            oConnection.Close()
        End Try

        Return iNumRows

    End Function

    'insert into invoice form
    Public Function insertinvoice(ByVal htData As Hashtable) As Integer

        Dim oConnection As OleDbConnection = New OleDbConnection(CONNECTION_STRING)
        Dim iNumRows As Integer

        Try
            oConnection.Open()
            Dim oCommand As OleDbCommand = New OleDbCommand
            oCommand.Connection = oConnection

            'insert value from booking form to parameter of sql query
            oCommand.CommandText = "INSERT INTO invoice (booking_id, date_invoice, amount) VALUES (?, ?, ?);"

            oCommand.Parameters.Add("booking_id", OleDbType.Integer, 12)
            oCommand.Parameters.Add("date_invoice", OleDbType.Date)
            oCommand.Parameters.Add("amount", OleDbType.BigInt, 12)

            'take data from form
            oCommand.Parameters("booking_id").Value = CInt(htData("booking_id"))
            oCommand.Parameters("date_invoice").Value = CDate(htData("date_invoice"))
            oCommand.Parameters("amount").Value = CLng(htData("amount"))

            'execute sql query
            oCommand.Prepare()
            iNumRows = oCommand.ExecuteNonQuery()

            'show error
        Catch ex As Exception
            Debug.Print("ERROR: " & ex.Message)
            MsgBox("An error occured. The record wasn't inserted.")
        Finally
            oConnection.Close()
        End Try

        Return iNumRows

    End Function

    'insert invoice
    Private Function findallinvoice() As List(Of Hashtable)
        Dim oConnection As OleDbConnection = New OleDbConnection(CONNECTION_STRING)
        Dim lsData As New List(Of Hashtable)

        Try
            oConnection.Open()
            Dim oCommand As OleDbCommand = New OleDbCommand
            oCommand.Connection = oConnection

            'select all by sql
            oCommand.CommandText =
                    "SELECT * FROM room ORDER BY room_id;"
            oCommand.Prepare()
            Dim oDataReader = oCommand.ExecuteReader()

            'take suitable data from select all in sql
            Dim htTempData As Hashtable
            Do While oDataReader.Read() = True
                htTempData = New Hashtable
                htTempData("room_id") = CStr(oDataReader("room_id"))
                htTempData("room_number") = CStr(oDataReader("room_number"))
                htTempData("type") = CStr(oDataReader("type"))
                htTempData("price") = CStr(oDataReader("price"))
                htTempData("num_bed") = CStr(oDataReader("num_bed"))
                htTempData("availability") = CStr(oDataReader("availability"))
                htTempData("floor") = CStr(oDataReader("floor"))
                htTempData("description") = CStr(oDataReader("description"))
                lsData.Add(htTempData)
            Loop

            'for debug purpose
            Debug.Print("The records were found.")

            'show error
        Catch ex As Exception
            Debug.Print("ERROR: " & ex.Message)
            MsgBox("An error occurred. The records could not be found!")
        Finally
            oConnection.Close()
        End Try

        Return lsData
    End Function

    'find all data for roomID
    Public Function findAll() As List(Of Hashtable)

        Dim oConnection As OleDbConnection = New OleDbConnection(CONNECTION_STRING)
        Dim lsData As New List(Of Hashtable)

        Try
            oConnection.Open()
            Dim oCommand As OleDbCommand = New OleDbCommand
            oCommand.Connection = oConnection

            'select all by sql
            oCommand.CommandText =
                    "SELECT * FROM room ORDER BY room_id;"
            oCommand.Prepare()
            Dim oDataReader = oCommand.ExecuteReader()

            'take suitable data from select all in sql
            Dim htTempData As Hashtable
            Do While oDataReader.Read() = True
                htTempData = New Hashtable
                htTempData("room_id") = CStr(oDataReader("room_id"))
                htTempData("room_number") = CStr(oDataReader("room_number"))
                htTempData("type") = CStr(oDataReader("type"))
                htTempData("price") = CStr(oDataReader("price"))
                htTempData("num_bed") = CStr(oDataReader("num_bed"))
                htTempData("availability") = CStr(oDataReader("availability"))
                htTempData("floor") = CStr(oDataReader("floor"))
                htTempData("description") = CStr(oDataReader("description"))
                lsData.Add(htTempData)
            Loop

            'for debug purpose
            Debug.Print("The records were found.")

            'show error
        Catch ex As Exception
            Debug.Print("ERROR: " & ex.Message)
            MsgBox("An error occurred. The records could not be found!")
        Finally
            oConnection.Close()
        End Try

        Return lsData

    End Function

    'find all customer based on customer id
    Public Function findAllCustomer() As List(Of Hashtable)

        Dim oConnection As OleDbConnection = New OleDbConnection(CONNECTION_STRING)
        Dim lsData As New List(Of Hashtable)

        Try
            oConnection.Open()
            Dim oCommand As OleDbCommand = New OleDbCommand
            oCommand.Connection = oConnection

            'select all by sql
            oCommand.CommandText =
                    "SELECT * FROM customer ORDER BY customer_id;"
            oCommand.Prepare()
            Dim oDataReader = oCommand.ExecuteReader()

            'take suitable data from select all in sql
            Dim htTempData As Hashtable
            Do While oDataReader.Read() = True
                htTempData = New Hashtable
                htTempData("customer_id") = CStr(oDataReader("customer_id"))
                htTempData("title") = CStr(oDataReader("title"))
                htTempData("firstname") = CStr(oDataReader("firstname"))
                htTempData("lastname") = CStr(oDataReader("lastname"))
                htTempData("phone") = CStr(oDataReader("phone"))
                htTempData("address") = CStr(oDataReader("address"))
                htTempData("email") = CStr(oDataReader("email"))
                htTempData("dob") = CDate(oDataReader("dob"))
                htTempData("gender") = CStr(oDataReader("gender"))
                lsData.Add(htTempData)
            Loop

            'for debug purpose
            Debug.Print("The records were found.")

            'show error
        Catch ex As Exception
            Debug.Print("ERROR: " & ex.Message)
            MsgBox("An error occurred. The records could not be found!")
        Finally
            oConnection.Close()
        End Try

        Return lsData

    End Function

    'find all booking
    Public Function findAllBooking() As List(Of Hashtable)
        Dim oConnection As OleDbConnection = New OleDbConnection(CONNECTION_STRING)
        Dim lsData As New List(Of Hashtable)

        Try
            Debug.Print("Connection string: " & oConnection.ConnectionString)

            oConnection.Open()
            Dim oCommand As OleDbCommand = New OleDbCommand
            oCommand.Connection = oConnection

            oCommand.CommandText =
                "SELECT * FROM booking ORDER BY booking_id;"

            oCommand.Prepare()

            Dim oDataReader = oCommand.ExecuteReader()

            Dim htTempData As Hashtable
            Do While oDataReader.Read() = True
                htTempData = New Hashtable
                htTempData("booking_id") = CStr(oDataReader("booking_id"))
                htTempData("bdate") = CDate(oDataReader("bdate"))
                htTempData("num_days") = CInt(oDataReader("num_days"))
                htTempData("num_guests") = CInt(oDataReader("num_guests"))
                htTempData("checkin_date") = CDate(oDataReader("checkin_date"))
                htTempData("comments") = CStr(oDataReader("comments"))
                htTempData("customer_id") = CInt(oDataReader("customer_id"))
                htTempData("room_id") = CInt(oDataReader("room_id"))
                htTempData("total_price") = CLng(oDataReader("total_price"))
                lsData.Add(htTempData)
            Loop
            Debug.Print("The record was found.")
        Catch ex As Exception
            Debug.Print("ERROR: " & ex.Message)
            MsgBox("An error occurred. The record(s) could not be found!")
        Finally
            oConnection.Close()
        End Try
        Return lsData
    End Function

    'update booking
    Public Function updatebooking(ByVal htData As Hashtable) As Integer
        Dim oConnection As OleDbConnection = New OleDbConnection(CONNECTION_STRING)
        Dim iNumRows As Integer
        Try
            Debug.Print("Connection string: " & oConnection.ConnectionString)
            oConnection.Open()
            Dim oCommand As OleDbCommand = New OleDbCommand
            oCommand.Connection = oConnection

            oCommand.CommandText =
                "UPDATE booking SET checkin_date = ?,comments = ?, num_days = ?, num_guests = ? WHERE booking_id = ?;"

            oCommand.Parameters.Add("checkin_date", OleDbType.Date)
            oCommand.Parameters.Add("comments", OleDbType.VarChar, 255)
            oCommand.Parameters.Add("num_days", OleDbType.Integer, 12)
            oCommand.Parameters.Add("num_guests", OleDbType.Integer, 12)
            oCommand.Parameters.Add("booking_id", OleDbType.Integer, 12)

            oCommand.Parameters("checkin_date").Value = CDate(htData("checkin_date"))
            oCommand.Parameters("comments").Value = CStr(htData("comments"))
            oCommand.Parameters("num_days").Value = CInt(htData("num_days"))
            oCommand.Parameters("num_guests").Value = CInt(htData("num_guests"))
            oCommand.Parameters("booking_id").Value = CInt(htData("booking_id"))

            oCommand.Prepare()
            iNumRows = oCommand.ExecuteNonQuery()

            Debug.Print(CStr(iNumRows))

            Debug.Print("The record was updated.")
        Catch ex As Exception
            Debug.Print("ERROR: " & ex.Message)
        Finally
            oConnection.Close()
        End Try
        Return iNumRows
    End Function

    'update customer
    Public Function updatecustomer(ByVal htData As Hashtable) As Integer
        Dim oConnection As OleDbConnection = New OleDbConnection(CONNECTION_STRING)
        Dim iNumRows As Integer
        Try
            Debug.Print("Connection string: " & oConnection.ConnectionString)
            oConnection.Open()
            Dim oCommand As OleDbCommand = New OleDbCommand
            oCommand.Connection = oConnection

            oCommand.CommandText =
                "UPDATE customer SET title = ?, firstname = ?, lastname = ?, phone = ?, address = ?, email = ?, dob = ?, gender = ? WHERE customer_id = ?;"

            oCommand.Parameters.Add("title", OleDbType.VarChar, 10)
            oCommand.Parameters.Add("firstname", OleDbType.VarChar, 255)
            oCommand.Parameters.Add("lastname", OleDbType.VarChar, 255)
            oCommand.Parameters.Add("phone", OleDbType.BigInt, 255)
            oCommand.Parameters.Add("address", OleDbType.VarChar, 255)
            oCommand.Parameters.Add("email", OleDbType.VarChar, 255)
            oCommand.Parameters.Add("dob", OleDbType.Date, 255)
            oCommand.Parameters.Add("gender", OleDbType.VarChar, 10)
            oCommand.Parameters.Add("customer_id", OleDbType.Integer, 12)

            'take data from form
            oCommand.Parameters("title").Value = CStr(htData("title"))
            oCommand.Parameters("firstname").Value = CStr(htData("firstname"))
            oCommand.Parameters("lastname").Value = CStr(htData("lastname"))
            oCommand.Parameters("phone").Value = CInt(htData("phone"))
            oCommand.Parameters("address").Value = CStr(htData("address"))
            oCommand.Parameters("email").Value = CStr(htData("email"))
            oCommand.Parameters("dob").Value = CDate(htData("dob"))
            oCommand.Parameters("gender").Value = CStr(htData("gender"))
            oCommand.Parameters("customer_id").Value = CInt(htData("customer_id"))

            oCommand.Prepare()
            iNumRows = oCommand.ExecuteNonQuery()

            Debug.Print(CStr(iNumRows))

            Debug.Print("The record was updated.")
        Catch ex As Exception
            Debug.Print("ERROR: " & ex.Message)
        Finally
            oConnection.Close()
        End Try
        Return iNumRows
    End Function

    'update room
    Public Function updateroom(ByVal htData As Hashtable) As Integer
        Dim oConnection As OleDbConnection = New OleDbConnection(CONNECTION_STRING)
        Dim iNumRows As Integer
        Try
            Debug.Print("Connection string: " & oConnection.ConnectionString)
            oConnection.Open()
            Dim oCommand As OleDbCommand = New OleDbCommand
            oCommand.Connection = oConnection

            oCommand.CommandText =
                "UPDATE room SET room_number = ?, type = ?, price = ?, num_bed = ?, availability = ?, floor = ?, description = ? WHERE room_id = ?;"

            oCommand.Parameters.Add("room_number", OleDbType.Integer, 11)
            oCommand.Parameters.Add("type", OleDbType.VarChar, 255)
            oCommand.Parameters.Add("price", OleDbType.BigInt, 11)
            oCommand.Parameters.Add("num_bed", OleDbType.Integer, 11)
            oCommand.Parameters.Add("availability", OleDbType.Boolean, 255)
            oCommand.Parameters.Add("floor", OleDbType.Integer, 11)
            oCommand.Parameters.Add("description", OleDbType.VarChar, 255)
            oCommand.Parameters.Add("room_id", OleDbType.Integer, 12)

            'take data from form
            oCommand.Parameters("room_number").Value = CInt(htData("room_number"))
            oCommand.Parameters("type").Value = CStr(htData("type"))
            oCommand.Parameters("price").Value = CInt(htData("price"))
            oCommand.Parameters("num_bed").Value = CInt(htData("num_bed"))
            oCommand.Parameters("availability").Value = CBool(htData("availability"))
            oCommand.Parameters("floor").Value = CInt(htData("floor"))
            oCommand.Parameters("description").Value = CStr(htData("description"))
            oCommand.Parameters("room_id").Value = CInt(htData("room_id"))

            oCommand.Prepare()
            iNumRows = oCommand.ExecuteNonQuery()

            Debug.Print(CStr(iNumRows))

            Debug.Print("The record was updated.")
        Catch ex As Exception
            Debug.Print("ERROR: " & ex.Message)
        Finally
            oConnection.Close()
        End Try
        Return iNumRows
    End Function

    'delete booking
    Public Function deleteBooking(ByVal deletevar As String) As Integer
        Dim oConnection As OleDbConnection = New OleDbConnection(CONNECTION_STRING)
        Dim iNumRows As Integer

        If MsgBox("Are you sure?", MsgBoxStyle.YesNo, Title:="confirm") = vbYes Then
            Try
                Debug.Print("Connection string: " & oConnection.ConnectionString)

                oConnection.Open()
                Dim oCommand As OleDbCommand = New OleDbCommand
                oCommand.Connection = oConnection

                'TODO    
                oCommand.CommandText = "DELETE FROM booking WHERE booking_id = ?;"
                oCommand.Parameters.Add("booking_id", OleDbType.Integer, 12)
                oCommand.Parameters("booking_id").Value = CInt(deletevar)
                oCommand.Prepare()
                iNumRows = oCommand.ExecuteNonQuery()

                Debug.Print(CStr(iNumRows))
                Debug.Print("The record was deleted.")
            Catch ex As Exception
                Debug.Print("ERROR: " & ex.Message)
            Finally
                oConnection.Close()
            End Try
        Else
        End If
        Return iNumRows

    End Function

    'This function delete 1 customer record in findcustomer form
    Public Function deletecustomer(ByVal deletevar As String) As Integer
        Dim oConnection As OleDbConnection = New OleDbConnection(CONNECTION_STRING)
        Dim iNumRows As Integer

        If MsgBox("Are you sure?", MsgBoxStyle.YesNo, Title:="confirmation") = vbYes Then
            Try
                Debug.Print("Connection string: " & oConnection.ConnectionString)

                oConnection.Open()
                Dim oCommand As OleDbCommand = New OleDbCommand
                oCommand.Connection = oConnection

                oCommand.CommandText = "DELETE FROM customer WHERE customer_id = ?;"
                oCommand.Parameters.Add("customer_id", OleDbType.Integer, 12)
                oCommand.Parameters("customer_id").Value = CInt(deletevar)
                oCommand.Prepare()
                iNumRows = oCommand.ExecuteNonQuery()

                Debug.Print(CStr(iNumRows))
                Debug.Print("The record was deleted.")
            Catch ex As Exception
                Debug.Print("ERROR: " & ex.Message)
            Finally
                oConnection.Close()
            End Try
        Else
        End If

        Return iNumRows
    End Function

    'delete room
    Public Function deleteroom(ByVal deletevar As String) As Integer
        Dim oConnection As OleDbConnection = New OleDbConnection(CONNECTION_STRING)
        Dim iNumRows As Integer

        If MsgBox("Are you sure?", MsgBoxStyle.YesNo, Title:="confirmation") = vbYes Then
            Try
                Debug.Print("Connection string: " & oConnection.ConnectionString)

                oConnection.Open()
                Dim oCommand As OleDbCommand = New OleDbCommand
                oCommand.Connection = oConnection

                oCommand.CommandText = "DELETE FROM room WHERE room_id = ?;"
                oCommand.Parameters.Add("room_id", OleDbType.Integer, 12)
                oCommand.Parameters("room_id").Value = CInt(deletevar)
                oCommand.Prepare()
                iNumRows = oCommand.ExecuteNonQuery()

                Debug.Print(CStr(iNumRows))
                Debug.Print("The record was deleted.")
            Catch ex As Exception
                Debug.Print("ERROR: " & ex.Message)
            Finally
                oConnection.Close()
            End Try
        Else
        End If

        Return iNumRows
    End Function


    ''''''''''''''''''''''''''''''''''''''''Report 1'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    'report = create report = {generate report [generate table (find data based on sql query)] + save report} 
    'Create specific table data (find customer's last booking)  that need in report
    Public Function findlastBookingCustomerID(ByVal customerid As String) As List(Of Hashtable)
        Dim oConnection As OleDbConnection = New OleDbConnection(CONNECTION_STRING)
        Dim lsData As New List(Of Hashtable)

        Try
            oConnection.Open()
            Dim oCommand As OleDbCommand = New OleDbCommand
            oCommand.Connection = oConnection

            oCommand.CommandText =
                    "SELECT * FROM booking WHERE customer_id = ? ORDER BY bdate DESC;"

            oCommand.Parameters.Add("customer_id", OleDbType.Integer, 10)

            oCommand.Parameters("customer_id").Value = CStr(customerid)

            oCommand.Prepare()
            Dim oDataReader = oCommand.ExecuteReader()

            Dim htTempData As Hashtable
            Do While oDataReader.Read() = True
                htTempData = New Hashtable
                htTempData("booking_id") = CStr(oDataReader("booking_id"))
                htTempData("bdate") = CStr(oDataReader("bdate"))
                htTempData("num_days") = CStr(oDataReader("num_days"))
                htTempData("num_guests") = CStr(oDataReader("num_guests"))
                htTempData("checkin_date") = CStr(oDataReader("checkin_date"))
                htTempData("comments") = CStr(oDataReader("comments"))
                htTempData("customer_id") = CStr(oDataReader("customer_id"))
                htTempData("room_id") = CStr(oDataReader("room_id"))
                htTempData("total_price") = CStr(oDataReader("total_price"))
                lsData.Add(htTempData)
            Loop

            Debug.Print("The records were found.")

        Catch ex As Exception
            Debug.Print("ERROR: " & ex.Message)
            MsgBox("An error occurred. The records could not be found!")
        Finally
            oConnection.Close()
        End Try

        Return lsData
    End Function

    'create report 1
    Public Sub createReport1(ByVal customerid As String)

        Debug.Print("Create Report 1..")

        Dim sReportFilename = "BookingReport1.html"
        Dim sReportTitle = "Last booking of customer " & customerid & ":"

        'find all the records in the booking table with selected year and month. 
        Dim lsData = findlastBookingCustomerID(customerid)

        Dim sReportContent = generateReport1(lsData, sReportTitle)

        saveReport(sReportContent, sReportFilename)

        'Load the generated file in default browser.        
        Dim sParam As String = """" & Application.StartupPath & "\" & sReportFilename & """"
        Debug.Print("sParam: " & sParam)
        System.Diagnostics.Process.Start(sParam)
    End Sub

    'generate table for report 1
    Private Function generateTable1(ByVal lsData As List(Of Hashtable)) As String
        'Create the start of the table
        Dim sTable = "<table border=""1"">" & vbCrLf
        If lsData.Count = 0 Then
            sTable &= "<tr><td colspan = """">" & " This customer has not booked anything </td></tr>" & vbCrLf
            sTable &= "</table>" & vbCrLf
            Return sTable
        End If
        Dim htSample As Hashtable = lsData.Item(0)

        Dim lsKeys As List(Of String) = New List(Of String)
        lsKeys.Add("booking_id")
        lsKeys.Add("bdate")
        lsKeys.Add("num_days")


        'Generate the header row
        Dim sHeaderRow = "<tr>" & vbCrLf
        For Each key In lsKeys
            sHeaderRow &= "<th>" & CStr(key) & "</th>" & vbCrLf
        Next
        sHeaderRow &= "</tr>" & vbCrLf
        Debug.Print("sHeaderRow: " & sHeaderRow)
        sTable &= sHeaderRow

        'Generate the table rows
        For Each record In lsData
            Dim product As Hashtable = record
            Dim sTableRow = "<tr>" & vbCrLf
            For Each key In lsKeys
                sTableRow &= "<td>" & CStr(product(key)) & "</td>" & vbCrLf
            Next
            sTableRow &= "</tr>" & vbCrLf
            Debug.Print("sTableRow: " & sTableRow)
            sTable &= sTableRow
            Exit For 'show only the latest record
        Next

        'Generate the end of the table
        sTable &= "</table>" & vbCrLf

        Return sTable
    End Function

    'generate report 1
    Private Function generateReport1(ByVal lsData As List(Of Hashtable), ByVal sReportTitle As String) As String
        Debug.Print("Generate Report ...")

        Dim sReportContent As String

        '1. Create the start of the HTML report file
        Dim sDoctype As String = "<!DOCTYPE html>"
        Dim sHtmlStartTag As String = "<html lang=""en"">"
        Dim sHeadTitle As String = "<head><title>" & sReportTitle & "</title></head>"
        Dim sBodyStartTag As String = "<body>"
        Dim sReportHeading As String = "<h1>" & sReportTitle & "</h1>"
        sReportContent = sDoctype & vbCrLf & sHtmlStartTag & vbCrLf & vbCrLf & sHeadTitle & vbCrLf & sBodyStartTag & vbCrLf & sReportHeading & vbCrLf

        '2. create the table and rows.
        Dim sTable = generateTable1(lsData)

        'create the rows of the table
        sReportContent &= sTable & vbCrLf

        '3. create the end of the HTML report file
        Dim sBodyEndTag As String = "</body>"
        Dim sHTMLEndTag As String = "</html>"
        sReportContent &= sBodyEndTag & vbCrLf & sHTMLEndTag

        Return sReportContent

    End Function

    '<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<Report 2>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    'report = create report = {generate report [generate table (find data based on sql query)] + save report} 
    'contain function to show the last time the room booked and the total price
    Public Function findRoomID(ByVal roomnumber As String) As List(Of Hashtable)
        Dim oConnection As OleDbConnection = New OleDbConnection(CONNECTION_STRING)
        Dim lsData As New List(Of Hashtable)

        Try
            oConnection.Open()
            Dim oCommand As OleDbCommand = New OleDbCommand
            oCommand.Connection = oConnection

            oCommand.CommandText =
                    "SELECT room.room_number, booking.bdate, booking.total_price From room Right Join booking on room.room_id = booking.room_id where Room.room_number = ?;"
            oCommand.Parameters.Add("room_number", OleDbType.Integer, 10)

            oCommand.Parameters("room_number").Value = CStr(roomnumber)
            oCommand.Prepare()

            Dim oDataReader = oCommand.ExecuteReader()

            Dim htTempData As Hashtable
            Do While oDataReader.Read() = True
                htTempData = New Hashtable
                htTempData("room_number") = CStr(oDataReader("room_number"))
                htTempData("bdate") = CStr(oDataReader("bdate"))
                htTempData("total_price") = CStr(oDataReader("total_price"))

                lsData.Add(htTempData)
            Loop
            Debug.Print("The records were found.")

        Catch ex As Exception
            Debug.Print("ERROR: " & ex.Message)
            MsgBox("An error occurred. The records could not be found!")
        Finally
            oConnection.Close()
        End Try
        Return lsData

    End Function

    'create report 2
    Public Sub createReport2(ByVal roomnumber As String)

        Debug.Print("Create Report 2..")


        Dim sReportFilename = "BookingReport2.html"

        Dim sReportTitle = "Last booking of room " & roomnumber & ":"

        'find the record
        Dim lsData = findRoomID(roomnumber)

        Dim sReportContent = generateReport2(lsData, sReportTitle)

        saveReport(sReportContent, sReportFilename)

        'Load the generated file in your default browser.      
        Dim sParam As String = """" & Application.StartupPath & "\" & sReportFilename & """"
        Debug.Print("sParam: " & sParam)
        System.Diagnostics.Process.Start(sParam)

    End Sub

    'generate table for report 2
    Private Function generateTable2(ByVal lsData As List(Of Hashtable)) As String
        'Create the start of the table
        Dim sTable = "<table border=""1"">" & vbCrLf
        If lsData.Count = 0 Then
            sTable &= "<tr><td colspan = """">" & " This room is not booked </td></tr>" & vbCrLf
            sTable &= "</table>" & vbCrLf
            Return sTable
        End If

        Dim htSample As Hashtable = lsData.Item(0)
        Dim lsKeys As List(Of String) = New List(Of String)
        lsKeys.Add("room_number")
        lsKeys.Add("bdate")
        lsKeys.Add("total_price")

        'create the table and rows.
        Dim sHeaderRow = "<tr>" & vbCrLf
        For Each key In lsKeys
            sHeaderRow &= "<th>" & CStr(key) & "</th>" & vbCrLf
        Next
        sHeaderRow &= "</tr>" & vbCrLf
        Debug.Print("sHeaderRow: " & sHeaderRow)
        sTable &= sHeaderRow

        'Generate the all rows in the table
        For Each record In lsData
            Dim product As Hashtable = record
            Dim sTableRow = "<tr>" & vbCrLf
            For Each key In lsKeys
                sTableRow &= "<td>" & CStr(product(key)) & "</td>" & vbCrLf
            Next
            sTableRow &= "</tr>" & vbCrLf
            Debug.Print("sTableRow: " & sTableRow)
            sTable &= sTableRow
            Exit For 'show only the latest record
        Next

        'Generate the last part of the html code for table
        sTable &= "</table>" & vbCrLf

        Return sTable
    End Function

    'generate report 2
    Private Function generateReport2(ByVal lsData As List(Of Hashtable), ByVal sReportTitle As String) As String
        Debug.Print("Generate Report ...")

        Dim sReportContent As String

        '1. Create the start of the HTML report file
        Dim sDoctype As String = "<!DOCTYPE html>"
        Dim sHtmlStartTag As String = "<html lang=""en"">"
        Dim sHeadTitle As String = "<head><title>" & sReportTitle & "</title></head>"
        Dim sBodyStartTag As String = "<body>"
        Dim sReportHeading As String = "<h1>" & sReportTitle & "</h1>"
        sReportContent = sDoctype & vbCrLf & sHtmlStartTag & vbCrLf & vbCrLf & sHeadTitle & vbCrLf & sBodyStartTag & vbCrLf & sReportHeading & vbCrLf

        '2. create the table and rows.
        Dim sTable = generateTable2(lsData)

        'create the rows of the table
        sReportContent &= sTable & vbCrLf

        '3. create the end of the HTML report file
        Dim sBodyEndTag As String = "</body>"
        Dim sHTMLEndTag As String = "</html>"
        sReportContent &= sBodyEndTag & vbCrLf & sHTMLEndTag

        Return sReportContent

    End Function

    ''''''''''''''''''''''''''''''''''''''''Report 3'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    'report = create report = {generate report [generate table (find data based on sql query)] + save report} 
    'Count the number of room that a customer book
    Public Function countroom(ByVal customerid As String, ByVal bmonth As String, ByVal byear As String) As List(Of Hashtable)
        Dim oConnection As OleDbConnection = New OleDbConnection(CONNECTION_STRING)
        Dim lsData As New List(Of Hashtable)

        Try
            oConnection.Open()
            Dim oCommand As OleDbCommand = New OleDbCommand
            oCommand.Connection = oConnection

            oCommand.CommandText = "select customer_id, count(booking.room_id) as number_of_room_booked from booking where customer_id = ? and month(bdate)= ? and year(bdate)= ? group by customer_id;"
            oCommand.Parameters.Add("customer_id", OleDbType.Char, 8)
            oCommand.Parameters.Add("month(bdate)", OleDbType.Char, 8)
            oCommand.Parameters.Add("year(bdate)", OleDbType.Char, 8)

            oCommand.Parameters("customer_id").Value = CStr(customerid)
            oCommand.Parameters("month(bdate)").Value = CStr(bmonth)
            oCommand.Parameters("year(bdate)").Value = CStr(byear)
            oCommand.Prepare()

            Dim oDataReader = oCommand.ExecuteReader()

            Dim htTempData As Hashtable
            Do While oDataReader.Read() = True
                htTempData = New Hashtable
                htTempData("number_of_room_booked") = CStr(oDataReader("number_of_room_booked"))
                htTempData("customer_id") = CStr(oDataReader("customer_id"))
                lsData.Add(htTempData)
            Loop
            Debug.Print("The records were found.")

        Catch ex As Exception
            Debug.Print("ERROR: " & ex.Message)
            MsgBox(" The records could not be found!")
        Finally
            oConnection.Close()
        End Try
        Return lsData
    End Function

    'create report 3
    Public Sub createReport3(ByVal customerid As String, ByVal bmonth As String, ByVal byear As String)

        Debug.Print("Create Report 3..")

        Dim sReportFilename = "BookingReport3.html"

        Dim sReportTitle = "The number of room booked by " & customerid & ":"

        'find the record
        Dim lsData = countroom(customerid, bmonth, byear)

        Dim sReportContent = generateReport3(lsData, sReportTitle)

        saveReport(sReportContent, sReportFilename)

        'Load the generated file in your default browser.        
        Dim sParam As String = """" & Application.StartupPath & "\" & sReportFilename & """"
        Debug.Print("sParam: " & sParam)
        System.Diagnostics.Process.Start(sParam)
    End Sub

    'generate table for report 3
    Private Function generateTable3(ByVal lsData As List(Of Hashtable)) As String
        'Create the start of the table
        Dim sTable = "<table border=""1"">" & vbCrLf
        If lsData.Count = 0 Then
            sTable &= "<tr><td colspan = """">" & " The customer had not yet booked a room </td></tr>" & vbCrLf
            sTable &= "</table>" & vbCrLf
            Return sTable
        End If

        Dim htSample As Hashtable = lsData.Item(0)
        Dim lsKeys As List(Of String) = New List(Of String)
        lsKeys.Add("number_of_room_booked")
        lsKeys.Add("customer_id")

        'Generate the header for all the row
        Dim sHeaderRow = "<tr>" & vbCrLf
        For Each key In lsKeys
            sHeaderRow &= "<th>" & CStr(key) & "</th>" & vbCrLf
        Next
        sHeaderRow &= "</tr>" & vbCrLf
        Debug.Print("sHeaderRow: " & sHeaderRow)
        sTable &= sHeaderRow

        'create the all rows in the table
        For Each record In lsData
            Dim product As Hashtable = record
            Dim sTableRow = "<tr>" & vbCrLf
            For Each key In lsKeys
                sTableRow &= "<td>" & CStr(product(key)) & "</td>" & vbCrLf
            Next
            sTableRow &= "</tr>" & vbCrLf
            Debug.Print("sTableRow: " & sTableRow)
            sTable &= sTableRow
            Exit For
        Next

        'Generate the last part of the html code for table
        sTable &= "</table>" & vbCrLf

        Return sTable
    End Function

    'generate report 3
    Private Function generateReport3(ByVal lsData As List(Of Hashtable), ByVal sReportTitle As String) As String
        Debug.Print("Generate Report ...")

        Dim sReportContent As String

        '1. Create the start of the HTML report file
        Dim sDoctype As String = "<!DOCTYPE html>"
        Dim sHtmlStartTag As String = "<html lang=""en"">"
        Dim sHeadTitle As String = "<head><title>" & sReportTitle & "</title></head>"
        Dim sBodyStartTag As String = "<body>"
        Dim sReportHeading As String = "<h1>" & sReportTitle & "</h1>"
        sReportContent = sDoctype & vbCrLf & sHtmlStartTag & vbCrLf & vbCrLf & sHeadTitle & vbCrLf & sBodyStartTag & vbCrLf & sReportHeading & vbCrLf

        '2. create the table and rows.
        Dim sTable = generateTable3(lsData)

        'create the rows of the table
        sReportContent &= sTable & vbCrLf

        '3. create the end of the HTML report file
        Dim sBodyEndTag As String = "</body>"
        Dim sHTMLEndTag As String = "</html>"
        sReportContent &= sBodyEndTag & vbCrLf & sHTMLEndTag

        Return sReportContent

    End Function
    '<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<4
    'report = create report = {generate report [generate table (find data based on sql query)] + save report} 
    'List all booking made in particular year
    Public Function bookingbydate(ByVal month As String, ByVal year As String) As List(Of Hashtable)

        Dim oConnection As OleDbConnection = New OleDbConnection(CONNECTION_STRING)
        Dim lsData As New List(Of Hashtable)

        Try
            Debug.Print("Connection string: " & oConnection.ConnectionString)

            oConnection.Open()
            Dim oCommand As OleDbCommand = New OleDbCommand
            oCommand.Connection = oConnection

            oCommand.CommandText = "Select * from booking where month(bdate)= ? and year(bdate)= ? "

            oCommand.Parameters.Add("month(bdate)", OleDbType.Char, 8)
            oCommand.Parameters.Add("year(bdate)", OleDbType.Char, 8)

            oCommand.Parameters("month(bdate)").Value = CStr(month)
            oCommand.Parameters("year(bdate)").Value = CStr(year)

            oCommand.Prepare()
            Dim oDataReader = oCommand.ExecuteReader()

            Dim htTempData As Hashtable
            Do While oDataReader.Read() = True
                htTempData = New Hashtable
                htTempData("booking_id") = CStr(oDataReader("booking_id"))
                htTempData("bdate") = CDate(oDataReader("bdate"))
                htTempData("num_days") = CInt(oDataReader("num_days"))
                htTempData("num_guests") = CInt(oDataReader("num_guests"))
                htTempData("checkin_date") = CDate(oDataReader("checkin_date"))
                htTempData("comments") = CStr(oDataReader("comments"))
                htTempData("customer_id") = CInt(oDataReader("customer_id"))
                htTempData("room_id") = CInt(oDataReader("room_id"))
                htTempData("total_price") = CDbl(oDataReader("total_price"))

                lsData.Add(htTempData)
            Loop

            Debug.Print("The record was found.")

        Catch ex As Exception
            Debug.Print("ERROR: " & ex.Message)
            MsgBox("An error occurred. The record(s) could not be found!")
        Finally
            oConnection.Close()
        End Try

        Return lsData

    End Function

    'create report 4
    Public Sub createReport4(ByVal month As String, ByVal year As String)

        Debug.Print("Create Report 4..")


        Dim sReportFilename = "BookingReport3.html"

        Dim sReportTitle = "All booking on: month " & month & " and year " & year & ""

        Dim lsData = bookingbydate(month, year)

        Dim sReportContent = generateReport4(lsData, sReportTitle)

        saveReport(sReportContent, sReportFilename)

        'Load the generated file in your default browser.        
        Dim sParam As String = """" & Application.StartupPath & "\" & sReportFilename & """"
        Debug.Print("sParam: " & sParam)
        System.Diagnostics.Process.Start(sParam)

    End Sub

    'generate table for report 4
    Private Function generateTable4(ByVal lsData As List(Of Hashtable)) As String
        'Generate the start of the table
        Dim sTable = "<table border=""1"">" & vbCrLf
        'check if there is data
        If lsData.Count = 0 Then
            sTable &= "<tr><td colspan = """">" & " No booking on this month and year </td></tr>" & vbCrLf
            sTable &= "</table>" & vbCrLf
            Return sTable
        End If

        Dim htSample As Hashtable = lsData.Item(0)
        Dim lsKeys As List(Of String) = New List(Of String)
        lsKeys.Add("booking_id")
        lsKeys.Add("bdate")
        lsKeys.Add("num_days")
        lsKeys.Add("num_guests")
        lsKeys.Add("checkin_date")
        lsKeys.Add("comments")
        lsKeys.Add("room_id")
        lsKeys.Add("customer_id")
        lsKeys.Add("total_price")

        'Generate the header for all the row
        Dim sHeaderRow = "<tr>" & vbCrLf
        For Each key In lsKeys
            sHeaderRow &= "<th>" & CStr(key) & "</th>" & vbCrLf
        Next
        sHeaderRow &= "</tr>" & vbCrLf
        Debug.Print("sHeaderRow: " & sHeaderRow)
        sTable &= sHeaderRow

        'create the all rows in the table
        For Each record In lsData
            Dim product As Hashtable = record
            Dim sTableRow = "<tr>" & vbCrLf
            For Each key In lsKeys
                sTableRow &= "<td>" & CStr(product(key)) & "</td>" & vbCrLf
            Next
            sTableRow &= "</tr>" & vbCrLf
            Debug.Print("sTableRow: " & sTableRow)
            sTable &= sTableRow
            Exit For
        Next

        'Generate the last part of the html code for table
        sTable &= "</table>" & vbCrLf

        Return sTable
    End Function

    'generate report 4
    Private Function generateReport4(ByVal lsData As List(Of Hashtable), ByVal sReportTitle As String) As String
        Debug.Print("Generate Report ...")

        Dim sReportContent As String

        'Create the first part of the HTML report file
        Dim sDoctype As String = "<!DOCTYPE html>"
        Dim sHtmlStartTag As String = "<html lang=""en"">"
        Dim sHeadTitle As String = "<head><title>" & sReportTitle & "</title></head>"
        Dim sBodyStartTag As String = "<body>"
        Dim sReportHeading As String = "<h1>" & sReportTitle & "</h1>"
        sReportContent = sDoctype & vbCrLf & sHtmlStartTag & vbCrLf & vbCrLf & sHeadTitle & vbCrLf & sBodyStartTag & vbCrLf & sReportHeading & vbCrLf

        'Create the room and booking table and there rows.
        Dim sTable = generateTable4(lsData)

        'create the table's content
        sReportContent &= sTable & vbCrLf

        'Create the end of the HTML file
        Dim sBodyEndTag As String = "</body>"
        Dim sHTMLEndTag As String = "</html>"
        sReportContent &= sBodyEndTag & vbCrLf & sHTMLEndTag

        Return sReportContent

    End Function

    ''''''''''''''''''''''''''''''''''''''''''5
    'report = create report = {generate report [generate table (find data based on sql query)] + save report} 
    'list all customer data + checkin_date from booking
    Public Function checkindateforcustomer(ByVal month As String, ByVal year As String) As List(Of Hashtable)

        Dim oConnection As OleDbConnection = New OleDbConnection(CONNECTION_STRING)
        Dim lsData As New List(Of Hashtable)

        Try
            Debug.Print("Connection string: " & oConnection.ConnectionString)

            oConnection.Open()
            Dim oCommand As OleDbCommand = New OleDbCommand
            oCommand.Connection = oConnection

            oCommand.CommandText = "SELECT customer.*, booking.checkin_date from customer  right join booking on customer.customer_id=booking.customer_id where month(booking.checkin_date) = ? and year(booking.checkin_date) = ?"

            oCommand.Parameters.Add("month(bdate)", OleDbType.Char, 8)
            oCommand.Parameters.Add("year(bdate)", OleDbType.Char, 8)

            oCommand.Parameters("month(bdate)").Value = CStr(month)
            oCommand.Parameters("year(bdate)").Value = CStr(year)

            oCommand.Prepare()
            Dim oDataReader = oCommand.ExecuteReader()

            Dim htTempData As Hashtable
            Do While oDataReader.Read() = True
                htTempData = New Hashtable
                htTempData("customer_id") = CStr(oDataReader("customer_id"))
                htTempData("title") = CStr(oDataReader("title"))
                htTempData("firstname") = CStr(oDataReader("firstname"))
                htTempData("lastname") = CStr(oDataReader("lastname"))
                htTempData("phone") = CStr(oDataReader("phone"))
                htTempData("address") = CStr(oDataReader("address"))
                htTempData("email") = CStr(oDataReader("email"))
                htTempData("dob") = CDate(oDataReader("dob"))
                htTempData("gender") = CStr(oDataReader("gender"))

                lsData.Add(htTempData)
            Loop

            Debug.Print("The record was found.")

        Catch ex As Exception
            Debug.Print("ERROR: " & ex.Message)
            MsgBox("An error occurred. The record(s) could not be found!")
        Finally
            oConnection.Close()
        End Try

        Return lsData

    End Function

    'create report 5
    Public Sub createReport5(ByVal month As String, ByVal year As String)

        Debug.Print("Create Report 5..")

        Dim sReportFilename = "BookingReport3.html"

        Dim sReportTitle = "customer checkin date: month " & month & " and year " & year & " "

        'find all the records 
        Dim lsData = checkindateforcustomer(month, year)

        Dim sReportContent = generateReport5(lsData, sReportTitle)

        saveReport(sReportContent, sReportFilename)

        'Load the generated file in your default browser. 
        Dim sParam As String = """" & Application.StartupPath & "\" & sReportFilename & """"
        Debug.Print("sParam: " & sParam)
        System.Diagnostics.Process.Start(sParam)
    End Sub

    'generate table for report 5
    Private Function generateTable5(ByVal lsData As List(Of Hashtable)) As String
        'Generate table style
        Dim sTable = "<table border=""1"">" & vbCrLf
        'check if there is data
        If lsData.Count = 0 Then
            sTable &= "<tr><td colspan = """">" & " No customer checkin this time </td></tr>" & vbCrLf
            sTable &= "</table>" & vbCrLf
            Return sTable
        End If

        Dim htSample As Hashtable = lsData.Item(0)
        Dim lsKeys As List(Of String) = New List(Of String)
        lsKeys.Add("customer_id")
        lsKeys.Add("title")
        lsKeys.Add("firstname")
        lsKeys.Add("lastname")
        lsKeys.Add("phone")
        lsKeys.Add("address")
        lsKeys.Add("email")
        lsKeys.Add("dob")
        lsKeys.Add("gender")

        'Generate the header for all the row
        Dim sHeaderRow = "<tr>" & vbCrLf
        For Each key In lsKeys
            sHeaderRow &= "<th>" & CStr(key) & "</th>" & vbCrLf
        Next
        sHeaderRow &= "</tr>" & vbCrLf
        Debug.Print("sHeaderRow: " & sHeaderRow)
        sTable &= sHeaderRow

        'create the all rows in the table
        For Each record In lsData
            Dim product As Hashtable = record
            Dim sTableRow = "<tr>" & vbCrLf
            For Each key In lsKeys
                sTableRow &= "<td>" & CStr(product(key)) & "</td>" & vbCrLf
            Next
            sTableRow &= "</tr>" & vbCrLf
            Debug.Print("sTableRow: " & sTableRow)
            sTable &= sTableRow
            'Exit For
        Next

        'Generate the last part of the html code for table
        sTable &= "</table>" & vbCrLf

        Return sTable
    End Function

    'generate report 5
    Private Function generateReport5(ByVal lsData As List(Of Hashtable), ByVal sReportTitle As String) As String
        Debug.Print("Generate Report ...")

        Dim sReportContent As String

        'Create the first part of the HTML report file
        Dim sDoctype As String = "<!DOCTYPE html>"
        Dim sHtmlStartTag As String = "<html lang=""en"">"
        Dim sHeadTitle As String = "<head><title>" & sReportTitle & "</title></head>"
        Dim sBodyStartTag As String = "<body>"
        Dim sReportHeading As String = "<h1>" & sReportTitle & "</h1>"
        sReportContent = sDoctype & vbCrLf & sHtmlStartTag & vbCrLf & vbCrLf & sHeadTitle & vbCrLf & sBodyStartTag & vbCrLf & sReportHeading & vbCrLf

        'Create the room and booking table and there rows.
        Dim sTable = generateTable5(lsData)

        'create the table's content
        sReportContent &= sTable & vbCrLf

        'Create the end of the HTML file
        Dim sBodyEndTag As String = "</body>"
        Dim sHTMLEndTag As String = "</html>"
        sReportContent &= sBodyEndTag & vbCrLf & sHTMLEndTag

        Return sReportContent

    End Function

    ''''''''''''''''''''''''''''''''''''6
    'report = create report = {generate report [generate table (find data based on sql query)] + save report} 
    'show all booking for a particular room in a particular time
    Public Function roomavailable(ByVal month As String, ByVal year As String, ByVal roomnumber As String) As List(Of Hashtable)

        Dim oConnection As OleDbConnection = New OleDbConnection(CONNECTION_STRING)
        Dim lsData As New List(Of Hashtable)

        Try
            Debug.Print("Connection string: " & oConnection.ConnectionString)

            oConnection.Open()
            Dim oCommand As OleDbCommand = New OleDbCommand
            oCommand.Connection = oConnection

            oCommand.CommandText = "SELECT booking.*, room.room_number from booking right join room on booking.room_id=room.room_id where month(booking.bdate) = ? and year(booking.bdate) = ? and room.room_number = ? "

            oCommand.Parameters.Add("month(bdate)", OleDbType.Char, 8)
            oCommand.Parameters.Add("year(bdate)", OleDbType.Char, 8)
            oCommand.Parameters.Add("room_number", OleDbType.Char, 8)

            oCommand.Parameters("month(bdate)").Value = CStr(month)
            oCommand.Parameters("year(bdate)").Value = CStr(year)
            oCommand.Parameters("room_number").Value = CStr(roomnumber)
            oCommand.Prepare()
            Dim oDataReader = oCommand.ExecuteReader()

            Dim htTempData As Hashtable
            Do While oDataReader.Read() = True
                htTempData = New Hashtable
                htTempData("booking_id") = CStr(oDataReader("booking_id"))
                htTempData("bdate") = CDate(oDataReader("bdate"))
                htTempData("num_days") = CInt(oDataReader("num_days"))
                htTempData("num_guests") = CInt(oDataReader("num_guests"))
                htTempData("checkin_date") = CDate(oDataReader("checkin_date"))
                htTempData("comments") = CStr(oDataReader("comments"))
                htTempData("customer_id") = CInt(oDataReader("customer_id"))
                htTempData("room_id") = CInt(oDataReader("room_id"))
                htTempData("total_price") = CDbl(oDataReader("total_price"))
                htTempData("room_number") = CStr(oDataReader("room_number"))
                lsData.Add(htTempData)
            Loop

            Debug.Print("The record was found.")

        Catch ex As Exception
            Debug.Print("ERROR: " & ex.Message)
            MsgBox("An error occurred. The record(s) could not be found!")
        Finally
            oConnection.Close()
        End Try

        Return lsData

    End Function

    'create report 6
    Public Sub createReport6(ByVal bmonth As String, ByVal byear As String, ByVal room_number As String)

        Debug.Print("Create Report 6..")

        Dim sReportFilename = "BookingReport6.html"

        Dim sReportTitle = " all the bookings for a particular room in a given month of a given year"

        Dim lsData = roomavailable(bmonth, byear, room_number)

        Dim sReportContent = generateReport6(lsData, sReportTitle)

        saveReport(sReportContent, sReportFilename)

        'Load the generated file in your default browser.         
        Dim sParam As String = """" & Application.StartupPath & "\" & sReportFilename & """"
        Debug.Print("sParam: " & sParam)
        System.Diagnostics.Process.Start(sParam)
    End Sub

    'generate table for report 6
    Private Function generateTable6(ByVal lsData As List(Of Hashtable)) As String
        'Generate table style
        Dim sTable = "<table border=""1"">" & vbCrLf
        'check if there is data
        If lsData.Count = 0 Then
            sTable &= "<tr><td colspan = """">" & " this room have not booked </td></tr>" & vbCrLf
            sTable &= "</table>" & vbCrLf
            Return sTable
        End If

        Dim htSample As Hashtable = lsData.Item(0)
        Dim lsKeys As List(Of String) = New List(Of String)
        lsKeys.Add("booking_id")
        lsKeys.Add("bdate")
        lsKeys.Add("num_days")
        lsKeys.Add("num_guests")
        lsKeys.Add("checkin_date")
        lsKeys.Add("comments")
        lsKeys.Add("customer_id")
        lsKeys.Add("room_id")
        lsKeys.Add("total_price")
        lsKeys.Add("room_number")
        'Generate the header for all the row
        Dim sHeaderRow = "<tr>" & vbCrLf
        For Each key In lsKeys
            sHeaderRow &= "<th>" & CStr(key) & "</th>" & vbCrLf
        Next
        sHeaderRow &= "</tr>" & vbCrLf
        Debug.Print("sHeaderRow: " & sHeaderRow)
        sTable &= sHeaderRow

        'create the all rows in the table
        For Each record In lsData
            Dim product As Hashtable = record
            Dim sTableRow = "<tr>" & vbCrLf
            For Each key In lsKeys
                sTableRow &= "<td>" & CStr(product(key)) & "</td>" & vbCrLf
            Next
            sTableRow &= "</tr>" & vbCrLf
            Debug.Print("sTableRow: " & sTableRow)
            sTable &= sTableRow
        Next

        'Generate the last part of the html code for table
        sTable &= "</table>" & vbCrLf

        Return sTable
    End Function

    'generate report 6
    Private Function generateReport6(ByVal lsData As List(Of Hashtable), ByVal sReportTitle As String) As String
        Debug.Print("Generate Report ...")

        Dim sReportContent As String

        'Create the first part of the HTML report file
        Dim sDoctype As String = "<!DOCTYPE html>"
        Dim sHtmlStartTag As String = "<html lang=""en"">"
        Dim sHeadTitle As String = "<head><title>" & sReportTitle & "</title></head>"
        Dim sBodyStartTag As String = "<body>"
        Dim sReportHeading As String = "<h1>" & sReportTitle & "</h1>"
        sReportContent = sDoctype & vbCrLf & sHtmlStartTag & vbCrLf & vbCrLf & sHeadTitle & vbCrLf & sBodyStartTag & vbCrLf & sReportHeading & vbCrLf

        'Create the room and booking table and there rows.
        Dim sTable = generateTable6(lsData)

        'create the table's content
        sReportContent &= sTable & vbCrLf

        'Create the end of the HTML file
        Dim sBodyEndTag As String = "</body>"
        Dim sHTMLEndTag As String = "</html>"
        sReportContent &= sBodyEndTag & vbCrLf & sHTMLEndTag

        Return sReportContent

    End Function

    '<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<report 7
    'report = create report = {generate report [generate table (generate control break table (find data based on sql query)) + generate] + save report} 
    'All bookings made for a given year and month broken down according to room number
    Public Function bookingroom(ByVal month As String, ByVal year As String) As List(Of Hashtable)

        Dim oConnection As OleDbConnection = New OleDbConnection(CONNECTION_STRING)
        Dim lsData As New List(Of Hashtable)

        Try
            Debug.Print("Connection string: " & oConnection.ConnectionString)

            oConnection.Open()
            Dim oCommand As OleDbCommand = New OleDbCommand
            oCommand.Connection = oConnection

            oCommand.CommandText = "Select booking.booking_id, room.room_id, room.room_number, room.type, room.price, room.num_bed, room.floor, room.description from room inner join booking on room.room_id=booking.room_id where month(booking.bdate)=? and year (booking.bdate)=? order by room.room_id"

            oCommand.Parameters.Add("month(bdate)", OleDbType.Char, 8)
            oCommand.Parameters.Add("year(bdate)", OleDbType.Char, 8)


            oCommand.Parameters("month(bdate)").Value = CStr(month)
            oCommand.Parameters("year(bdate)").Value = CStr(year)

            oCommand.Prepare()
            Dim oDataReader = oCommand.ExecuteReader()

            Dim htTempData As Hashtable
            Do While oDataReader.Read() = True
                htTempData = New Hashtable
                htTempData("booking_id") = CStr(oDataReader("booking_id"))
                htTempData("room_id") = CStr(oDataReader("room_id"))
                htTempData("room_number") = CStr(oDataReader("room_number"))
                htTempData("type") = CStr(oDataReader("type"))
                htTempData("price") = CStr(oDataReader("price"))
                htTempData("num_bed") = CStr(oDataReader("num_bed"))
                htTempData("floor") = CStr(oDataReader("floor"))
                htTempData("description") = CStr(oDataReader("description"))
                lsData.Add(htTempData)
            Loop

            Debug.Print("The record was found.")

        Catch ex As Exception
            Debug.Print("ERROR: " & ex.Message)
            MsgBox("An error occurred. The record(s) could not be found!")
        Finally
            oConnection.Close()
        End Try

        Return lsData

    End Function

    'create control break report 7
    Public Sub createBreakReport7(ByVal month As String, year As String)

        Debug.Print("CreateBreakReport ...")

        'create title for the report
        Dim lsData = bookingroom(month, year)
        If CInt(lsData.Count.ToString) > 0 Then
            Dim sReportTitle = "Control Break Report 7"
            Dim sReportContent = generateBreakReport7(lsData, sReportTitle)
            Dim sReportFilename = "ProductBreakReport.html"

            'call save report function. 
            saveReport(sReportContent, sReportFilename)

            'Load the generated file in your default browser. 
            Dim sParam As String = """" & Application.StartupPath & "\" & sReportFilename & """"
            Debug.Print("sParam: " & sParam)
            System.Diagnostics.Process.Start(sParam)
        Else
            MsgBox("No booking for this room during this time")

        End If
    End Sub

    'generate report 7
    Private Function generateBreakReport7(ByVal lsData As List(Of Hashtable), ByVal sReportTitle As String) As String
        Debug.Print("GenerateBreakReport ...")

        Dim sReportContent As String

        'Create the first part of the HTML report file
        Dim sDoctype As String = "<!DOCTYPE html>"
        Dim sHtmlStartTag As String = "<html lang=""en"">"
        Dim sHeadTitle As String = "<head><title>" & sReportTitle & "</title></head>"
        Dim sBodyStartTag As String = "<body>"
        Dim sReportHeading As String = "<h1>" & sReportTitle & "</h1>"
        sReportContent = sDoctype & vbCrLf & sHtmlStartTag & vbCrLf & vbCrLf & sHeadTitle & vbCrLf & sBodyStartTag & vbCrLf & sReportHeading & vbCrLf

        'Create the room and booking table and there rows.
        Dim sTable = generateControlBreakTable7(lsData)
        'create the table's content
        sReportContent &= sTable & vbCrLf

        'Create the end of the HTML file
        Dim sBodyEndTag As String = "</body>"
        Dim sHTMLEndTag As String = "</html>"
        sReportContent &= sBodyEndTag & vbCrLf & sHTMLEndTag

        Return sReportContent

    End Function

    'generate control break table 7
    Private Function generateControlBreakTable7(ByVal lsData As List(Of Hashtable)) As String
        'Generate the start of the table
        Dim sTable = "<table border=""1"">" & vbCrLf

        Dim htSample As Hashtable = lsData.Item(0)
        Dim lsKeys As List(Of String) = New List(Of String)
        lsKeys.Add("booking_id")
        lsKeys.Add("room_id")
        lsKeys.Add("room_number")
        lsKeys.Add("type")
        lsKeys.Add("price")
        lsKeys.Add("num_beds")
        lsKeys.Add("floor")
        lsKeys.Add("description")

        'create the header row
        Dim sHeaderRow = "<tr>" & vbCrLf
        For Each key In lsKeys
            sHeaderRow &= "<th>" & CStr(key) & "</th>" & vbCrLf
        Next
        sHeaderRow &= "</tr>" & vbCrLf
        Debug.Print("sHeaderRow: " & sHeaderRow)
        sTable &= sHeaderRow

        'create the table rows
        sTable &= generateTableRows7(lsData, lsKeys)

        'create the last part of the table
        sTable &= "</table>" & vbCrLf

        Return sTable
    End Function

    'generate table row 7
    Private Function generateTableRows7(ByVal lsData As List(Of Hashtable), ByVal lsKeys As List(Of String)) As String

        '1.Initialisation 
        Dim sRows As String = ""
        Dim sTableRow As String
        Dim iCountRecordsPerDate As Integer = 0
        Dim bAvailability As Boolean = True
        Dim sCurrentControlField As String = ""
        Dim sPreviousControlField As String = ""

        '2.Loop through the list of hashtables
        For Each record In lsData

            '2.a Get a room and set the current key
            Dim room As Hashtable = record
            sCurrentControlField = CStr(room("availability"))

            '2.b Do not check for control break on the first iteration of the loop
            If bAvailability Then
                bAvailability = False
            Else
                If sCurrentControlField <> sPreviousControlField Then
                    sTableRow = "<tr><td colspan = """ & lsKeys.Count & """>" & " Total booked rooms in " & sPreviousControlField & " Date: " & iCountRecordsPerDate & "</td></tr>" & vbCrLf
                    sRows &= sTableRow
                    iCountRecordsPerDate = 0
                End If
            End If

            '2.c Output a normal row for every pass through the list
            sTableRow = "<tr>" & vbCrLf
            For Each key In lsKeys
                sTableRow &= "<td>" & CStr(room(key)) & "</td>" & vbCrLf
            Next
            sTableRow &= "</tr>" & vbCrLf
            Debug.Print("sTableRow: " & sTableRow)
            sRows &= sTableRow

            '2.d Update control field and increment total
            sPreviousControlField = sCurrentControlField
            iCountRecordsPerDate += 1
        Next

        '3. After the loop, need to output the last total row
        sTableRow = "<tr><td colspan = """ & lsKeys.Count & """>" & " Total booked rooms in " & sPreviousControlField & " Date: " & iCountRecordsPerDate & "</td></tr>" & vbCrLf
        sRows &= sTableRow

        Return sRows

    End Function

    '<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<report 8
    'show all invoice
    Public Function invoice() As List(Of Hashtable)

        Dim oConnection As OleDbConnection = New OleDbConnection(CONNECTION_STRING)
        Dim lsData As New List(Of Hashtable)

        Try
            Debug.Print("Connection string: " & oConnection.ConnectionString)

            oConnection.Open()
            Dim oCommand As OleDbCommand = New OleDbCommand
            oCommand.Connection = oConnection

            oCommand.CommandText = "select booking_id, date_invoice, amount from invoice where year(date_invoice)=year(now) order by month(date_invoice);"

            oCommand.Prepare()
            Dim oDataReader = oCommand.ExecuteReader()

            Dim htTempData As Hashtable
            Do While oDataReader.Read() = True
                htTempData = New Hashtable
                htTempData("booking_id") = CStr(oDataReader("booking_id"))
                htTempData("date_invoice") = CDate(oDataReader("date_invoice"))
                htTempData("amount") = CStr(oDataReader("amount"))

                lsData.Add(htTempData)
            Loop

            Debug.Print("The record was found.")

        Catch ex As Exception
            Debug.Print("ERROR: " & ex.Message)
            MsgBox("An error occurred. The record(s) could not be found!")
        Finally
            oConnection.Close()
        End Try

        Return lsData

        Return lsData

    End Function

    'create control break report 8
    Public Sub createBreakReport8()

        Debug.Print("CreateBreakReport ...")

        'create title for the report
        Dim lsData = invoice()
        If CInt(lsData.Count.ToString) > 0 Then
            Dim sReportTitle = "Control Break Report 8"
            Dim sReportContent = generateBreakReport8(lsData, sReportTitle)
            Dim sReportFilename = "ProductBreakReport.html"

            'call save report function. 
            saveReport(sReportContent, sReportFilename)

            'Load the generated file in your default browser. 
            Dim sParam As String = """" & Application.StartupPath & "\" & sReportFilename & """"
            Debug.Print("sParam: " & sParam)
            System.Diagnostics.Process.Start(sParam)
        Else
            MsgBox("No booking for this room during this time")

        End If
    End Sub

    'generate report 8
    Private Function generateBreakReport8(ByVal lsData As List(Of Hashtable), ByVal sReportTitle As String) As String
        Debug.Print("GenerateBreakReport ...")

        Dim sReportContent As String

        'Create the first part of the HTML report file
        Dim sDoctype As String = "<!DOCTYPE html>"
        Dim sHtmlStartTag As String = "<html lang=""en"">"
        Dim sHeadTitle As String = "<head><title>" & sReportTitle & "</title></head>"
        Dim sBodyStartTag As String = "<body>"
        Dim sReportHeading As String = "<h1>" & sReportTitle & "</h1>"
        sReportContent = sDoctype & vbCrLf & sHtmlStartTag & vbCrLf & vbCrLf & sHeadTitle & vbCrLf & sBodyStartTag & vbCrLf & sReportHeading & vbCrLf

        'Create the room and booking table and there rows.
        Dim sTable = generateControlBreakTable8(lsData)
        'construct the rows of the table
        sReportContent &= sTable & vbCrLf

        'Create the end of the HTML file
        Dim sBodyEndTag As String = "</body>"
        Dim sHTMLEndTag As String = "</html>"
        sReportContent &= sBodyEndTag & vbCrLf & sHTMLEndTag

        Return sReportContent

    End Function

    'generate control break table 8
    Private Function generateControlBreakTable8(ByVal lsData As List(Of Hashtable)) As String
        'Generate the start of the table
        Dim sTable = "<table border=""1"">" & vbCrLf

        Dim htSample As Hashtable = lsData.Item(0)
        Dim lsKeys As List(Of String) = New List(Of String)
        lsKeys.Add("booking_id")
        lsKeys.Add("date_invoice")
        lsKeys.Add("amount")

        'create the header row
        Dim sHeaderRow = "<tr>" & vbCrLf
        For Each key In lsKeys
            sHeaderRow &= "<th>" & CStr(key) & "</th>" & vbCrLf
        Next
        sHeaderRow &= "</tr>" & vbCrLf
        Debug.Print("sHeaderRow: " & sHeaderRow)
        sTable &= sHeaderRow

        'create the table rows
        sTable &= generateTableRows8(lsData, lsKeys)

        'create the last part of the table
        sTable &= "</table>" & vbCrLf

        Return sTable
    End Function

    'generate table row 8
    Private Function generateTableRows8(ByVal lsData As List(Of Hashtable), ByVal lsKeys As List(Of String)) As String

        '1.Initialisation 
        Dim sRows As String = ""
        Dim sTableRow As String
        Dim iCountRecordsPerRoom As Integer = 0
        Dim bFirstTime As Boolean = True
        Dim sCurrentControlField As String = ""
        Dim sPreviousControlField As String = ""

        '2.Loop through the list of hashtables
        For Each record In lsData

            '2.a Get a room and set the current key
            Dim invoice As Hashtable = record
            sCurrentControlField = CStr(Month(CDate(invoice("date_invoice"))))

            '2.b Do not check for control break on the first iteration of the loop
            If bFirstTime Then
                bFirstTime = False
            Else
                If sCurrentControlField <> sPreviousControlField Then
                    sTableRow = "<tr><td colspan = """ & lsKeys.Count & """>" & " Total booked rooms in " & sPreviousControlField & " Date: " & iCountRecordsPerRoom & "</td></tr>" & vbCrLf
                    sRows &= sTableRow
                    iCountRecordsPerRoom = 0
                End If
            End If

            '2.c Output a normal row for every pass through the list
            sTableRow = "<tr>" & vbCrLf
            For Each key In lsKeys
                sTableRow &= "<td>" & CStr(invoice(key)) & "</td>" & vbCrLf
            Next
            sTableRow &= "</tr>" & vbCrLf
            Debug.Print("sTableRow: " & sTableRow)
            sRows &= sTableRow

            '2.d Update control field and increment total
            sPreviousControlField = sCurrentControlField
            iCountRecordsPerRoom += 1
        Next

        '3. After the loop, need to output the last total row
        sTableRow = "<tr><td colspan = """ & lsKeys.Count & """>" & " Total invoice in " & sCurrentControlField & " : " & iCountRecordsPerRoom & "</td></tr>" & vbCrLf
        sRows &= sTableRow

        Return sRows

    End Function


    '<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<essential function and sub for all report>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>..

    'Save report on debug folder
    Private Sub saveReport(ByVal sReportContent As String, ByVal sReportFilename As String)
        Debug.Print("saveReport: " & sReportFilename)
        Dim oReportFile As StreamWriter = New StreamWriter(Application.StartupPath & "\" & sReportFilename)
        If Not (oReportFile Is Nothing) Then
            oReportFile.Write(sReportContent)
            oReportFile.Close()
        End If
    End Sub

End Class
