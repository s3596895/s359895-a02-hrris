﻿Option Explicit On
Option Strict On

Imports System.Drawing
Imports System.IO
'Author: Dao Ngoc Minh 
'studen id: s3596895
'This code is written by Dao Ngoc Minh s3596895
Public Class Report2
    Dim ocontroller As DataController = New DataController
    'load combobox
    Private Sub Report2_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        cbbRoomNum.DropDownStyle = ComboBoxStyle.DropDownList
        Dim lsdata = ocontroller.findAll()

        For Each roomnumber In lsdata
            cbbRoomNum.Items.Add(CStr(roomnumber("room_number")))
        Next
    End Sub

    'generate report
    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click

        Dim tt As New ToolTip()
        If cbbRoomNum.SelectedIndex = -1 Then
            p1.Visible = True
            tt.SetToolTip(p1, "You must select a room number")
            Exit Sub
        Else p1.Visible = False
        End If
        ocontroller.createReport2(cbbRoomNum.SelectedItem.ToString)
    End Sub

End Class