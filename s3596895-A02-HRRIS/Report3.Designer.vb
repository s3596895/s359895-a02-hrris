﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Report3
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Report3))
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.p3 = New System.Windows.Forms.PictureBox()
        Me.p2 = New System.Windows.Forms.PictureBox()
        Me.p1 = New System.Windows.Forms.PictureBox()
        Me.txtBookingYear = New System.Windows.Forms.TextBox()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.cbbBookingMonth = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cbbCusId = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupBox3.SuspendLayout()
        CType(Me.p3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.p2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.p1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox3
        '
        Me.GroupBox3.BackColor = System.Drawing.Color.White
        Me.GroupBox3.Controls.Add(Me.p3)
        Me.GroupBox3.Controls.Add(Me.p2)
        Me.GroupBox3.Controls.Add(Me.p1)
        Me.GroupBox3.Controls.Add(Me.txtBookingYear)
        Me.GroupBox3.Controls.Add(Me.Button3)
        Me.GroupBox3.Controls.Add(Me.cbbBookingMonth)
        Me.GroupBox3.Controls.Add(Me.Label3)
        Me.GroupBox3.Controls.Add(Me.Label2)
        Me.GroupBox3.Controls.Add(Me.cbbCusId)
        Me.GroupBox3.Controls.Add(Me.Label1)
        Me.GroupBox3.Location = New System.Drawing.Point(42, 61)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(314, 192)
        Me.GroupBox3.TabIndex = 2
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "The number of room was booked in a particular time by a given customer"
        '
        'p3
        '
        Me.p3.Image = CType(resources.GetObject("p3.Image"), System.Drawing.Image)
        Me.p3.Location = New System.Drawing.Point(288, 113)
        Me.p3.Name = "p3"
        Me.p3.Size = New System.Drawing.Size(20, 20)
        Me.p3.TabIndex = 17
        Me.p3.TabStop = False
        '
        'p2
        '
        Me.p2.Image = CType(resources.GetObject("p2.Image"), System.Drawing.Image)
        Me.p2.Location = New System.Drawing.Point(288, 71)
        Me.p2.Name = "p2"
        Me.p2.Size = New System.Drawing.Size(20, 20)
        Me.p2.TabIndex = 16
        Me.p2.TabStop = False
        '
        'p1
        '
        Me.p1.Image = CType(resources.GetObject("p1.Image"), System.Drawing.Image)
        Me.p1.Location = New System.Drawing.Point(288, 34)
        Me.p1.Name = "p1"
        Me.p1.Size = New System.Drawing.Size(20, 20)
        Me.p1.TabIndex = 15
        Me.p1.TabStop = False
        '
        'txtBookingYear
        '
        Me.txtBookingYear.Location = New System.Drawing.Point(116, 71)
        Me.txtBookingYear.MaxLength = 4
        Me.txtBookingYear.Name = "txtBookingYear"
        Me.txtBookingYear.Size = New System.Drawing.Size(150, 20)
        Me.txtBookingYear.TabIndex = 9
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(116, 154)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(104, 23)
        Me.Button3.TabIndex = 5
        Me.Button3.Text = "Generate"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'cbbBookingMonth
        '
        Me.cbbBookingMonth.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbbBookingMonth.FormattingEnabled = True
        Me.cbbBookingMonth.Items.AddRange(New Object() {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"})
        Me.cbbBookingMonth.Location = New System.Drawing.Point(116, 112)
        Me.cbbBookingMonth.Name = "cbbBookingMonth"
        Me.cbbBookingMonth.Size = New System.Drawing.Size(150, 21)
        Me.cbbBookingMonth.TabIndex = 8
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(35, 112)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(37, 13)
        Me.Label3.TabIndex = 7
        Me.Label3.Text = "Month"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(35, 78)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(29, 13)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "Year"
        '
        'cbbCusId
        '
        Me.cbbCusId.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbbCusId.FormattingEnabled = True
        Me.cbbCusId.Location = New System.Drawing.Point(116, 34)
        Me.cbbCusId.Name = "cbbCusId"
        Me.cbbCusId.Size = New System.Drawing.Size(150, 21)
        Me.cbbCusId.TabIndex = 4
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(35, 41)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(65, 13)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Customer ID"
        '
        'Report3
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(384, 361)
        Me.Controls.Add(Me.GroupBox3)
        Me.Name = "Report3"
        Me.Text = "Report3"
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        CType(Me.p3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.p2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.p1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents txtBookingYear As TextBox
    Friend WithEvents Button3 As Button
    Friend WithEvents cbbBookingMonth As ComboBox
    Friend WithEvents Label3 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents cbbCusId As ComboBox
    Friend WithEvents Label1 As Label
    Friend WithEvents p1 As PictureBox
    Friend WithEvents p3 As PictureBox
    Friend WithEvents p2 As PictureBox
End Class
