﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Invoice
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Invoice))
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.btnGenerate = New System.Windows.Forms.Button()
        Me.txtAmount = New System.Windows.Forms.TextBox()
        Me.DateInvoice = New System.Windows.Forms.DateTimePicker()
        Me.cbbBookingId = New System.Windows.Forms.ComboBox()
        Me.lblcusFirstName = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.p1 = New System.Windows.Forms.PictureBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        CType(Me.p1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnCancel
        '
        Me.btnCancel.Location = New System.Drawing.Point(218, 203)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 23)
        Me.btnCancel.TabIndex = 80
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'btnGenerate
        '
        Me.btnGenerate.Location = New System.Drawing.Point(75, 203)
        Me.btnGenerate.Name = "btnGenerate"
        Me.btnGenerate.Size = New System.Drawing.Size(75, 23)
        Me.btnGenerate.TabIndex = 79
        Me.btnGenerate.Text = "Generate"
        Me.btnGenerate.UseVisualStyleBackColor = True
        '
        'txtAmount
        '
        Me.txtAmount.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtAmount.Location = New System.Drawing.Point(117, 130)
        Me.txtAmount.Name = "txtAmount"
        Me.txtAmount.ReadOnly = True
        Me.txtAmount.Size = New System.Drawing.Size(200, 20)
        Me.txtAmount.TabIndex = 78
        '
        'DateInvoice
        '
        Me.DateInvoice.Enabled = False
        Me.DateInvoice.Location = New System.Drawing.Point(117, 70)
        Me.DateInvoice.Name = "DateInvoice"
        Me.DateInvoice.Size = New System.Drawing.Size(200, 20)
        Me.DateInvoice.TabIndex = 77
        '
        'cbbBookingId
        '
        Me.cbbBookingId.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbbBookingId.FormattingEnabled = True
        Me.cbbBookingId.Location = New System.Drawing.Point(117, 23)
        Me.cbbBookingId.Name = "cbbBookingId"
        Me.cbbBookingId.Size = New System.Drawing.Size(200, 21)
        Me.cbbBookingId.TabIndex = 74
        '
        'lblcusFirstName
        '
        Me.lblcusFirstName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblcusFirstName.Location = New System.Drawing.Point(25, 22)
        Me.lblcusFirstName.Name = "lblcusFirstName"
        Me.lblcusFirstName.Size = New System.Drawing.Size(68, 20)
        Me.lblcusFirstName.TabIndex = 81
        Me.lblcusFirstName.Text = "Booking ID"
        Me.lblcusFirstName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label1
        '
        Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label1.Location = New System.Drawing.Point(25, 70)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(68, 20)
        Me.Label1.TabIndex = 82
        Me.Label1.Text = "Date"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label2
        '
        Me.Label2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label2.Location = New System.Drawing.Point(25, 130)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(68, 20)
        Me.Label2.TabIndex = 83
        Me.Label2.Text = "Amount"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'p1
        '
        Me.p1.Image = CType(resources.GetObject("p1.Image"), System.Drawing.Image)
        Me.p1.Location = New System.Drawing.Point(382, 24)
        Me.p1.Name = "p1"
        Me.p1.Size = New System.Drawing.Size(20, 20)
        Me.p1.TabIndex = 84
        Me.p1.TabStop = False
        Me.p1.Visible = False
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(382, 130)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(20, 20)
        Me.PictureBox1.TabIndex = 85
        Me.PictureBox1.TabStop = False
        Me.PictureBox1.Visible = False
        '
        'Invoice
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(414, 356)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.p1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.lblcusFirstName)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnGenerate)
        Me.Controls.Add(Me.txtAmount)
        Me.Controls.Add(Me.DateInvoice)
        Me.Controls.Add(Me.cbbBookingId)
        Me.Name = "Invoice"
        Me.Text = "Invoice"
        CType(Me.p1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnCancel As Button
    Friend WithEvents btnGenerate As Button
    Friend WithEvents txtAmount As TextBox
    Friend WithEvents DateInvoice As DateTimePicker
    Friend WithEvents cbbBookingId As ComboBox
    Friend WithEvents lblcusFirstName As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents p1 As PictureBox
    Friend WithEvents PictureBox1 As PictureBox
End Class
