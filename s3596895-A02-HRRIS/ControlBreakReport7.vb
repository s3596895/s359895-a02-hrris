﻿Option Explicit On
Option Strict On

Imports System.Drawing
Imports System.IO

'Author: Dao Ngoc Minh 
'studen id: s3596895
'This code is written by Dao Ngoc Minh s3596895
Public Class ControlBreakReport7
    Dim ocontroller As DataController = New DataController
    Private Sub Button7_Click(sender As Object, e As EventArgs) Handles Button7.Click
        Dim ovalidation As New Validation
        Dim bAllFieldsValid As Boolean
        bAllFieldsValid = True
        Dim bIsValid As Boolean
        Dim tt As New ToolTip()

        'check year
        bIsValid = ovalidation.validateyear(txtYear.Text)
        If bIsValid Then
            p2.Visible = False
        Else
            p1.Visible = True
            tt.SetToolTip(p2, "please select a number year")
            bAllFieldsValid = False

        End If
        'check month
        If cbbMonth.SelectedIndex = -1 Then
            p1.Visible = True
            tt.SetToolTip(p1, "please select a number month")
            bAllFieldsValid = False

        Else p1.Visible = False
        End If

        If bAllFieldsValid Then
            Debug.Print("All fields are valid")
            ocontroller.createBreakReport7(cbbMonth.SelectedItem.ToString, txtYear.Text.ToString)
        End If
    End Sub

    Private Sub ControlBreakReport7_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub
End Class